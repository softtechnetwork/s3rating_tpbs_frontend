function initialize_map(){
		var customer_lat_lon = {
			lat:parseFloat($('input[name="device_latitude"]').val()),
			lon:parseFloat($('input[name="device_longitude"]').val())
		};

		console.log('customer_lat_lon');
		console.log(customer_lat_lon);
		var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon  
                },
                zoom:10,
                mapTypeId: google.maps.MapTypeId.ROADMAP    
        });

        var marker = new google.maps.Marker({   
                position:{
                        lat:customer_lat_lon.lat,
                        lng:customer_lat_lon.lon         
                },
                map:map,
                draggable:false
        });

        marker.setMap(map);  

        var iw = new google.maps.InfoWindow({
            content: "<strong>บริเวณของกล่อง</strong> <br><small>"+jQuery('input[name="device_address"]').val()+"</small>"
        });
        google.maps.event.addListener(marker, "mouseover", function(e) {
            iw.open(map, this);
        });

}
