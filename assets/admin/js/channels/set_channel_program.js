function createChannelProgram(element){
	var element = $(element);

	console.log(element);
	$('input[name="hide_channels_id"]').val(element.data('channelid'));
	$('#createChannelProgramModal').modal('toggle');

}

function editChannelProgram(element){
	var element = $(element);
	var row_data = element.data('rowdata');

	console.log(row_data);

	// console.log(moment(row_data.start_time,'h:mm').format('h:mm')); return false;

	var form_obj = $('form[name="create-channel-program"]');
	form_obj.find('input[name="hide_channels_id"]').val(row_data.channels_id);
	form_obj.find('input[name="hide_channel_programs_id"]').val(row_data.id);
	form_obj.find('input[name="action"]').val('update');
	form_obj.find('input[name="name"]').val(row_data.name);
	form_obj.find('input[name="description"]').val(row_data.description);

	form_obj.find('button.ms-choice').attr('disabled','disabled');
	form_obj.find('input[data-name="selectItemdate_of_week"][value="Mon"]').prop('checked',true);

	form_obj.find('input[name="start_time"]').val(moment(row_data.start_time,'h:mm').format('H:mm'));
	form_obj.find('input[name="end_time"]').val(moment(row_data.end_time,'h:mm').format('H:mm'));
	form_obj.find('select[name="active"]').val(row_data.active);
	$('#createChannelProgramModal').modal('toggle');
}

function deleteChannelProgram(element){
	var element = $(element);
	var row_data = element.data('rowdata');
	var base_url = $('input[name="base_url"]').val();

	$.ajax({
		url: base_url+"admin/channels/ajaxDeleteChannelProgram",
		type: "post",
		data: {'rowid':row_data.id} ,
		async:true,
		dataType:'json',
		success: function (response) {
			console.log('==response==');
			console.log(response);
			if(response.status){
				// swal({
				// 	title: "Good job!",
				// 	text: "Save data success",
				// 	icon: "success",
				// }).then(function(){

				// 	location.reload();
				// }); 
				swal({
            						title: "Good job!",
            						text: "Save data success",
            						icon: "success",
            					},function(){
            						location.reload();
            					});

			}
		           // you will get response from your php page (what you echo or print)                 

		       },
		       error: function (request, status, error) {
		       	console.log(request.responseText);
		       }


		   });
}
$(document).ready(function(){
	$('#start_time').datetimepicker({
		format: 'LT'
	});
	$('#end_time').datetimepicker({
		format: 'LT'
	});

	$('select[name="date_of_week"]').change(function() {
            //$('input[name="hide_agent_type"]').val('').val($(this).val());
    }).multipleSelect({
            width: '100%'
    });

    $('input[data-name="selectItemdate_of_week"][value=""]').parent().remove();


});



function submitCreateChannelProgram(e){
	e.preventDefault();

	// check date of week empty
	var str_dow = $('select[name="date_of_week"]').val();

	if(str_dow == '' && $('input[name="action"]').val() == 'create'){
		$('select[name="date_of_week"]').focus();
		return false;
	}



	// var form_data = $('form[name="create-channel-program"]').serialize();

	var form_data = {
		'action':$('input[name="action"]').val(),
		'hide_channels_id':$('input[name="hide_channels_id"]').val(),
		'name':$('input[name="name"]').val(),
		'start_time':$('input[name="start_time"]').val(),
		'end_time':$('input[name="end_time').val(),
		'date_of_week':str_dow,
		'active':$('select[name="active"]').val(),
		'description':$('textarea[name="description"]').val(),
		'hide_channel_programs_id':$('input[name="hide_channel_programs_id"]').val()
	};

	// console.log(form_data);return false;

	var base_url = $('input[name="base_url"]').val();

	            	$.ajax({
            			url: base_url+"admin/channels/ajaxSetChannelProgram",
            			type: "post",
            			data: form_data ,
            			async:true,
            			dataType:'json',
            			success: function (response) {
            				console.log('==response==');
            				console.log(response);
            				if(response.status){

            					
            					$('#createChannelProgramModal').modal('toggle');

            					
            					
            					swal({
            						title: "Good job!",
            						text: "Save data success",
            						icon: "success",
            					},function(){
            						location.reload();
            					});





            				}
							           // you will get response from your php page (what you echo or print)                 

							       },
							       error: function (request, status, error) {
							       	console.log(request.responseText);
							       }


					});





}

function changeSorting(sort_value){
	if(sort_value !== ''){
		var url = window.location.href,
	    retObject = {},
	    parameters;

	    if (url.indexOf('?') === -1) {
	    	window.location.href = window.location.href+'?sorting='+sort_value;
	        return null;
	    }

	    var url_without_querystring = url.split('?')[0];
	    url = url.split('?')[1];


	    var query_string = url;

	    parameters = url.split('&');

	    for (var i = 0; i < parameters.length; i++) {
	        retObject[parameters[i].split('=')[0]] = parameters[i].split('=')[1];
	    }

	    console.log(query_string);

	    if($.isEmptyObject(retObject)){
	    	//console.log(url_without_querystring);
	    	//console.log('empty');
	    	window.location.href = url_without_querystring+'?sorting='+sort_value;
	    }else{
	    	window.location.href = window.location.href+'&sorting='+sort_value;
	    }
	}
}

function checkAllEvent(element){
	var element = $(element);
	if(element.is(':checked')){
		$('input[name="check_event[]"]').each(function(index,el){
			//$(el).attr('checked','checked');
			if(!$(el).is(':checked')){
				$(el).prop('checked',true);
			}
		});
	}else{
		$('input[name="check_event[]"]').each(function(index,el){
			$(el).prop('checked',false);
		});
	}
}
function clickCheckEvent(element){
	var element = $(element);

	if(!element.is(':checked')){
		element.prop('checked',false);
	}

	/* check still check all */
	var check_event_length = $('input[name="check_event[]"]').length;
	var check_event_checked_length = $('input[name="check_event[]"]:checked').length;

	if(check_event_length !== check_event_checked_length){
		$('input[name="check_all"]').prop('checked',false);
	}
	/* */

}

function submitActionEventForm(element){
	var element = $(element);

	var action_type = $('select[name="action"]').val();

	if(!action_type){
			swal({
				title: "Warning!",
				text: "Please select action.",
				icon: "warning",
			}).then(function(){
				$('select[name="action"]').focus();
			});
		return false;
	}

	var count_check_action = $('input[name="check_event[]"]:checked').length;
	if(count_check_action === 0){
		swal({
				title: "Warning!",
				text: "Please check record at least 1 record for action.",
				icon: "warning",
			}).then(function(){
				
			});
		return false;
	}

	/* elf pass condition */
	var arrCheckEvent = [];
	//arrCheckEvent['eventRows'] = [];
	//arrCheckEvent['action'] = $('select[name="action"]').val();


	$('input[name="check_event[]"]:checked').each(function(i,el){
		var el = $(el);
		arrCheckEvent.push({
			'program_id':el.val()			
		});
	});

	var post_data = array2json(arrCheckEvent);

	$('input[name="json_data"]').val(post_data);

	$('form[name="action-event-form"]').attr('action',window.location.href);

	//return false;
	$('form[name="action-event-form"]').submit();

	// var form_html = '<form action="" method="post">';
	// form_html += '<input type="hidden" name="json_data" value="'+post_data+'"';
	// form_html += '</form>';
	// $(form_html).appendTo('body').submit();
	



	/* eof pass condition */
}

function array2json(arr) {
    var parts = [];
    var is_list = (Object.prototype.toString.apply(arr) === '[object Array]');

    for(var key in arr) {
    	var value = arr[key];
        if(typeof value == "object") { //Custom handling for arrays
            if(is_list) parts.push(array2json(value)); /* :RECURSION: */
            else parts.push('"' + key + '":' + array2json(value)); /* :RECURSION: */
            //else parts[key] = array2json(value); /* :RECURSION: */
            
        } else {
            var str = "";
            if(!is_list) str = '"' + key + '":';

            //Custom handling for multiple data types
            if(typeof value == "number") str += value; //Numbers
            else if(value === false) str += 'false'; //The booleans
            else if(value === true) str += 'true';
            else str += '"' + value + '"'; //All other things
            // :TODO: Is there any more datatype we should be in the lookout for? (Functions?)

            parts.push(str);
        }
    }
    var json = parts.join(",");
    
    if(is_list) return '[' + json + ']';//Return numerical JSON
    return '{' + json + '}';//Return associative JSON
}