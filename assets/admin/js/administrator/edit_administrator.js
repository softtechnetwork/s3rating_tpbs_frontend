$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    $('input[name="email"]').attr('readonly','readonly');


    $('form[name="create-administrator-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                user_accesstype_id:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                firstname: {
                    validators: {
                        notEmpty: {
                            message:''
                        }
                    }
                },
                lastname:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                email:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                telephone:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                }
            }
    });


        $('form[name="reset-password-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        stringLength: {
                                min: 6,
                                max: 30,
                                message: ''
                        }
                    }
                },
                confirm_new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        identical: {
                                field: 'new_password',
                                message: ''
                        }
                    }
                }

            },
            onSuccess: function(e, data) {
                e.preventDefault();
                var base_url = $('input[name="base_url"]').val();
                $.ajax({
                url: base_url+"admin/administrator/ajaxSetResetPassword",
                type: "post",
                data: {
                    'new_password':$('input[name="new_password"]').val(),
                    'administrator_id':$('input[name="administrator_id"]').val()
                },
                async:true,
                dataType:'json',
                success: function (response) {
                    console.log('==response==');
                    console.log(response);
                    if(response.status){
                        $('#resetPasswordModal').modal('toggle');

                        swal({
                          title: "Good job!",
                          text: "Password has been reseted!",
                          icon: "success",
                        });
                    }
                   // you will get response from your php page (what you echo or print)                 

                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }


                });

            }

    });

});