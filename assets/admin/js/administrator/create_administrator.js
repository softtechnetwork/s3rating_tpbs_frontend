$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    $('form[name="create-administrator-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                user_accesstype_id:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                firstname: {
                    validators: {
                        notEmpty: {
                            message:''
                        }
                    }
                },
                lastname:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                email:{
                    validators:{
                        remote: {
                            message: $('input[name="email_exist"]').val(),
                            url: base_url+'admin/administrator/ajaxCheckEmailExist'
                        },
                        notEmpty:{
                            message:''
                        }
                    }
                },
                password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        stringLength: {
                                min: 6,
                                max: 30,
                                message: ''
                        }
                    }
                },
                confirm_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        identical: {
                                field: 'password',
                                message: ''
                        }
                    }
                },
                telephone:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                }
            }
    });


});