$(document).ready(function(){

    var base_url = $('input[name="base_url"]').val();
    $('form[name="change-password-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                old_password: {
                    validators: {
                        remote:{
                            message: '',
                            url: base_url+'admin/activity/ajaxCheckOldPassword'
                        },
                        notEmpty: {
                            message: ''
                        }
                    }
                },
                new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        stringLength: {
                                min: 6,
                                max: 30,
                                message: ''
                        }
                    }
                },
                confirm_new_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        identical: {
                                field: 'new_password',
                                message: ''
                        }
                    }
                }
            }
    });

});