function searchDevice(){
	var form_data = $('form[name="search-form"]').serialize();
	console.log(form_data);
	var encodedString = $.base64.encode(form_data);
	//console.log(encodedString);

	var current_location = window.location.href.split('?')[0];
	current_location += '?q='+encodedString;
	window.location.href = current_location;
}