$(document).ready(function () {
	// Return today's date and time
	var currentTime = new Date()

	// case : date picker start range
	// var current_year = currentTime.getFullYear()
	$('input[name="duration"]').daterangepicker({
		"autoApply": true,
		timePicker: false,
		showDropdowns: true,
		singleDatePicker: true,
		startDate: moment().subtract(1, 'day'),
		minDate: new Date(2022, 1, 17),
		maxDate: moment().subtract(1, 'day'),
		locale: {
			format: 'YYYY-MM-DD'
		}
	}, function (start, end, label) {
		console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
	});
});












function export_overviewrating(filename, foldername) {
	var base_url = $('input[name="base_url"]').val();
	var duration = $("#duration").val();

	//?filename=overviewreport_1648630505.xlsx&report_type=overview&foldername=2022_2_19
	//var urlExport = "https://tpbsrating.softtechnw.com/s3rating_nodeapi/api/export_excel/reporttype";
	var urlExport = "http://testweb.softtechnw.com/s3rating_apiservice/api/export_excel/reporttype";
	var urlPram = "?filename=" + filename+"&report_type=overview&foldername="+foldername;
	window.location = urlExport+urlPram;
}

function place_event_in_ajax() {
	
	$("#export-all-btn").click(function() {
		var base_url = $('input[name="base_url"]').val();
		var duration = $("#duration").val();
		//console.log(duration);
		$.ajax({
			type: "POST",
			url: base_url+'report/overview/get_store_overviews',
			dataType : 'json',
			data: {
				'duration' : duration,
			},
			error: function(e, s, d) { console.log(d); },
			success: function (response, status, xhr) {
				if(response.length >0){
					//console.log(response[0].filename);
					//var filename = $(this).attr('filename');
					//var foldername = $(this).attr('foldername');
					export_overviewrating(response[0].fname, response[0].foldername);
				}
			}
		});
		
	});


	$(".export-datas").click(function() {
		var filename = $(this).attr('filename');
		var foldername = $(this).attr('foldername');
		export_overviewrating(filename, foldername);
	});
}

function get_overview(duration, periodtime) {
	var base_url = $('input[name="base_url"]').val();
	$('.exportall-btn-place').html('');
	$(".loader").show();
	$.ajax({
		type: "POST",
		url: base_url+'report/overview/get_overviews',
		dataType : 'json',
		data: {
			'duration' : duration, 
			'periodtime' : periodtime
		},
		error: function(e, s, d) { console.log(d); },
		success: function (response, status, xhr) {
			if(response.length > 0){
				$('#overview-table tbody').html('');
				var tbdy = '';
				response.forEach(item => {
					if(item.type == 'all'){
						var exportall_btn = '';
						exportall_btn += '<button id="export-all-btn" class="btn btn-success" style="background-color: #17a2b8; border-color: #17a2b8; font-weight: normal; padding: 0.175rem 0.75rem; font-size: smaller;float: right;" filename="'+item.filename+'" foldername="'+item.foldername+'">Export All ( 00:00 - 23:59 )</button>';
						$('.exportall-btn-place').html(exportall_btn);
					}else{
						tbdy += '<tr>';
						tbdy += '<td>'+item.date+'</td>';
						tbdy += '<td>'+item.start_time+'</td>';
						tbdy += '<td>'+item.end_time+'</td>';
						tbdy += '<td>';
						tbdy += '	<button filename="'+item.filename+'" foldername="'+item.foldername+'" class="btn btn-success export-datas" style="background-color: #009688; border-color: #009688; font-weight: normal; padding: 0.375rem 1rem; font-size: smaller;">Export</button>';
						tbdy += '</td>';
						tbdy += '</tr>';
					}
				});
				$('#overview-table tbody').append(tbdy);
			}else{
				$('#overview-table tbody').html('');
				$('#overview-table tbody').append('<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty" style="text-align: center;">No data available in table</td></tr>');
			}

			$(".loader").hide();
			place_event_in_ajax();
		}
	});
}

$( "#search-btn" ).click(function( event ) {
	var duration = $("#duration").val();
	var periodtime = $("#periodtime").val();
	get_overview(duration, periodtime);//get overview data
});