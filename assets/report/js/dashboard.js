$(document).ready(function () {
	// Return today's date and time
	var currentTime = new Date()

	// case : date picker start range
	// var current_year = currentTime.getFullYear()
	$('input[name="duration"]').daterangepicker({
		"autoApply": true,
		timePicker: false,
		showDropdowns: true,
		singleDatePicker: true,
		startDate: moment().subtract(1, 'day'),
		minDate: new Date(2022, 0, 1),
		maxDate: moment().subtract(1, 'day'),
		locale: {
			format: 'YYYY-MM-DD'
		}
	}, function (start, end, label) {
		console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
	});

});

function set_inputpicker(channel_id) {
	if (channel_id == 252) {
		$('input[name="duration"]').daterangepicker({
			"autoApply": true,
			timePicker: false,
			showDropdowns: true,
			singleDatePicker: true,
			startDate: moment().subtract(1, 'day'),
			minDate: new Date(2022, 0, 1),
			maxDate: moment().subtract(1, 'day'),
			locale: {
				format: 'YYYY-MM-DD'
			}
		}, function (start, end, label) {
			console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
		});
	} else {
        $('input[name="duration"]').daterangepicker({
			"autoApply": true,
			timePicker: false,
			showDropdowns: true,
			singleDatePicker: true,
			startDate: moment().subtract(1, 'day'),
			minDate: new Date(2022, 0, 1),
			maxDate: moment().subtract(1, 'day'),
			locale: {
				format: 'YYYY-MM-DD'
			}
		}, function (start, end, label) {
			console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
		});
	}

}

function change_date() {
	var channel_id = $("#channels").val();
  
	if (channel_id == 252) {
		$('#duration').daterangepicker('destroy');
		set_inputpicker(channel_id);
	} else {
		$('#duration').daterangepicker('destroy');
        set_inputpicker(channel_id);

	}
}




function export_dailyratinghr() {
	var base_url = $('input[name="base_url"]').val();
	var channels = $("#channels").val();
	var duration_month_start = $("#duration_month_start").val();
	var duration_month_end = $("#duration_month_end").val();

	var dstart_split = duration_month_start.split('/');
	var dstart_m = dstart_split[0];
	var dstart_y = dstart_split[1];

	var dend_split = duration_month_end.split('/');
	var dend_m = dend_split[0];
	var dend_y = dend_split[1];
	// case : ปีน้อยกว่า หรือ ปีเดียวกันเดือนน้อยกว่า
	if (dend_y < dstart_y || (dend_m < dstart_m && dend_y <= dstart_y)) {
		alert("ช่วงเวลาสิ้นสุดไม่สามารถน้อยกว่าเริ่มต้นได้");
	} else {
		window.location = base_url + "report/dashboard/exportchanneldailyratinghr_ajaxcall?id=" + channels + "&duration_month_start=" + duration_month_start + "&duration_month_end=" + duration_month_end;
	}
}


function export_dailyrating() {
	var base_url = $('input[name="base_url"]').val();
	var channels = $("#channels").val();
	var duration = $("#duration").val();
	// alert(duration);
	// alert(channels);
	var url = get_exceldailyreport(type);
	$(".loader").show();
	$.ajax({
		type: "POST",
		url: url,
		data: {
			channel_id: channels,
			date: duration
		},
		success: function (response, status, xhr) {
			//var response = JSON.parse(response);

			var downloadurl = onaction_downloadexcel(type);
			if (response.status == true) {
				// setTimeout(
				// window.location = downloadurl + "?filename=" + response.data

				// , 10000); // set delay 3 second  wait backend build file
				window.location = downloadurl + "?filename=" + response.data
				

			}

			$(".loader").hide();
		}
	});

}


function get_exceldailyreport(type = null) {
	var url = "";
	if (type == "testing") {
		url = "http://testweb.softtechnw.com/s3rating_apiservice/api/get_deviceviewer_report"
	} else {
		url = "https://tpbsrating.softtechnw.com/s3rating_nodeapi/api/get_deviceviewer_report"
	}
	return url;

}

function onaction_downloadexcel(type = null) {
	var url = "";
	if (type == "testing") {
		url = "http://testweb.softtechnw.com/s3rating_apiservice/api/excel/daily"
	} else {
		url = "https://tpbsrating.softtechnw.com/s3rating_nodeapi/api/excel/daily"
	}
	return url;

}
