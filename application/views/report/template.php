<!DOCTYPE html>
<html lang="en">
  <head>
    
    <title>รายงาน</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php echo $this->template->stylesheet?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
    <style>
      body{
        font-family: Kanit !important;
      }
    </style>

  </head>
  <body class="app sidebar-mini rtl <?php echo @$this->session->userdata('toggle_status')?>">
    <input type="hidden" name="base_url" value="<?php echo base_url()?>">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="<?php echo base_url('report/overview')?>"><?php echo __('TPBS RATING')?></a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
     
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <!-- <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li> -->
            <!-- <li><a class="dropdown-item" href="<?php echo base_url('report/activity/change_password')?>"><i class="fa fa-key fa-lg"></i> <?php echo __('Change password')?></a></li>
             -->
            <li><a class="dropdown-item" href="<?php echo base_url('report/login/logout')?>"><i class="fa fa-sign-out fa-lg"></i> <?php echo __('Logout')?></a></li>
          </ul>
        </li>
      </ul>
    </header>

    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <?php $this->load->view('report/template_left_menu')?>
    <main class="app-content">
      <?php echo $this->template->content;?>
    </main>
    <?php echo $this->template->javascript?>
  </body>
</html>