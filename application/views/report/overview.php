<style>
  .loader {
    background-color: white !important;
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 999999999;
    background: url('//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Phi_fenomeni.gif/50px-Phi_fenomeni.gif') 50% 50% no-repeat rgb(249, 249, 249);
  }
</style>
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> ข้อมูลภาพรวม</h1>
    <p></p>
  </div>
  <!--<ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="#">ข้อมูลรับชม</a></li>
  </ul>-->

</div>

<div class="loader" style="display:none;background-color:white !important;"><span style="position:fixed;top:55%;left:47%;">Processing...</span>
<br><span style="position:fixed;top:60%;left:47%;">Please wait</span></div>
<input type="hidden" name="base_url" value="<?=base_url();?>">
<div class="row mb-3">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><i class="fa fa-search"></i> <?php echo __('เลือกช่วงเวลาที่ต้องการ export ข้อมูล') ?></div>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-6 col-md-4 col-lg-3">
            <label for="">วันที่</label>
            <div class="form-group">
              <?php echo form_input(array(
                  'id' => 'duration',
                  'name' => 'duration',
                  'class' => 'form-control',
                  'autocomplete' => 'off',
                  'value' => isset($_POST["duration"]) ? $_POST["duration"] : "",
              )); ?>
            </div>
          </div>
          <div class="col-sm-12 col-md-2 col-lg-2" style="padding-top: 1.7rem;">
            <div class="form-group">
              <button id="search-btn" type="submit" class="btn btn-info" style="width: 100%;"><i class="fa fa-search"></i> ค้นหา</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- table--->
<div class="row mb-3">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <?php echo __('ตาราง ข้อมูลภาพรวม') ?>
        <div class="exportall-btn-place"></div>
      </div>
      <div class="card-body">
        <table id="overview-table" class="table table-striped" style="width:100%;">
          <thead>
              <tr>
                  <th>วันที่</th>
                  <th>ช่วงเวลาเริ่มต้น</th>
                  <th>ช่วงเวลาสิ้นสุด</th>
                  <th>Export Excel</th>
              </tr>
          </thead>
          <tbody><tr class="odd"><td valign="top" colspan="4" class="dataTables_empty" style="text-align: center;">No data available in table</td></tr> </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<script> var type = <?php echo json_encode($type)?>; </script>