<style>

  
.loader {
  
  background-color: white !important;

	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 999999999;
	background: url('//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Phi_fenomeni.gif/50px-Phi_fenomeni.gif') 50% 50% no-repeat rgb(249, 249, 249);

  /* background: url('../images/loading.gif')  50% 50% no-repeat rgb(249, 249, 249); */
}

  .ui-datepicker-calendar {
   /* display: none; */
  }
</style>
<div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> ข้อมูลรับชมรายวัน</h1>
    <p></p>
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="#">ข้อมูลรับชม</a></li>
  </ul>

</div>

<div class="loader" style="display:none;background-color:white !important;"><span style="position:fixed;top:55%;left:48%;">Processing...</span>
<br><span style="position:fixed;top:60%;left:48%;">Please wait</span></div>
<input type="hidden" name="base_url" value="<?php echo base_url();?>">
<div class="row mb-3">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><i class="fa fa-search"></i> <?php echo __('เลือกช่วงเวลาที่ต้องการ export ข้อมูล') ?></div>
      <div class="card-body">
        <?php echo form_open('', ['name' => 'form_tvdexportexcel', 'method' => 'post']) ?>

        <div class="row">

          <div class="col-lg-3">
            <label for="">วันที่</label>
            <div class="form-group">
              <!-- <input name="duration" id="duration" class="form-control" value="<?php echo empty($duration) ? DATE("m") . '/' . DATE("Y") : $duration_month; ?>"> -->

              <?php echo form_input(array(
                                                        'id' => 'duration',
                                                        'name' => 'duration',
                                                      
                                                        'class' => 'form-control',
                                                        'autocomplete' => 'off',
                                                        'value' => isset($_POST["duration"]) ? $_POST["duration"] : "",

                                                    )); ?>

            </div>
          </div>


          <div class="col-lg-3">
            <label for="">ช่อง</label>
            <div class="form-group">
              <select name="channels" id="channels" class="form-control"   onchange="change_date()" title="Please select">
                <?php
                foreach ($tvd_channels as $key => $value) {
                  $selected = "";
                  if (isset($search_text)) {
                    if ($search_text == $value->id) {
                      $selected = "selected";
                    }
                  }
                ?>
                  <option value="<?php echo $value->id ?>" <?php echo $selected; ?>><?php echo $value->channel_name; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>


         

        </div>

        <div class="row">
          <div class="col-lg-12">
            <?php if (!empty($_GET)) { ?>
              <a href="<?php echo base_url($this->uri->uri_string()) ?>" class="btn btn-danger float-right ml-2"><?php echo __('Reset', 'default') ?></a>
            <?php } ?>

            <!-- <a href="#" class="btn btn-success float-right" onclick="export_dailyratinghr()">Export Excel ข้อมูล Rating รายชั่วโมง</a>&nbsp; -->
            <a href="#" class="btn btn-info float-right pr-2 mr-2" onclick="export_dailyrating()">Export Excel ข้อมูลรับชมรายวัน</a>
            <!-- <?php echo form_button([
              'type' => 'submit',
              'class' => 'btn btn-success float-right',
              'content' => '<i class="fa fa-file-excel-o"></i> ' . __('Export Excel')
            ]) ?> -->


          </div>
        </div>
        <span class="text-danger">** หมายเหตุ : วันที่แสดงเฉพาะวันที่มีผังรายการและวันที่น้อยกว่าวันปัจจุบัน</span>
        <?php echo form_close() ?>

      </div>
    </div>
  </div>
</div>

<script>
var type = <?php echo json_encode($type)?>;


</script>