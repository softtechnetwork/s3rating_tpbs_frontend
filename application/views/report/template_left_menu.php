<aside class="app-sidebar">
      <!-- <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="<?php echo getPictureProfile()?>" alt="User Image" width="48" height="48">
        <div>
          <p class="app-sidebar__user-name"><?php echo $this->user_data['firstname'].' '.mb_substr($this->user_data['lastname'],0,1,'utf-8').'.';?></p>
          <p class="app-sidebar__user-designation"></p>
        </div>
      </div> -->
      <ul class="app-menu">
        <?php // if($this->report_data['owner_channel_id'] != 252){?>
          
        <li><a class="app-menu__item <?php echo ($this->controller == 'dashboard')?'active':''?> " href="<?php echo base_url('report/dashboard')?>"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label"><?php echo 'รายงานข้อมูลรับชมรายวัน';?></span></a></li>
        <?php // }?>
        <li><a class="app-menu__item <?php echo ($this->controller == 'overview')?'active':''?> " href="<?php echo base_url('report/overview')?>"><i class="app-menu__icon fa fa-file"></i><span class="app-menu__label"><?php echo 'รายงานข้อมูลภาพรวม';?></span></a></li>
      </ul>
    </aside>