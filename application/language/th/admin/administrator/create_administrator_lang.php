<?php

$lang['Create Administrator'] = "Create Administrator";
$lang['Administrator List'] = "Administrator List";
$lang['Create Edit Administrator'] = "Create Edit Administrator";
$lang['Email has been used.Please enter other email address'] = "Email has been used.Please enter other email address";
$lang['Firstname'] = "Firstname";
$lang['Lastname'] = "Lastname";
$lang['Email'] = "Email";
$lang['Password'] = "Password";
$lang['Confirm Password'] = "Confirm Password";
$lang['Telephone'] = "Telephone";
$lang['Access Type'] = "Access Type";
$lang['Create administrator success'] = "Create administrator success";
$lang['Edit Administrator'] = "Edit Administrator";
$lang['Reset Password'] = "Reset Password";
$lang['Reset password form'] = "Reset password form";
$lang['New Password'] = "New Password";
$lang['Confirm new password'] = "Confirm new password";
$lang['For review device'] = "For review device";
$lang['Edit administrator success'] = "Edit administrator success";
