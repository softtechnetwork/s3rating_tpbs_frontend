<?php

$lang['Set advertisment images and video'] = "Set advertisment images and video";
$lang['Channel Campaign List'] = "Channel Campaign List";
$lang['Click for upload image'] = "Click for upload image";
$lang['Upload image and set duration'] = "Upload image and set duration";
$lang['Youtube Url'] = "Youtube Url";
$lang['Click for add youtube url'] = "Click for add youtube url";
$lang['Add youtube url and duration'] = "Add youtube url and duration";
$lang['Duration'] = "Duration";
$lang['Youtube URL'] = "Youtube URL";
$lang['Video Url'] = "Video Url";
$lang['Start Time'] = "Start Time";
$lang['Start'] = "Start";
$lang['Add/Edit youtube url and duration'] = "Add/Edit youtube url and duration";
$lang['Image'] = "Image";
$lang['pls_select_file'] = "pls_select_file";
$lang['Create advertisement image success'] = "Create advertisement image success";
$lang['confirm_delete_image'] = "confirm_delete_image";
$lang['Image Preview'] = "Image Preview";
$lang['Delete advertisement image success'] = "Delete advertisement image success";
$lang['Edit image and set duration'] = "Edit image and set duration";
$lang['Edit advertisement image success'] = "Edit advertisement image success";
