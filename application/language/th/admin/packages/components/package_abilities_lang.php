<?php

$lang['Show hide channels ranking (Shopping)'] = "Show hide channels ranking (Shopping)";
$lang['Show total devices'] = "Show total devices";
$lang['Show tv program'] = "Show tv program";
$lang['Show population and devices'] = "Show population and devices";
$lang['Show latitude/longitude map'] = "Show latitude/longitude map";
$lang['Compare Channel menu'] = "Compare Channel menu";
$lang['Top twenty menu'] = "Top twenty menu";
$lang['TV program menu'] = "TV program menu";
$lang['Compare channel by region menu'] = "Compare channel by region menu";
$lang['Realtime viewers menu'] = "Realtime viewers menu";
$lang['Compare channel export excel'] = "Compare channel export excel";
