<?php

$lang['Create amphur'] = "Create amphur";
$lang['amphur List'] = "amphur List";
$lang['Create Amphur Form'] = "Create Amphur Form";
$lang['Amphur Name'] = "Amphur Name";
$lang['Edit amphur'] = "Edit amphur";
$lang['Edit Amphur Form'] = "Edit Amphur Form";
$lang['Latitude'] = "Latitude";
$lang['Longitude'] = "Longitude";
$lang['Update amphur success'] = "Update amphur success";
$lang['Province'] = "Province";
