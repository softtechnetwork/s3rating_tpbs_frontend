<?php

$lang['User Menu'] = "User Menu";
$lang['Create User Menu'] = "Create User Menu";
$lang['AccessType'] = "AccessType";
$lang['Firstname&amp;Lastname'] = "Firstname&amp;Lastname";
$lang['Email'] = "Email";
$lang['Telephone'] = "Telephone";
$lang['Updated'] = "Updated";
$lang['Status'] = "Status";
$lang['Menu Name'] = "Menu Name";
$lang['Controller Name'] = "Controller Name";
$lang['Method Name'] = "Method Name";
