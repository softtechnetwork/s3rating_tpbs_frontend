<?php

$lang['User'] = "User";
$lang['Chip Code'] = "Chip Code";
$lang['IP Address'] = "IP Address";
$lang['Address'] = "Address";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['AccessType'] = "AccessType";
$lang['Firstname&amp;Lastname'] = "Firstname&amp;Lastname";
$lang['Email'] = "Email";
$lang['Telephone'] = "Telephone";
$lang['Status'] = "Status";
$lang['User Permission'] = "User Permission";
$lang['Create Corporate'] = "Create Corporate";
$lang['Create User'] = "Create User";
$lang['Delete user success'] = "Delete user success";
