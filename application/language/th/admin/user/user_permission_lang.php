<?php

$lang['Create User'] = "Create User";
$lang['User List'] = "User List";
$lang['Edit User Permission'] = "Edit User Permission";
$lang['Email has been used.Please enter other email address'] = "Email has been used.Please enter other email address";
$lang['Menu Name'] = "Menu Name";
$lang['Controller Name'] = "Controller Name";
$lang['Method Name'] = "Method Name";
$lang['Enable/Disable'] = "Enable/Disable";
$lang['Event Ability'] = "Event Ability";
