<?php

$lang['Edit district'] = "Edit district";
$lang['District List'] = "District List";
$lang['Edit District Form'] = "Edit District Form";
$lang['District Name'] = "District Name";
$lang['Latitude'] = "Latitude";
$lang['Longitude'] = "Longitude";
$lang['Update district success'] = "Update district success";
