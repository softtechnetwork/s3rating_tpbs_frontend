<?php

$lang['Dashboard'] = "Dashboard";
$lang['Channels'] = "Channels";
$lang['Channel Lists'] = "Channel Lists";
$lang['Channel Categories'] = "Channel Categories";
$lang['Corporate'] = "Corporate";
$lang['Device'] = "Device";
$lang['Devices'] = "Devices";
$lang['Corporates'] = "Corporates";
$lang['Users'] = "Users";
$lang['User Menu'] = "User Menu";
$lang['Shopping'] = "Shopping";
$lang['Advertisment'] = "Advertisment";
$lang['Advertisement'] = "Advertisement";
$lang['Shopping Campaign'] = "Shopping Campaign";
$lang['Advertisement Campaign'] = "Advertisement Campaign";
$lang['Administrator'] = "Administrator";
$lang['Administrator List'] = "Administrator List";
$lang['Province'] = "Province";
$lang['Channel Ranking'] = "Channel Ranking";
$lang['Channel Program Chart'] = "Channel Program Chart";
$lang['Amphur'] = "Amphur";
$lang['District'] = "District";
$lang['Packages'] = "Packages";
