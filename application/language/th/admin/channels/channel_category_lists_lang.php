<?php

$lang['Channel Categories'] = "Channel Categories";
$lang['Create Channel'] = "Create Channel";
$lang['Category Name'] = "Category Name";
$lang['Category Description'] = "Category Description";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['Status'] = "Status";
$lang['Create Channel Category'] = "Create Channel Category";
