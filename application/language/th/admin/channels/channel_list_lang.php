<?php

$lang['Channels'] = "Channels";
$lang['Channel Name'] = "Channel Name";
$lang['SRQ'] = "SRQ";
$lang['SYM'] = "SYM";
$lang['VDO PID'] = "VDO PID";
$lang['ADO PID'] = "ADO PID";
$lang['FRQ'] = "FRQ";
$lang['Create Channel'] = "Create Channel";
$lang['Delete channel success'] = "Delete channel success";
$lang['Service ID'] = "Service ID";
$lang['Set channel privilege'] = "Set channel privilege";
$lang['Set channel program'] = "Set channel program";
$lang['Default channel program'] = "Default channel program";
$lang['Set default channel program'] = "Set default channel program";
$lang['Set channel program by date'] = "Set channel program by date";
$lang['Frequency Component'] = "Frequency Component";
$lang['Search'] = "Search";
$lang['Result'] = "Result";
$lang['Record(s)'] = "Record(s)";
