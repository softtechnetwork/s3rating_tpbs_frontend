<?php

$lang['Username or password invalid'] = "Username or password invalid";
$lang['Email or password invalid'] = "Email or password invalid";
$lang['อีเมล์หรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง'] = "อีเมล์หรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง";
$lang['Permission not set yet,Please contact administrator'] = "Permission not set yet,Please contact administrator";
$lang['Sep'] = "Sep";
$lang['Submit'] = "Submit";
$lang['Success'] = "Success!!!";
$lang['Access has been disable,Please try again.'] = "Access has been disable,Please try again.";
$lang['Status'] = "Status";
$lang['Unactive'] = "Unactive";
$lang['Active'] = "Active";
$lang['Oct'] = "Oct";
$lang['Save'] = "Save";
$lang['Select Month'] = "Select Month";

// $lang['Jan'] = "มกราคม";
// $lang['Feb'] = "กุมภาพันธ์";
// $lang['Mar'] = "มีนาคม";
// $lang['Apr'] = "เมษายน";
// $lang['May'] = "พฤษภาคม";
// $lang['Jun'] = "มิถุนายน";
// $lang['Jul'] = "กรกฎาคม";
// $lang['Aug'] = "สิงหาคม";
// $lang['Sep'] = "กันยายน";
// $lang['Oct'] = "ตุลาคม";
// $lang['Nov'] = "พฤศจิกายน";
// $lang['Dec'] = "ธันวาคม";

$lang['Jan'] = "January";
$lang['Feb'] = "February";
$lang['Mar'] = "March";
$lang['Apr'] = "April";
$lang['May'] = "May";
$lang['Jun'] = "June";
$lang['Jul'] = "July";
$lang['Aug'] = "August";
$lang['Sep'] = "September";
$lang['Oct'] = "October";
$lang['Nov'] = "November";
$lang['Dec'] = "December";


$lang['Time'] = "Time : ";
$lang['No'] = "No";
$lang['Yes'] = "Yes";
$lang['Duration'] = "Duration";
$lang['to'] = "to";
$lang['Show'] = "Show";
$lang['Not Show'] = "Not Show";
$lang['Close'] = "Close";
$lang['No Show'] = "No Show";
$lang['Delete record success'] = "Delete record success";
$lang['Search'] = "Search";
$lang['Reset'] = "Reset";
