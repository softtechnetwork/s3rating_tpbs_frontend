<?php

$lang['SIGN IN'] = "SIGN IN";
$lang['Email'] = "Email";
$lang['Password'] = "Password";
$lang['Stay Signed in'] = "Stay Signed in";
$lang['Forgot Password ?'] = "Forgot Password ?";
$lang['EMAIL'] = "EMAIL";
$lang['RESET'] = "RESET";
$lang['Back to Login'] = "Back to Login";
$lang['Username'] = "Username";
$lang['Email or password invalid'] = "Email or password invalid";
$lang['Username or password invalid'] = "Username or password invalid";
