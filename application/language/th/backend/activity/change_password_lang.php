<?php

$lang['Change Password'] = "Change Password";
$lang['Change password form'] = "Change password form";
$lang['Old Password'] = "Old Password";
$lang['New Password'] = "New Password";
$lang['Confirm New Password'] = "Confirm New Password";
$lang['Change password complete'] = "Change password complete";
