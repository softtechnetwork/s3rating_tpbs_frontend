<?php

$lang['Rating Summary'] = "Rating Summary";
$lang['ChipCode'] = "ChipCode";
$lang['FRQ'] = "FRQ";
$lang['SYM'] = "SYM";
$lang['POL'] = "POL";
$lang['Service Id'] = "Service Id";
$lang['VDO PID'] = "VDO PID";
$lang['ADO PID'] = "ADO PID";
$lang['Start View'] = "Start View";
$lang['Duration (Seconds)'] = "Duration (Seconds)";
$lang['Created'] = "Created";
$lang['Channel'] = "Channel";
$lang['No'] = "No";
$lang['Channel Number'] = "Channel Number";
$lang['View Seconds'] = "View Seconds";
