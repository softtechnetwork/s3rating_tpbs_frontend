<?php

$lang['Search rating by chipcode'] = "Search rating by chipcode";
$lang['Ratings'] = "Ratings";
$lang['Chip Code'] = "Chip Code";
$lang['Search'] = "Search";
$lang['Not found rating data'] = "Not found rating data";
$lang['ChipCode'] = "ChipCode";
$lang['Channel'] = "Channel";
$lang['FRQ'] = "FRQ";
$lang['SYM'] = "SYM";
$lang['POL'] = "POL";
$lang['Service Id'] = "Service Id";
$lang['VDO PID'] = "VDO PID";
$lang['ADO PID'] = "ADO PID";
$lang['Start View'] = "Start View";
$lang['Duration (Seconds)'] = "Duration (Seconds)";
$lang['Created'] = "Created";
