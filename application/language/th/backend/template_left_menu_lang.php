<?php

$lang['Dashboard'] = "Dashboard";
$lang['Rating'] = "Rating";
$lang['Rating List'] = "Rating List";
$lang['Devices'] = "Devices";
$lang['Rating Summary'] = "Rating Summary";
$lang['Rating by devices'] = "Rating by devices";
$lang['Rating by chipcode'] = "Rating by chipcode";
