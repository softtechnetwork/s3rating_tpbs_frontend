<?php

$lang['SIGN IN'] = "SIGN IN";
$lang['USERNAME'] = "USERNAME";
$lang['Username'] = "Username";
$lang['PASSWORD'] = "PASSWORD";
$lang['Password'] = "Password";
$lang['Stay Signed in'] = "Stay Signed in";
$lang['Forgot Password ?'] = "Forgot Password ?";
$lang['EMAIL'] = "EMAIL";
$lang['RESET'] = "RESET";
$lang['Back to Login'] = "Back to Login";
$lang['Email'] = "Email";
$lang['Email or password invalid'] = "อีเมล์หรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง";
