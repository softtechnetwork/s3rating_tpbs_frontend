<?php

$lang['Device Detail'] = "Device Detail";
$lang['Devices Detail'] = "Devices Detail";
$lang['Device Information'] = "Device Information";
$lang['Ship Code'] = "Ship Code";
$lang['IP Address'] = "IP Address";
$lang['Created'] = "Created";
$lang['Address Information'] = "Address Information";
$lang['Location'] = "Location";
$lang['Continent'] = "Continent";
$lang['Country'] = "Country";
$lang['City'] = "City";
$lang['Zipcode'] = "Zipcode";
$lang['Device List'] = "Device List";
