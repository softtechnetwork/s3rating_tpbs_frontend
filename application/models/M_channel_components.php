<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_channel_components extends DataMapper {

    //put your code here
    var $table = 'channel_components';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'channels' => array(
             'class' => 'M_channels',
               'other_field' => 'channel_components',
               'join_other_as' => 'channels',
               'join_table' => 'channels'
           )
   );
    
    var $has_many = array(
       'channel_components_update_logs' => array(
           'class' => 'M_channel_components_update_logs',
           'other_field' => 'channel_components',
           'join_self_as' => 'channel_components',
           'join_other_as' => 'channel_components',
           'join_table' => 'channel_components_update_logs')
   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}