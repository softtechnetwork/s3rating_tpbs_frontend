<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_user_owner_channels extends DataMapper {

    //put your code here
    var $table = 'user_owner_channels';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'channels' => array(
             'class' => 'M_channels',
               'other_field' => 'user_owner_channels',
               'join_other_as' => 'channels',
               'join_table' => 'channels'
           ),
           'user'=>array(
                'class'=>'M_user',
                'other_field'=>'user_owner_channels',
                'join_other_as'=>'user',
                'join_table'=>'user'
           )
   );
    
    // var $has_many = array(
    //    'customer' => array(
    //        'class' => 'M_customer',
    //        'other_field' => 'amphur',
    //        'join_self_as' => 'amphur',
    //        'join_other_as' => 'amphur',
    //        'join_table' => 'customer'
    //    )
    // );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}