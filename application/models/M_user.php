<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_user extends DataMapper {

    //put your code here
    var $table = 'user';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           
           'user_accesstype'=>array(
                'class'=>'M_user_accesstype',
                'other_field'=>'user',
                'join_other_as'=>'user_accesstype',
                'join_table'=>'user_accesstype'
           )
   );
    
    var $has_many = array(
       'user_menu_permission' => array(
           'class' => 'M_user_menu_permission',
           'other_field' => 'user',
           'join_self_as' => 'user',
           'join_other_as' => 'user',
           'join_table' => 'user_menu_permission'
       ),
       'user_owner_channels' => array(
           'class' => 'M_user_owner_channels',
           'other_field' => 'user',
           'join_self_as' => 'user',
           'join_other_as' => 'user',
           'join_table' => 'user_owner_channels'
       ),
       'user_authentication_token' => array(
           'class' => 'M_user_authentication_token',
           'other_field' => 'user',
           'join_self_as' => 'user',
           'join_other_as' => 'user',
           'join_table' => 'user_authentication_token'
       )
    );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
}