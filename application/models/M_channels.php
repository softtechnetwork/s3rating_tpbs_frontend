<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_channels extends DataMapper {

    //put your code here
    var $table = 'channels';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   // var $has_one = array(
   //         'product' => array(
   //           'class' => 'M_product',
   //             'other_field' => 'product_image',
   //             'join_other_as' => 'product',
   //             'join_table' => 'product'
   //         )
   // );
    
    var $has_many = array(
       'user_owner_channels' => array(
           'class' => 'M_user_owner_channels',
           'other_field' => 'channels',
           'join_self_as' => 'channels',
           'join_other_as' => 'channels',
           'join_table' => 'user_owner_channels'
       ),
       'advertisement_campaigns'=>array(
            'class'=>'M_advertisement_campaigns',
            'other_field'=>'channels',
            'join_self_as'=>'channels',
            'join_other_as'=>'channels',
            'join_table'=>'advertisement_campaigns'
       ),
       'channel_components'=>array(
            'class'=>'M_channel_components',
            'other_field'=>'channels',
            'join_self_as'=>'channels',
            'join_other_as'=>'channels',
            'join_table'=>'channel_components'
       )
   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}