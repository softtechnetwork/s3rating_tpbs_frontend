<?php 
     class ReportLibrary extends CI_Controller{
            
             public $meta_title = '';
             public $meta_keywords = '';
             public $meta_description = '';
             public $data = array();
             public $page_num = 10;
             public $controller = '';
             public $method = '';
             public $language = 'th';
             public $default_language;
             public $site_setting = '';
             public $user_data = array();
             private $conn_mysql;
             public $onesignal_config = array();
             public $extrameta = "";
             public $encryption_key = 'k^R&<cKQ(44f{mb,';
             public $event_ability = array();
            
         public function __construct() {
                    parent::__construct();
                    $CI = & get_instance();
                    $this->setController($CI->router->class);
                    $this->setMethod($CI->router->method);

                    $this->load->library(array('Msg', 'Lang_controller'));
                    $this->load->helper(array('lang', 'our', 'inflector','html','cookie','url'));
                    
                    $this->template->set_template('report/template');


                    $this->_checkAlready_signin();
                   
                    $this->_load_js();
                    $this->_load_css();

                    //print_r($this->session->userdata());



            }
     
    /**
     * load javascript
     */
    public function _load_js() {
        $this->template->javascript->add(base_url('assets/report/js/jquery-3.2.1.min.js'));

        $this->template->javascript->add(base_url('assets/report/js/popper.min.js'));
        $this->template->javascript->add(base_url('assets/report/js/bootstrap.min.js'));
        $this->template->javascript->add(base_url('assets/report/js/main.js'));
        $this->template->javascript->add(base_url('assets/report/js/plugins/pace.min.js'));
        $this->template->javascript->add(base_url('assets/report/js/template.js'));

    }
    /**
     * load style sheet
     */
    public function _load_css() {

        $this->template->stylesheet->add(base_url('assets/report/css/main.css'));
        $this->template->stylesheet->add('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

    }

    public function loadSweetAlert(){
        $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
    }
    
    public function loadValidator(){
        $this->template->stylesheet->add(base_url('assets/report/css/bootstrapvalidator/bootstrapValidator.min.css'));
        $this->template->javascript->add(base_url('assets/report/js/bootstrapvalidator/bootstrapValidator.min.js'));

        if($this->config->item('language') == 'th'){
            $this->template->javascript->add(base_url('assets/report/js/bootstrapvalidator/language/th_TH.js'));
        }else{
            $this->template->javascript->add(base_url('assets/report/js/bootstrapvalidator/language/en_US.js'));
        }
    }
    public function loadImageUploadStyle(){
        $this->template->stylesheet->add(base_url('assets/report/css/imghover/img_common.css'));
        $this->template->stylesheet->add(base_url('assets/report/css/imghover/img_hover.css'));

    }
    public function loadImageUploadScript(){
        $this->template->javascript->add(base_url('assets/report/js/fileupload_script.js'));
    }
    public function loadDataTableStyle(){
        $this->template->stylesheet->add(base_url('assets/report/vendors/datatables/dataTables.bootstrap4.css'));
    }
    public function loadDataTableScript(){
        $this->template->javascript->add(base_url('assets/report/js/plugins/jquery.dataTables.min.js'));
        $this->template->javascript->add(base_url('assets/report/js/plugins/dataTables.bootstrap.min.js'));
        $this->template->javascript->add(base_url('assets/report/js/enable_datatable.js'));
    }

    public function loadDatePickerStyle(){
        $this->template->stylesheet->add(base_url('assets/report/vendors/jquery-ui-1.12.1/jquery-ui.css'));
    }

    public function bootstrapDatepicker(){
    
        $this->template->stylesheet->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css');
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js')    ;
    }

    public function loadDatePickerScript(){
       
        $this->template->javascript->add(base_url('assets/report/vendors/jquery-ui-1.12.1/jquery-ui.js'));
        $this->template->javascript->add(base_url('assets/report/vendors/jquery-ui-1.12.1/jquery-ui.min.js'));
      
    }
    

    public function loadDateRangePickerStyle(){
        $this->template->stylesheet->add(base_url('assets/report/css/bootstrap-daterangepicker/daterangepicker.css'));
    }
    public function loadDateRangePickerScript(){
        $this->template->javascript->add(base_url('assets/report/js/bootstrap-daterangepicker/moment.min.js'));
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js');
        $this->template->javascript->add(base_url('assets/report/js/bootstrap-daterangepicker/daterangepicker.js'));
    }
    public function loadTempusDominusStyle(){
        $this->template->stylesheet->add(base_url('assets/report/css/bootstrap-tempusdominus/tempusdominus-bootstrap-4.min.css'));
    }
    public function loadTempusDominusScript(){
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment.min.js');
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js');

        $this->template->javascript->add(base_url('assets/report/js/bootstrap-tempusdominus/tempusdominus-bootstrap-4.min.js'));
    }

    public function loadTypeAheadScript(){
        $this->template->javascript->add(base_url('assets/report/js/typeahead/bootstrap3-typeahead.min.js'));
    }

    public function loadTagsInputStyle(){
        $this->template->stylesheet->add(base_url('assets/report/css/bootstrap-tagsinput/bootstrap-tagsinput.css'));
    }
    public function loadTagsInputScript(){
        $this->template->javascript->add(base_url('assets/report/js/bootstrap-tagsinput/bootstrap-tagsinput.min.js'));

    }

    public function loadChartJsScript(){
        $this->template->javascript->add(base_url('assets/report/vendors/chart.js/Chart.min.js'));
    }

    public function loadBootstrapNotify(){
        $this->template->javascript->add(base_url('assets/report/js/plugins/bootstrap-notify.min.js'));
    }

    

    public function getEventAbility(){
        $query = $this->db->select('*')
        ->from('AccountantMenuPermission')
        ->join('AccountantMenu','AccountantMenuPermission.AccountantMenu_id = AccountantMenu.id')
        ->where('AccountantMenu.Controller_name',$this->controller)
        ->where('AccountantMenu.Method_name',$this->method)
        ->where('AccountantMenuPermission.Accountant_id',$this->administrator_data['id'])
        ->get();

        //echo $this->db->last_query();exit;
        if($query->num_rows() > 0){
            $this->event_ability = json_decode($query->row()->EventAbility,true);
        }
        //print_r($this->event_ability);
    }

    public function _checkAlready_signin(){
  
        if(!$this->session->userdata('report_data')){
            redirect(base_url('report'));
        }else{
            // echo '5';exit();
            $this->report_data = $this->session->userdata('report_data');
           // print_r($this->administrator_data);exit;
        }
    }

    public function _getAccountData(){
        if($this->session->userdata('administrator_id') && $this->session->userdata('administrator_data')){
            $this->administrator_data = $this->session->userdata('administrator_data');
            //$this->account_data = $this->session->userdata('member_data');
        }

    }

    /**
     * get default language of hotel
     */
    public function _default_langauge() {
        $shop_language = new M_gshop_shop_language();
        $shop_language->where('gshop_shop_id', $this->shop_id)->where('default_language', 1)->get();
        $this->default_language = $shop_language->language->get()->getForeign_language_name();
       // echo "default lang =".$this->default_language;
    }

    /**
     * change lang system
     */
    public function change_lang() {
       
        if ($this->input->post(NULL, FALSE)) {
            $this->session->set_userdata('language', $this->input->post('language'));
        }
    }

    /**
     *
     * @param <type> $file_view  ชื่อ file view
     * @param <type> $return true = return view �?ลับมา�?สดงเรย / false = ไม่ return view �?ลับ
     */
    public function setView($file_view = null, $return = false) {
        if ($return)
            $this->load->view($file_view, $this->getData());
        else
            $this->setData('the_content', $this->load->view($file_view, $this->getData(), true));
    }

    /**
     * �?สดง view
     */
    public function getView() {
        $this->load->view($this->getTheme(), $this->getData());
    }

    /**
     *
     * @return them template
     */
    public function getTheme() {
        return $this->theme;
    }

    public function setTheme($theme) {
        $this->theme = $theme;
    }

    public function getMeta_title() {
        return $this->meta_title;
    }

    public function setMeta_title($meta_title) {
        $this->setData('meta_title', $meta_title);
        $this->meta_title = $meta_title;
    }

    public function getMeta_keywords() {
        return $this->meta_keywords;
    }

    public function setMeta_keywords($meta_keywords) {
        $this->setData('meta_keywords', $meta_keywords);
        $this->meta_keywords = $meta_keywords;
    }

    public function getMeta_description() {
        return $this->meta_description;
    }

    public function setMeta_description($meta_description) {
        $this->setData('meta_description', $meta_description);
        $this->meta_description = $meta_description;
    }

    public function getData($key = null) {
        if (is_null($key) || empty($key))
            return $this->data;
        else
            return $this->data[$key];
    }

    public function setData($key, $data) {
        $this->data[$key] = $data;
    }

    public function getPage_num() {
        return $this->page_num;
    }

    public function setPage_num($page_num) {
        $this->page_num = $page_num;
    }

    public function getController() {
        return $this->controller;
    }

    public function setController($controller) {
        $this->setData('controller', $controller);
        $this->controller = $controller;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        $this->setData('method', $method);
        $this->method = $method;
    }

    /**
     * จัด�?ารเรื่อง pagination
     * @param <type> $total จำนวน�?ถวทั้งหมด
     * @param <type> $cur_page หน้าปัจจุบัน
     */
    public function config_page($total, $cur_page) {

        //print_r($cur_page);exit;
                $config['base_url'] = base_url();
                $config['total_rows'] = $total;
                $config['per_page'] = $this->page_num;

                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['num_tag_open'] = '<li class="page-item">';
                $config['num_tag_close'] = '</li>';

                $config['prev_tag_open'] = '<li class="paging_btn">';
                $config['prev_tag_close'] = '</li>';
                $config['prev_link'] = '« ' . __('Previous', 'b2c_default');

                $config['cur_tag_open'] = '<li class="active"><a class="page-link">';
                $config['cur_tag_close'] = '</a></li>';
                $config['next_link'] = __('Next', 'b2c_default') . ' »';
                $config['next_tag_open'] = '<li class="paging_btn">';
                $config['next_tag_close'] = '</li>';
                $config['num_links'] = 5;

                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['first_link'] = __('First', 'b2c_default');
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['last_link'] = __('Last', 'b2c_default');
                $config['cur_page'] = $cur_page;
                $this->data['config_page'] = $config;
                $this->setData('config_page', $config);
            }
    
    
    //================ IF SET COOKIE TO KEEP SIGN IN ==================
    public  function check_stay_signin($cookie_status,$cookie_accid){
			if($cookie_status == base64_decode(base64_decode('1'))){
				$user_data = array(
				'customer_id' => base64_decode(base64_decode($cookie_accid))
				);
				$this->session->set_userdata($user_data);	
                               // var_dump($this->session->userdata);exit;
			}
    }
    
    public function getPath_online(){
        switch (ENVIRONMENT){
            case "development":
                $this->path_online = "http://cooshop_system.local/";
            break;
            case "testing":
                
            break;
            case "production":
                
            break;
        }
        
    }
    public function getSite_setting(){
        $setting = new M_setting();
        $setting->where('id',1)->get();
        $this->site_setting = $setting;
    }

    public function checkMenuPermission(){  
        /* get all menu */
        /* get first class and method available to access */
        $first_class = "";
        $first_method = "";

        $first_class = $this->account_data['member_permissions'][0]['controller_name'];
        $first_method = $this->account_data['member_permissions'][0]['method_name'];



        // print_r($this->account_data['member_permissions']);
        $checkpermission_class_method = checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>$this->controller,'method_name'=>$this->method));

        //print_r($checkpermission_class_method);
        $this->event_ability = json_decode((string)$checkpermission_class_method);

        if($checkpermission_class_method || in_array($this->controller, $this->account_data[
            'member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){
            $this->event_ability = json_decode((string)$checkpermission_class_method);
        }else{
            redirect(base_url('report/'.$first_class.'/'.$first_method));
        }
        //echo $this->controller.'<br>'.$this->method;

        /* eof check menu permission */

    }

    protected function checkControllerAvailable(){

    }
    protected function checkMethodAvailable(){
        if(!checkMethodAvailable(array('controller'=>$this->controller,'method'=>$this->method))){
            if($this->controller == 'dashboard' && $this->method == 'index'){
                //print_r($this->session->userdata());exit;
                $menu_available = $this->session->userdata('menu_available');
                redirect(base_url('report/'.$menu_available[0]['controller'].'/'.$menu_available[0]['method']));
            }else{
                redirect(base_url('backend'));
            }
        }
    }

 
    public function getdatetimereportV2($y, $m, $lastdate_filter = null)
	{
		$month = $m;
		$year = $y;

		$start_date = "01-" . $month . "-" . $year;
		$start_time = strtotime($start_date);

		$end_time = strtotime("+1 month", $start_time);

		for ($i = $start_time; $i < $end_time; $i += 86400) {
			if (empty($lastdate_filter)) {
				$list[] = date('Y-m-d', $i);
			} else {
				if (date('Y-m-d', $i) <= $lastdate_filter) {
					$list[] = date('Y-m-d', $i);

				}
			}
		}
      
		//return $list;
		return $list;
	}

}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 

