<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class S3RatingLibrary{

    private $ci;

    private $current_rating_data_table;
    private $current_rating_data_daily_devices;

    function __construct() {
        $this->ci =& get_instance();
        $this->ci->load->library(array(
            'Lang_controller'
        ));
        $this->ci->load->helper(array(
            'lang',
            'our'
        ));

        $this->setCurrentDataTable();

        //print_r($this->current_rating_data_table);exit;
    }

    public function setUserLogin($requestCriteria){


        $this->ci->load->library('user_agent');




        //print_r($requestCriteria);
        /* query for check user */

        if(strtolower($requestCriteria->email) == 'nbtc'){
            $requestCriteria->password = strtolower($requestCriteria->password);
        }

        //print_r($requestCriteria);exit;

        $user = new M_user();
        $user->where('email',$requestCriteria->email)
        ->where('password',sha1($requestCriteria->password))
        ->where('owner_channel_status',1)
        ->get();

        // echo $user->check_last_query();exit;

        if($user->id){

            if($user->user_accesstype_id == 3){
                return array(
                    'status'=>false,
                    'result_code'=>'-009',
                    'result_desc'=>'Access denied.'
                );
            }
            if(!$user->active){
                return array(
                    'status'=>false,
                    'result_code'=>'-009',
                    'result_desc'=>'Your account has been banded,Please contact to administrator.'
                );
            }

            if(!$user->owner_channel_id){
                return array(
                    'status'=>false,
                    'result_code'=>'-002',
                    'result_desc'=>'Do not permit from any channel.'
                );
            }

            /* check or insert user authentication token  */

            if(property_exists($requestCriteria, 'storage_token') && strtolower($requestCriteria->email) != 'nbtc'){



                // write log for check nbtc 
                $log_file_path = $this->createLogFilePath($requestCriteria->email.' - RequestS3Rating');
                $file_content = $requestCriteria->email.' - '.date("Y-m-d H:i:s") . ' post value : ' . json_encode($requestCriteria) . " | ip address : ".$this->getUserIP()." | platform : ".$this->ci->agent->platform()." \n";
                file_put_contents($log_file_path, $file_content, FILE_APPEND);
                unset($file_content);

                // check insert if not found storage token for this user before

                if($requestCriteria->storage_token == ''){
                    // request for receive token
                    $this->checkReceiveToken([
                        'user'=>$user
                    ]);

                }else{
                    // already token check and set status
                    $arr_return = $this->returnCheckUserAuthenticationToken([
                        'storage_token'=>$requestCriteria->storage_token,
                        'user'=>$user
                    ]);

                    if(!$arr_return['status'] && $arr_return['event'] == 'login-by-other'){
                        return [
                            'status'=>false,
                            'result_code'=>'-10',
                            'result_desc'=>'Have access from other devices,Please log in again.'
                        ];
                    }
                }



            }else{

                // write log for check nbtc 
                $log_file_path = $this->createLogFilePath($requestCriteria->email.' - RequestS3Rating');
                $file_content = $requestCriteria->email.' - '.date("Y-m-d H:i:s") . ' post value : ' . json_encode($requestCriteria) . " | ip address : ".$this->getUserIP()." | platform : ".$this->ci->agent->platform()." \n";
                file_put_contents($log_file_path, $file_content, FILE_APPEND);
                unset($file_content);


            }
            

            $rating_package_data = $this->getRatingPackagesData([
                'rating_packages_id'=>$user->rating_packages_id
            ]);

            $rating_package_data->{'image'} = base_url('uploaded/packages/'.$rating_package_data->id.'/'.$rating_package_data->image);
            //print_r($rating_package_data);exit; 

            if(isset($_GET['test']) && $_GET['test'] == 'test'){
                print_r($rating_package_data);exit;
            }
            $user_data = [
                'user_id'=>$user->id,
                'user_firstname'=>$user->firstname,
                'user_lastname'=>$user->lastname,
                'user_email'=>$user->email,
                'user_telephone'=>$user->telephone,
                'admin_status'=>($rating_package_data)?$rating_package_data->realtime_viewers_menu_status:0,
                'show_total_device_status'=>($rating_package_data)?$rating_package_data->show_total_device_status:0,
                'show_tv_program_menu'=>($rating_package_data)?$rating_package_data->show_tv_program_status:0,
                'show_population_and_devices'=>($rating_package_data)?$rating_package_data->show_population_and_devices_status:0,
                'show_latlon_map_status'=>($rating_package_data)?$rating_package_data->show_latlon_map_status:0,
                'compare_channel_menu_status'=>($rating_package_data)?$rating_package_data->compare_channel_menu_status:0,
                'top_twenty_channels_menu_status'=>($rating_package_data)?$rating_package_data->top_twenty_channels_menu_status:0,
                'compare_channel_by_region_menu_status'=>($rating_package_data)?$rating_package_data->compare_channel_by_region_menu_status:0,
                'realtime_viewers_menu_status'=>($rating_package_data)?$rating_package_data->realtime_viewers_menu_status:0,
                'compare_channel_excel_status'=>($rating_package_data)?$rating_package_data->compare_channel_export_status:0
            ];

            // print_r($user_data);exit;

            $usr_channel_compare = (count($this->getUserChannelCompare($user->id)) > 0)?$this->getUserChannelCompare($user->id):$this->getChannelRelatedCategory($user->owner_channel_id);
            return array(
                'status'=>true,
                // 'UserData'=>array(
                //     'user_id'=>$user->id,
                //     'user_firstname'=>$user->firstname,
                //     'user_lastname'=>$user->lastname,
                //     'user_email'=>$user->email,
                //     'user_telephone'=>$user->telephone,
                //     'admin_status'=>$user->admin_status,
                //     'show_total_device_status'=>$user->show_total_device_status,
                //     'show_tv_program_menu'=>$user->show_tv_program_status,
                //     'show_population_and_devices'=>$user->show_population_and_devices_status,
                //     'show_latlon_map_status'=>$user->show_latlon_map_status
                // ),
                'UserData'=>$user_data,
                'PackageData'=>$rating_package_data,
                'ChannelData'=>$this->getChannelDataByChannelId($user->owner_channel_id),
                'ChannelRelatedCategory'=>$this->getChannelRelatedCategory($user->owner_channel_id),
                'MainPageData'=>$this->getMainPageData($user->owner_channel_id),
                'UserChannelsCompare'=>$usr_channel_compare,
                'storage_token'=>$this->getStorageTokenByUser(['user_id'=>$user->id]),
                'result_code'=>'000',
                'result_desc'=>'Success'
            );


        }else{

            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Username/Email or password invalid,Please try again.'
            );
        }
    }

    public function getRefreshMainPageData($requestCriteria){
        //print_r($requestCriteria);
        $user = new M_user($requestCriteria->user_id);

        $channels_id = $user->owner_channel_id;
        //echo $channels_id;exit;

        return array(
            'status'=>true,
            'MainPageData'=>$this->getMainPageData($channels_id),
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    public function getCompareChannelTopTwentyRating($requestCriteria){
        //print_r($requestCriteria);
        // $query = $this->ci->db->select('*')
        // ->from('channel_daily_rating')
        // ->join('channels','channels.id = channel_daily_rating.channels_id')
        // ->join('rating_data_daily','rating_data_daily.channels_id = channels.id')
        // ->join('channel_daily_rating_logs','channel_daily_rating.id = channel')
        // ->where('')

        $arrData = array();
        $start_datetime = new DateTime($requestCriteria->date_start.' '.$requestCriteria->time_start);
        $end_datetime = new DateTime($requestCriteria->date_end.' '.$requestCriteria->time_end);

        $query_string = "select TOP 40 channel_daily_rating_id,AVG(channel_daily_rating_logs.rating) as average_rating";
        $query_string .= " from channel_daily_rating_logs";
        $query_string .= " where created >= '".$start_datetime->format('Y-m-d H:i:s')."'";
        $query_string .= " and created <= '".$end_datetime->format('Y-m-d H:i:s')."'";
        $query_string .= " group by channel_daily_rating_id";
        $query_string .= " order by average_rating desc";

        //echo $query_string;exit;

        $query = $this->ci->db->query($query_string);

        $all_channels = array();
        $average_channel = array();
        $all_average_channel = array();
        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $queryChannelAndRatingDaily = $this->ci->db->select('*')
                ->from('channel_daily_rating')
                ->join('channels','channel_daily_rating.channels_id = channels.id')
                ->where('channel_daily_rating.id',$value->channel_daily_rating_id)
                ->get();

                //print_r($queryChannelAndRatingDaily->row());
                if($queryChannelAndRatingDaily->num_rows() > 0){
                    $rowData = $queryChannelAndRatingDaily->row();

                    array_push($all_channels, $rowData->channels_id);
                    array_push($all_average_channel, array(
                        'channels_id'=>$rowData->channels_id,
                        'rating'=>$value->average_rating
                    ));
                }



                // array_push($arrData, array(
                //     'ordinal'=>++$key,
                //     'channel_id'=>$rowData->channels_id,
                //     'channel_name'=>$rowData->channel_name,
                //     'rating'=>number_format($value->average_rating,2),
                //     'watch_time'=>$this->getWatchTimeByChannelAndDate(array(
                //         'channels_id'=>$rowData->channels_id,
                //         'date_start'=>$requestCriteria->date_start,
                //         'date_end'=>$requestCriteria->date_end
                //     ))
                // ));

            }

            // print_r($all_average_channel);exit;

            $all_channels = array_unique($all_channels);
            // print_r($all_channels);exit;

            $arr_summary = array();
            foreach ($all_channels as $key => $value) {
                # code...
                $arr_summary[$key]['channels_id'] = $value;
                $arr_summary[$key]['rating'] = array();
                foreach ($all_average_channel as $k => $v) {
                    # code...
                    if($value == $v['channels_id']){
                        array_push($arr_summary[$key]['rating'], $v['rating']);
                    }
                }

            }
            //print_r($arr_summary);exit;
            $ordinal_number = 1;
            foreach ($arr_summary as $key => $value) {
                # code...
                if($ordinal_number <= 20){
                    $queryChannel = $this->ci->db->select('*')
                    ->from('channels')
                    ->where('id',$value['channels_id'])
                    ->get();

                    $rowChannel = $queryChannel->row();

                    array_push($arrData, array(
                        'ordinal'=>$ordinal_number,
                        'channel_id'=>$rowChannel->id,
                        'channel_name'=>$rowChannel->channel_name,
                        'rating'=>number_format(max($value['rating']),3),
                        'watch_time'=>$this->getWatchTimeByChannelAndDate(array(
                            'channels_id'=>$rowChannel->id,
                            'date_start'=>$requestCriteria->date_start,
                            'date_end'=>$requestCriteria->date_end
                        ))
                    ));

                    $ordinal_number++;


                }
            }
            //print_r($arrData);exit;



        }

        return array(
            'status'=>true,
            'ChannelList'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );

        //exit;
    }

    public function getCompareManualChannels($requestCriteria){

        $arrData = array();
        $arrAnnualReport = array();
        //print_r($requestCriteria);
        $start_datetime = new DateTime($requestCriteria->date_start.' '.$requestCriteria->time_start);
        $end_datetime = new DateTime($requestCriteria->date_end.' '.$requestCriteria->time_end);

        foreach ($requestCriteria->channels as $key => $value) {
            # code...
            $arrRating = array();
            $query = $this->ci->db->select('channel_daily_rating_logs.channel_daily_rating_id,AVG(channel_daily_rating_logs.rating) as average_rating')
            ->from('channel_daily_rating_logs')
            ->join('channel_daily_rating','channel_daily_rating.id = channel_daily_rating_logs.channel_daily_rating_id')
            ->where('channel_daily_rating_logs.created >= ',$start_datetime->format('Y-m-d H:i:s'))
            ->where('channel_daily_rating_logs.created <= ',$end_datetime->format('Y-m-d H:i:s'))
            ->where('channel_daily_rating.channels_id',$value)
            ->group_by('channel_daily_rating_logs.channel_daily_rating_id')
            ->get();
            $rating = 0;
            if($query->num_rows() <= 0){
                $rating = 0;
            }else if($query->num_rows() == 1){
                $rating = $query->row()->average_rating;
            }else{
                foreach ($query->result() as $k => $v) {
                    # code...
                    array_push($arrRating, $v->average_rating);
                }
                $a = array_filter($arrRating);
                $average = array_sum($a)/count($a);
                $rating = $average;
            }

            $queryChannel = $this->ci->db->select('*')
            ->from('channels')
            ->where('id',$value)
            ->get();

            array_push($arrData, array(
                'channel_id'=>$value,
                'channel_name'=>$queryChannel->row()->channel_name,
                'rating'=>number_format($rating,3),
                'watch_time'=>$this->getWatchTimeByChannelAndDate(array(
                        'channels_id'=>$value,
                        'date_start'=>$requestCriteria->date_start,
                        'date_end'=>$requestCriteria->date_end
                ))
            ));

            //echo $rating.'<br>';

            /* get annual report */
            $annual_rating = $this->getAnnualRatingByChannel(array(
                'channel_id'=>$value
            )); 

            $arrData[$key]['annual_rating'] = $annual_rating;
            /* eof get annual report */

            //echo $this->ci->db->last_query();exit;
        }
        return array(
            'status'=>true,
            'ChannelList'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    public function getAllChannelRating($requestCriteria){
        //print_r($requestCriteria);
        $arrReturn = array();
        $query = $this->ci->db->select('*')
        ->from('channels')
        ->where('active',1)
        ->where('channel_extended_status',0)
        ->order_by('channel_name','asc')
        ->get();

        foreach ($query->result() as $key => $value) {
            # code...

            
            array_push($arrReturn, array(
                'channel_id'=>$value->id,
                'channel_name'=>$value->channel_name,
                'channel_logo'=>$this->getChannelLogo($value->logo),
                'channel_frq'=>$value->frq,
                'channel_sym'=>$value->sym,
                'channel_pol'=>$value->pol,
                'channel_vdo_pid'=>$value->vdo_pid,
                'channel_ado_pid'=>$value->ado_pid
            ));
        }

        return array(
            'status'=>true,
            'ChannelList'=>$arrReturn,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );  
    }

    public function getAllChannelRatingWithChannelProgram($requestCriteria){
        $arrReturn = array();
        


        $arrChannel = [];
        $query = $this->ci->db->select('channels_id')
        ->from('channel_programs')
        ->group_by('channels_id')
        ->order_by('channels_id','desc')
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                array_push($arrChannel, $value->channels_id);
            }
        }

        $queryBoDate = $this->ci->db->select('channels_id')
        ->from('channel_programs_baseon_date')
        ->group_by('channels_id')
        ->get();

        if($queryBoDate->num_rows() > 0){
            foreach ($queryBoDate->result() as $key => $value) {
                # code...
                array_push($arrChannel, $value->channels_id);
            }
        }

        $arrChannel =  array_unique($arrChannel);

        foreach ($arrChannel as $key => $value) {
            # code...
            $queryChannel = $this->ci->db->select('*')
            ->from('channels')
            ->where('active',1)
            ->where('channel_extended_status',0)
            ->where('enable_tv_program',1)
            ->where('id',$value)
            ->get();

            if($queryChannel->num_rows() > 0){
                $row = $queryChannel->row();
                array_push($arrReturn, array(
                    'channel_id'=>$row->id,
                    'channel_name'=>$row->channel_name,
                    'channel_logo'=>$this->getChannelLogo($row->logo),
                    'channel_frq'=>$row->frq,
                    'channel_sym'=>$row->sym,
                    'channel_pol'=>$row->pol,
                    'channel_vdo_pid'=>$row->vdo_pid,
                    'channel_ado_pid'=>$row->ado_pid
                ));

            }
        }

        return array(
            'status'=>true,
            'ChannelList'=>$arrReturn,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );  


    }

    public function getCompareChannelByRegion($requestCriteria){
        // print_r($requestCriteria);
        // exit;

        $current_date = (isset($_GET['date_report']))?$_GET['date_report']:date('Y-m-d');



        $firstday_of_month = new DateTime($requestCriteria->year.'-'.$requestCriteria->month.'-01');


        //echo $current_date

        //print_r($requestCriteria);exit;
        // if($requestCriteria->month == '07' && $requestCriteria->year == '2019'){
        //     return [
        //         'status'=>false,
        //         'result_code'=>'-010',
        //         'result_desc'=>'The system is currently improving.'
        //     ];
        // }



        /* older version return every day ignore first day */
        /*
        if($firstday_of_month->format('Y-m-d') != date('Y-m-d')){
            return $this->getCompareChannelByRegionTest($requestCriteria);
            exit;
        }
        */
        return $this->getCompareChannelByRegionTest($requestCriteria);
        exit;

        $arrRegion = array(
            'Bangkok',
            'Northern',
            'NorthEastern',
            'Central',
            'Eastern',
            'Southern',
            'Western'
        );
        $arrRegionData = array(
            'Bangkok'=>array(
                'name'=>'Bangkok',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Bangkok'
            ),
            'Central'=>array(
                'name'=>'Central',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Central'
            ),
            'Northern'=>array(
                'name'=>'Northern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'North'
            ),
            'NorthEastern'=>array(
                'name'=>'NorthEastern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Northeast'
            ),
            'Western' => array(
                'name'=>'Western',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'West'
            ),
            'Eastern' => array(
                'name'=>'Eastern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'East'
            ),
            'Southern' => array(
                'name'=>'Southern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'South'
            ),
            
        );
        $user = new M_user();
        $user->where('id',$requestCriteria->user_id)
        ->get();


        $channel_id = (property_exists($requestCriteria, 'channel_id') && $requestCriteria->channel_id != '')?$requestCriteria->channel_id:$user->owner_channel_id;
        //echo $channel_id;exit;




        //========================= OLDER VERSION =================================
        //========================= 20.11.18 ======================================
        // $query = $this->ci->db->select('rating_data_daily_devices.devices_id,count(rating_data_daily_devices.id) as count_devices,SUM(rating_data_daily_devices.total_seconds) as sum_seconds')
        // ->from('rating_data_daily_devices')
        // ->join('rating_data_daily','rating_data_daily_devices.rating_data_daily_id = rating_data_daily.id')
        // ->where('rating_data_daily.channels_id',$channel_id)
        // ->where('MONTH(rating_data_daily_devices.created)',$requestCriteria->month)
        // ->where('YEAR(rating_data_daily_devices.created)',$requestCriteria->year)
        // ->group_by('rating_data_daily_devices.devices_id')
        // ->get();
        //========================= EOF OLDER VERSION ================================

        $requestCriteriaDailyDevices = 'rating_data_daily_devices_'.$requestCriteria->year.'_'.$requestCriteria->month;


        try {
            $query = $this->ci->db->select($requestCriteriaDailyDevices.".devices_id,count(".$requestCriteriaDailyDevices.".id) as count_devices,SUM(".$requestCriteriaDailyDevices.".total_seconds) as sum_seconds")
            ->from($requestCriteriaDailyDevices)
            ->join('rating_data_daily',$requestCriteriaDailyDevices.".rating_data_daily_id = rating_data_daily.id")
            ->where('rating_data_daily.channels_id',$channel_id)
            ->where('MONTH('.$requestCriteriaDailyDevices.'.created)',$requestCriteria->month)
            ->where('YEAR('.$requestCriteriaDailyDevices.'.created)',$requestCriteria->year)
            ->group_by($requestCriteriaDailyDevices.'.devices_id')
            ->get();
            
        } catch (Exception $e) {
            //print_r($e);exit;
            return array(
                'status'=>true,
                'RegionData'=>array(),
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
            
        }
        


        //echo $this->ci->db->last_query();exit;

        //print_r($query->num_rows)

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                /* get province region from device_addresses*/
                $qAddress = $this->ci->db->select('devices_id,province_region')
                ->from('device_addresses')->where('devices_id',$value->devices_id)
                ->get();

                if($qAddress->num_rows() > 0){
                    $rowAddress = $qAddress->row();
                    if(in_array($rowAddress->province_region, $arrRegion)){
                        array_push($arrRegionData[$rowAddress->province_region]['devices'], $value->devices_id);

                        array_push($arrRegionData[$rowAddress->province_region]['watch_time_array'], $value->sum_seconds?$value->sum_seconds:0);
                    }else{
                        array_push($arrRegionData['Central']['devices'], $value->devices_id);
                        array_push($arrRegionData['Central']['watch_time_array'], $value->sum_seconds?$value->sum_seconds:0);
                    }
                }else{
                    array_push($arrRegionData['Central']['devices'], $value->devices_id);
                    array_push($arrRegionData['Central']['watch_time_array'], $value->sum_seconds?$value->sum_seconds:0);
                }



            }

            //print_r($arrRegionData);exit;

            foreach ($arrRegionData as $key => $value) {
                # code...
                $arrRegionData[$key]['total_devices'] = count($arrRegionData[$key]['devices']); 
                $arrRegionData[$key]['watch_time_summary'] = (count($arrRegionData[$key]['watch_time_array']) > 0)?array_sum($arrRegionData[$key]['watch_time_array']):0;
            }

            
            foreach ($arrRegionData as $key => $value) {
                # code...
                $arrRegionData[$key]['rating'] = $this->getRegionRating(array(
                    'total_devices'=>$value['total_devices']
                ));
                $arrRegionData[$key]['watch_time']=$this->getRegionWatchTime($value['watch_time_summary']);
                $arrRegionData[$key]['short_name'] = $value['short_name'];
            }

            //print_r($arrRegionData);
        }else{
            return array(
                'status'=>true,
                'RegionData'=>$arrRegionData,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );

        }

        if($_GET['test']=='test'){
            print_r($arrRegionData);exit;
        }

        return array(
            'status'=>true,
            'RegionData'=>$arrRegionData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    private function getCompareChannelByRegionTest($requestCriteria){
        $arrRegion = array(
            'Bangkok',
            'Northern',
            'NorthEastern',
            'Central',
            'Eastern',
            'Southern',
            'Western'
        );
        $arrRegionData = array(
            'Bangkok'=>array(
                'name'=>'Bangkok',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Bangkok'
            ),
            'Central'=>array(
                'name'=>'Central',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Central'
            ),
            'Northern'=>array(
                'name'=>'Northern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'North'
            ),
            'NorthEastern'=>array(
                'name'=>'NorthEastern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Northeast'
            ),
            'Western' => array(
                'name'=>'Western',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'West'
            ),
            
            'Eastern' => array(
                'name'=>'Eastern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'East'
            ),
            'Southern' => array(
                'name'=>'Southern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'South'
            )
            
        );
        $user = new M_user();
        $user->where('id',$requestCriteria->user_id)
        ->get();


        $datetime_criteria = new DateTime('01-'.$requestCriteria->month.'-'.$requestCriteria->year);

        //print_r($datetime_criteria);exit;


        $channel_id = (property_exists($requestCriteria, 'channel_id') && $requestCriteria->channel_id != '')?$requestCriteria->channel_id:$user->owner_channel_id;

        $query = $this->ci->db->select('province_region,AVG(rating) as average_rating,SUM(cast(sum_duration as bigint)) as sum_duration')
        ->from('channel_audience_region_summary_new')
        ->where('MONTH(date)',$datetime_criteria->format('m'))
        ->where('YEAR(date)',$datetime_criteria->format('Y'))
        ->where('channels_id',$channel_id)
        ->group_by('province_region')
        ->get();

    //echo $this->ci->db->last_query();exit();        
        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                foreach ($arrRegionData as $k => $v) {
                    # code...
                    if($value->province_region == $v['name']){
                        $arrRegionData[$k]['watch_time_summary'] = abs($value->sum_duration);
                        $arrRegionData[$k]['rating'] = number_format($value->average_rating,3);
                        $arrRegionData[$k]['watch_time'] = $this->getRegionWatchTime($value->sum_duration);
                    }
                }
            }

            //print_r($arrRegionData);
        }

        if(@$_GET['test']=='test'){
            // echo 'abcd';
            print_r($arrRegionData);exit;
        }

        return array(
            'status'=>true,
            'RegionData'=>$arrRegionData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
        
        

    }

    public function getAllDeviceOnlineEachRegion($requestCriteria){
        //print_r($requestCriteria);
        // $this->ci->load->library('user_agent');

        // if($this->ci->agent->platform() != 'iOS'){

        //     if(!isset($_GET['test'])){
        //         return [
        //         'status'=>false,
        //         'result_code'=>'-010',
        //         'result_desc'=>'The system is currently improving.'
        //         ];
        //     }
        // }else if($this->ci->agent->platform() == 'iOS'){

        // }

        /* check last updated record */

        if(!property_exists($requestCriteria, 'schedule_task')){ 

            // echo 'aaa';exit;

            $summary_view_all_type = $this->getSummaryViewersByType();
            $summary_view_all_type_totals = ($summary_view_all_type['Satellite']+$summary_view_all_type['Youtube']+$summary_view_all_type['InternetTV']);
            //print_r($summary_view_all_type);exit;

            if(property_exists($requestCriteria, 'select_type')){

                switch ($requestCriteria->select_type) {
                    case 'Satellite':
                        # code...
                        $getSummaryData = $this->ci->db->select('*')
                        ->from('online_each_region')
                        ->where('created !=','')
                        ->where('type','satellite')
                        ->limit(1)->get();

                        if($getSummaryData->num_rows() > 0){
                            $row = $getSummaryData->row();
                            $decode_response = json_decode($row->response);
                            $decode_response->{'SummaryViewerTotals'} = $summary_view_all_type_totals;
                            $decode_response->{'SummaryViewers'} = $summary_view_all_type;
                            echo json_encode($decode_response);exit;
                        }
                    break;
                    case 'Youtube':
                        $getSummaryData = $this->ci->db->select('*')
                        ->from('online_each_region')
                        ->where('created !=','')
                        ->where('type','youtube')
                        ->limit(1)->get();

                        if($getSummaryData->num_rows() > 0){
                            $row = $getSummaryData->row();
                            $decode_response = json_decode($row->response);                            
                            $decode_response->{'SummaryViewerTotals'} = $summary_view_all_type_totals;
                            $decode_response->{'SummaryViewers'} = $summary_view_all_type;
                            echo json_encode($decode_response);exit;
                        }
                    break;
                    
                    default:
                        $getSummaryData = $this->ci->db->select('*')
                        ->from('online_each_region')
                        ->where('created !=','')
                        ->where('type','satellite')
                        ->limit(1)->get();

                        if($getSummaryData->num_rows() > 0){
                            $row = $getSummaryData->row();
                            $decode_response = json_decode($row->response);                            
                            $decode_response->{'SummaryViewerTotals'} = $summary_view_all_type_totals;
                            $decode_response->{'SummaryViewers'} = $summary_view_all_type;
                            echo json_encode($decode_response);exit;
                        }
                    break;
                }


            }else{
                $GetByTable = $this->ci->db->select('*')
                ->from('online_each_region')
                ->where('created !=','')
                ->where('type','satellite')
                ->limit(1)
                ->get();

                if($GetByTable->num_rows() > 0){
                    $row = $GetByTable->row();
                    $decode_response = json_decode($row->response);
                    $decode_response->{'SummaryViewerTotals'} = $summary_view_all_type_totals;
                    $decode_response->{'SummaryViewers'} = $summary_view_all_type;
                    echo json_encode($decode_response);exit;
                }
            }

            




        }
        /* eof check last updated record */

        $arrData = array();
        


        //============= new query ===================
        
        $query = $this->getAllDeviceOnlineLastTwoHours();

        //print_r($query->result());exit;

        $all_province_region = array();
        $arr_province = array();
        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                // $device_data = $this->getDeviceAddressAndDeviceData(array(
                //     'devices_id'=>$value->devices_id
                // ));

                $device_addresses = $this->getAllDeviceAddressAndDeviceData(array(
                    'devices_id'=>$value->devices_id
                ));

                //print_r($device_data);

                if($device_addresses && $device_addresses->region_name){
                    array_push($all_province_region, $device_addresses->province_region);
                    array_push($arr_province, [
                        'id'=>$device_addresses->devices_id,
                        'chip_code'=>$device_addresses->ship_code,
                        'province_region'=>$device_addresses->province_region
                    ]);
                }

            }

            //exit;
        }

        //print_r($all_province_region);
        //exit;

        $all_province_region = array_unique($all_province_region);

        $arrReturn  = array();
        foreach ($all_province_region as $key => $value) {
            # code...
            $arrReturn[$key]['province_region'] = $value;
            $arrReturn[$key]['all_devices'] = array();
            foreach ($arr_province as $k => $v) {
                # code...
                if($v['province_region'] == $value){
                    array_push($arrReturn[$key]['all_devices'], $v['chip_code']);
                }

            }
        }

        //print_r($arrReturn);exit;

        //print_r($arrReturn);exit;


        if(count($arrReturn) > 0){
            foreach ($arrReturn as $key => $value) {
                # code...
                $arrReturn[$key]['sum_all_devices'] = count($value['all_devices']);
            }
        }

        //print_r($arrReturn);exit;

        $newArrReturn  = array();

        foreach ($arrReturn as $key => $value) {
            # code...
            array_push($newArrReturn, $value);
        }

        foreach ($newArrReturn as $key => $value) {
            # code...
            unset($newArrReturn[$key]['all_devices']);
        }
        

        return array(
            'status'=>true,
            'ReturnData'=>$newArrReturn,
            'AllDeviceRegistered'=>$this->getAllDeviceRegistered(),
            'result_code'=>'000',
            'result_desc'=>'Success'
        );

        // print_r($arrData);

        /* eof get all device and group by region*/
    }

    public function setAllDeviceOnlineEachRegion($requestCriteria){

        //============= new query ===================

        $arrReturn = [];
        
        $query = $this->getAllDeviceOnlineLastTwoHoursNewVersion();

        foreach ($query->result() as $key => $value) {
            # code...
            array_push($arrReturn, $value);
        }

        return array(
            'status'=>true,
            'ReturnData'=>$arrReturn,
            'AllDeviceRegistered'=>$this->getAllDeviceRegistered(),
            'result_code'=>'000',
            'result_desc'=>'Success'
        );


    }

    public function setAllDeviceOnlineEachRegionRV1($requestCriteria){
        //============= new query ===================

        $arrReturn = [];
        
        $query = $this->getAllDeviceOnlineLastTwoHoursNewVersionRV1();

        foreach ($query->result() as $key => $value) {
            # code...
            array_push($arrReturn, $value);
        }

        return array(
            'status'=>true,
            'ReturnData'=>$arrReturn,
            'AllDeviceRegistered'=>$this->getAllDeviceRegistered(),
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    public function getYoutubeAllDeviceOnlineEachRegion($requestCriteria){
        //print_r($requestCriteria);exit;

        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        $query = $this->ci->db->select('devices_id,updated')
        ->from('youtube_view_daily_devices')
        ->where('updated >=',$minusTwoHour->format('Y-m-d H:i:s'))
        ->get();

        //echo $this->ci->db->last_query();exit;

        $all_province_region = array();
        $arr_province = array();
        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                // $device_data = $this->getDeviceAddressAndDeviceData(array(
                //     'devices_id'=>$value->devices_id
                // ));

                $device_addresses = $this->getAllDeviceAddressAndDeviceData(array(
                    'devices_id'=>$value->devices_id
                ));

                //print_r($device_data);

                if($device_addresses && $device_addresses->region_name){
                    array_push($all_province_region, $device_addresses->province_region);
                    array_push($arr_province, [
                        'id'=>$device_addresses->devices_id,
                        'chip_code'=>$device_addresses->ship_code,
                        'province_region'=>$device_addresses->province_region
                    ]);
                }

            }

            //exit;
        }

        $all_province_region = array_unique($all_province_region);

        $arrReturn  = array();
        foreach ($all_province_region as $key => $value) {
            # code...
            $arrReturn[$key]['province_region'] = $value;
            $arrReturn[$key]['all_devices'] = array();
            foreach ($arr_province as $k => $v) {
                # code...
                if($v['province_region'] == $value){
                    array_push($arrReturn[$key]['all_devices'], $v['chip_code']);
                }

            }
        }

        if(count($arrReturn) > 0){
            foreach ($arrReturn as $key => $value) {
                # code...
                $arrReturn[$key]['sum_all_devices'] = count($value['all_devices']);
            }
        }


        $newArrReturn  = array();

        foreach ($arrReturn as $key => $value) {
            # code...
            array_push($newArrReturn, $value);
        }

        foreach ($newArrReturn as $key => $value) {
            # code...
            unset($newArrReturn[$key]['all_devices']);
        }

        return array(
            'status'=>true,
            'ReturnData'=>$newArrReturn,
            'AllDeviceRegistered'=>$this->getAllDeviceRegistered(),
            'result_code'=>'000',
            'result_desc'=>'Success'
        );


    }

    public function getYoutubeAllDeviceOnlineEachRegionNewVersion($requestCriteria){

        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        //print_r($minusTwoHour);exit;

        $query = $this->ci->db->select('count(youtube_rating_daily_devices.devices_id) as sum_all_devices,device_addresses.province_region')
        ->from('youtube_rating_daily_devices')
        ->join('device_addresses','youtube_rating_daily_devices.devices_id = device_addresses.devices_id')
        ->where('device_addresses.province_region !=','')
        ->where('youtube_rating_daily_devices.updated >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('youtube_rating_daily_devices.date',date('Y-m-d'))
        ->group_by('device_addresses.province_region')
        ->get();

        //echo $this->ci->db->last_query();exit;


            $mapShortProvinceRegion = [
                'Southern'=>'South',
                'Central'=>'Bangkok & Central',
                'NorthEastern'=>'Northeast',
                'Eastern'=>'East',
                'Northern'=>'North',
                'Western'=>'West'
            ];


        $arrReturn = [];
        if($query->num_rows() > 0){
            echo '<PRE>';
            print_r($query->result());exit();
            foreach ($query->result() as $key => $value) {
                # code...
                // array_push($arrReturn, $value);
                array_push($arrReturn, [
                    'sum_all_devices'=>$value->sum_all_devices,
                    'province_region'=>$value->province_region,
                    'name'=>$mapShortProvinceRegion[trim($value->province_region)]
                ]);
            }
        }
        
        return array(
            'status'=>true,
            'ReturnData'=>$arrReturn,
            'AllDeviceRegistered'=>$this->getAllDeviceRegistered(),
            'result_code'=>'000',
            'result_desc'=>'Success'
        );


        //echo $this->ci->db->last_query();exit;



    }

    public function getYoutubeAllDeviceOnlineEachRegionNewVersionRV1($requestCriteria){

        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        //print_r($minusTwoHour);exit;

        $query = $this->ci->db->select('count(youtube_rating_daily_devices.devices_id) as sum_all_devices,devices_addresses_tmp.province_region')
        ->from('youtube_rating_daily_devices')
        ->join('devices_addresses_tmp','youtube_rating_daily_devices.devices_id = devices_addresses_tmp.devices_tmp_id')
        ->where('devices_addresses_tmp.province_region !=','')
        ->where('youtube_rating_daily_devices.updated >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('youtube_rating_daily_devices.date',date('Y-m-d'))
        ->group_by('devices_addresses_tmp.province_region')
        ->get();

        $mapShortProvinceRegion = [
                'Bangkok'=>'Bangkok',
                'Southern'=>'South',
                'Central'=>'Central',
                'NorthEastern'=>'Northeast',
                'Eastern'=>'East',
                'Northern'=>'North',
                'Western'=>'West'
            ];


        $arrReturn = [];
        if($query->num_rows() > 0){

            foreach ($query->result() as $key => $value) {
                # code...
                // array_push($arrReturn, $value);
                array_push($arrReturn, [
                    'sum_all_devices'=>$value->sum_all_devices,
                    'province_region'=>$value->province_region,
                    'name'=>$mapShortProvinceRegion[trim($value->province_region)]
                ]);
            }
        }
        
        return array(
            'status'=>true,
            'ReturnData'=>$arrReturn,
            'AllDeviceRegistered'=>$this->getAllDeviceRegistered(),
            'result_code'=>'000',
            'result_desc'=>'Success'
        );



    }

    

    public function getAllDeviceOnlineEachProvinceByRegionName($requestCriteria){
        // print_r($requestCriteria);

        /* check request criteria */
        if(!property_exists($requestCriteria, 'province_region')){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found province region'
            );
        }
        /* eof check request criteria */

        /* get from table */
        if(!property_exists($requestCriteria, 'schedule_task')){

            if(property_exists($requestCriteria, 'select_type')){
                switch ($requestCriteria->select_type) {
                    case 'Satellite':
                        # code...
                        $querySummary = $this->ci->db->select('*')
                        ->from('online_each_region_by_name')
                        ->where('province_region',$requestCriteria->province_region)
                        ->where('type','satellite')
                        ->limit(1)->get();

                        if($querySummary->num_rows() > 0){
                            echo $querySummary->row()->response;exit;
                        }

                    break;
                    case 'Youtube':
                        $querySummary = $this->ci->db->select('*')
                        ->from('online_each_region_by_name')
                        ->where('province_region',$requestCriteria->province_region)
                        ->where('type','youtube')
                        ->limit(1)->get();

                        if($querySummary->num_rows() > 0){
                            echo $querySummary->row()->response;exit;
                        }
                    break;
                    
                    default:
                        $querySummary = $this->ci->db->select('*')
                        ->from('online_each_region_by_name')
                        ->where('province_region',$requestCriteria->province_region)
                        ->where('type','satellite')
                        ->limit(1)->get();

                        if($querySummary->num_rows() > 0){
                            echo $querySummary->row()->response;exit;
                        }
                    break;
                }

            }else{
                $queryFromTable = $this->ci->db->select('*')
                ->from('online_each_region_by_name')
                ->where('province_region',$requestCriteria->province_region)
                ->where('type','satellite')
                ->limit(1)
                ->get();

                if($queryFromTable->num_rows() > 0){
                    echo $queryFromTable->row()->response;exit;
                }
            }
        }
        /* get from table*/




        $arrData = array();
        $arrData['devices'] = array();
        $arrData['devices_by_province'] = array();

        $query = $this->getAllDeviceOnlineLastTwoHoursByProvinceRegion($requestCriteria->province_region);

        //print_r($query->result());exit;
        $arr_province = array();
        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $device_data = $this->getAllDeviceAddressAndDeviceData(array(
                    'devices_id'=>$value->devices_id
                ));

                //print_r($device_data);

                if($device_data && $device_data->region_name){
                    array_push($arr_province, $device_data->region_name);
                    array_push($arrData['devices'], array(
                        'id'=>$device_data->devices_id,
                        'chip_code'=>$device_data->ship_code,
                        'region_name'=>$device_data->region_name,
                        'city'=>$device_data->city,
                        'zipcode'=>$device_data->zip,
                        'latitude'=>$device_data->latitude,
                        'longitude'=>$device_data->longitude,
                        'province_region'=>$device_data->province_region
                    ));
                }else if(trim($device_data->country_name) != 'Thailand' && $device_data->country_name != ''){
                    //print_r($device_data);
                }
            }

            //exit;

        }
        $arr_province = array_unique($arr_province);

        //exit;
        //exit;
        //print_r($arr_province);exit;

        $arr_by_province = array();
        foreach ($arr_province as $key => $value) {
            # code...
            $arr_by_province[$key]['province_region'] = $value;
            $arr_by_province[$key]['devices'] = array();
            foreach ($arrData['devices'] as $k => $v) {
                # code...
                if($value == $v['region_name']){
                    array_push($arr_by_province[$key]['devices'], $v['chip_code']);
                }
            }
        }

        foreach ($arr_by_province as $key => $value) {
            # code...
            $arr_by_province[$key]['total_devices'] = count($value['devices']);
        }

        //$arrData['devices_by_province'] = $arr_by_province;
        //print_r($arr_by_province);exit;

        $new_device_by_province = array();
        $arr_total_devices = array();
        foreach ($arr_by_province as $key => $value) {
            # code...
            array_push($arr_total_devices, $value['total_devices']);
            array_push($new_device_by_province, $value);
        }

        array_multisort($arr_total_devices, SORT_DESC, $new_device_by_province);

        $arrData['devices_by_province'] = $new_device_by_province;


        // $query = $this->ci->db->select('*')
        // ->from('device_addresses')
        // ->join('devices','device_addresses.devices_id = devices.id')
        // ->where('device_addresses.province_region',$requestCriteria->province_region)
        // ->where('device_addresses.active',1)
        // ->get();

        // if($query->num_rows() > 0){
        //     foreach ($query->result() as $key => $value) {
        //         # code...
        //         array_push($arrData,array(
        //             'id'=>$value->devices_id,
        //             'chip_code'=>$value->ship_code,
        //             'region_name'=>$value->region_name,
        //             'city'=>$value->city,
        //             'zipcode'=>$value->zip,
        //             'latitude'=>$value->latitude,
        //             'longitude'=>$value->longitude,
        //             'province_region'=>$value->province_region
        //         ));
        //     }

        // }

        //print_r($arrData);

        foreach ($arrData['devices_by_province'] as $key => $value) {
            # code...
            if(strpos($value['province_region'], 'Changwat') !== false){
                $arrData['devices_by_province'][$key]['province_region'] = str_replace('Changwat ', '', $value['province_region']);
            }

            if(strpos($value['province_region'], 'Province') !== false){
                $arrData['devices_by_province'][$key]['province_region'] = str_replace(' Province', '', $value['province_region']);
            }

            unset($arrData['devices_by_province'][$key]['devices']);

        }
        unset($arrData['devices']);

        
        return array(
            'status'=>true,
            'ReturnData'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    public function setAllDeviceOnlineEachProvinceByRegionName($requestCriteria){

        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        $query = $this->ci->db->select('count('.$this->current_rating_data_daily_devices.'.devices_id) as total_devices,device_addresses.region_name')
        ->from($this->current_rating_data_daily_devices)
        ->join('device_addresses',$this->current_rating_data_daily_devices.'.devices_id = device_addresses.devices_id')
        ->where($this->current_rating_data_daily_devices.'.updated >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('device_addresses.country_name','Thailand')
        ->where('device_addresses.region_name !=','')
        ->group_by('device_addresses.region_name')
        ->order_by('total_devices','desc')
        ->get();


        $arrData = [];
        $arrData['Southern'] = [];
        $arrData['Bangkok'] = [];
        $arrData['Central'] = [];
        $arrData['NorthEastern'] = [];
        $arrData['Eastern'] = [];
        $arrData['Western'] = [];
        $arrData['Northern'] = [];

        foreach ($query->result() as $key => $value) {
            # code...
            $stdValue  = $value;
            $query_province_region = $this->ci->db->select('*')
            ->from('provinces')->where('ipstack_province_name',$value->region_name)->get();

            if($query_province_region->num_rows() > 0){
                $row_province_region = $query_province_region->row();

                $stdValue->{'province_id'} = $row_province_region->id;
                $stdValue->{'count_devices'} = $this->getTotalDevicesByProvinceId([
                    'province_id'=>$row_province_region->id
                ]);
                $stdValue->{'population'} = $row_province_region->population;
                array_push($arrData[$row_province_region->province_region], $stdValue);
            }

        }

        $arrReturn = [];

        
        foreach ($arrData as $key => $value) {
            //print_r($value);exit;
            # code...
            $arrReturn[$key]['status'] = true;
            $arrReturn[$key]['ReturnData']['devices_by_province'] = [];

            foreach ($value as $k => $v) {
                array_push($arrReturn[$key]['ReturnData']['devices_by_province'], [
                    'province_id'=>$v->province_id,
                    'total_devices'=>$v->total_devices,
                    'count_all_devices'=>$v->count_devices,
                    'population'=>$v->population,
                    'province_region'=>$this->getRegionNameWithoutOtherText($v->region_name)
                ]);
            }
            
            $arrReturn[$key]['result_code'] = '000';
            $arrReturn[$key]['result_desc'] = 'Success'; 

        }


        return $arrReturn;



    }

    public function setAllDeviceOnlineEachProvinceByRegionNameRV1($requestCriteria){

        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        $query = $this->ci->db->select('count('.$this->current_rating_data_daily_devices.'.devices_id) as total_devices,devices_addresses_tmp.region_name')
        ->from($this->current_rating_data_daily_devices)
        ->join('devices_addresses_tmp',$this->current_rating_data_daily_devices.'.devices_id = devices_addresses_tmp.devices_tmp_id')
        ->where($this->current_rating_data_daily_devices.'.updated >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('devices_addresses_tmp.country_name','Thailand')
        ->where('devices_addresses_tmp.region_name !=','')
        ->group_by('devices_addresses_tmp.region_name')
        ->order_by('total_devices','desc')
        ->get();

        if(@$_GET['test'] == 'test'){
            //print_r($this->ci->db->last_query());exit;
        }

        $arrData = [];
        $arrData['Southern'] = [];
        $arrData['Bangkok'] = [];
        $arrData['Central'] = [];
        $arrData['NorthEastern'] = [];
        $arrData['Eastern'] = [];
        $arrData['Western'] = [];
        $arrData['Northern'] = [];

        foreach ($query->result() as $key => $value) {
            # code...
            $stdValue  = $value;
            $query_province_region = $this->ci->db->select('*')
            ->from('provinces_rv1')->where('ipstack_province_name',$value->region_name)->get();

            if($query_province_region->num_rows() > 0){
                $row_province_region = $query_province_region->row();

                $stdValue->{'province_id'} = $row_province_region->id;
                $stdValue->{'count_devices'} = $this->getTotalDevicesByProvinceIdRV1([
                    'province_id'=>$row_province_region->id
                ]);
                $stdValue->{'population'} = $row_province_region->population;
                array_push($arrData[$row_province_region->province_region], $stdValue);
            }

        }

        $arrReturn = [];

        
        foreach ($arrData as $key => $value) {
            //print_r($value);exit;
            # code...
            $arrReturn[$key]['status'] = true;
            $arrReturn[$key]['ReturnData']['devices_by_province'] = [];

            foreach ($value as $k => $v) {
                array_push($arrReturn[$key]['ReturnData']['devices_by_province'], [
                    'province_id'=>$v->province_id,
                    'total_devices'=>$v->total_devices,
                    'count_all_devices'=>$v->count_devices,
                    'population'=>$v->population,
                    'province_region'=>$this->getRegionNameWithoutOtherText($v->region_name)
                ]);
            }
            
            $arrReturn[$key]['result_code'] = '000';
            $arrReturn[$key]['result_desc'] = 'Success'; 

        }


        return $arrReturn;





    }

    public function getYoutubeAllDeviceOnlineEachProvinceByRegionName($requestCriteria){
        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        $arrData = array();
        $arrData['devices'] = array();
        $arrData['devices_by_province'] = array();


        $query = $this->ci->db->select('youtube_view_daily_devices.devices_id,youtube_view_daily_devices.updated')
        ->from('youtube_view_daily_devices')
        ->join('device_addresses','youtube_view_daily_devices.devices_id = device_addresses.devices_id')
        ->where('youtube_view_daily_devices.updated >=',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('device_addresses.province_region',$requestCriteria->province_region)
        ->get();

        //print_r($query->result());exit;

        $arr_province = array();
        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $device_data = $this->getAllDeviceAddressAndDeviceData(array(
                    'devices_id'=>$value->devices_id
                ));

                //print_r($device_data);

                if($device_data && $device_data->region_name){
                    array_push($arr_province, $device_data->region_name);
                    array_push($arrData['devices'], array(
                        'id'=>$device_data->devices_id,
                        'chip_code'=>$device_data->ship_code,
                        'region_name'=>$device_data->region_name,
                        'city'=>$device_data->city,
                        'zipcode'=>$device_data->zip,
                        'latitude'=>$device_data->latitude,
                        'longitude'=>$device_data->longitude,
                        'province_region'=>$device_data->province_region
                    ));
                }else if(trim(@$device_data->country_name) != 'Thailand' && @$device_data->country_name != ''){
                    //print_r($device_data);
                }
            }

            //exit;

        }
        $arr_province = array_unique($arr_province);

         $arr_by_province = array();
        foreach ($arr_province as $key => $value) {
            # code...
            $arr_by_province[$key]['province_region'] = $value;
            $arr_by_province[$key]['devices'] = array();
            foreach ($arrData['devices'] as $k => $v) {
                # code...
                if($value == $v['region_name']){
                    array_push($arr_by_province[$key]['devices'], $v['chip_code']);
                }
            }
        }

        //print_r($arr_province);exit;

        foreach ($arr_by_province as $key => $value) {
            # code...
            $arr_by_province[$key]['total_devices'] = count($value['devices']);
        }


        $new_device_by_province = array();
        $arr_total_devices = array();
        foreach ($arr_by_province as $key => $value) {
            # code...
            array_push($arr_total_devices, $value['total_devices']);
            array_push($new_device_by_province, $value);
        }

        array_multisort($arr_total_devices, SORT_DESC, $new_device_by_province);

        $arrData['devices_by_province'] = $new_device_by_province;


        

        foreach ($arrData['devices_by_province'] as $key => $value) {
            # code...
            if(strpos($value['province_region'], 'Changwat') !== false){
                $arrData['devices_by_province'][$key]['province_region'] = str_replace('Changwat ', '', $value['province_region']);
            }

            if(strpos($value['province_region'], 'Province') !== false){
                $arrData['devices_by_province'][$key]['province_region'] = str_replace(' Province', '', $value['province_region']);
            }

            unset($arrData['devices_by_province'][$key]['devices']);

        }
        unset($arrData['devices']);

        
        return array(
            'status'=>true,
            'ReturnData'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );







    }

    public function setYoutubeAllDeviceOnlineEachProvinceByRegionName($requestCriteria){
        //print_r($requestCriteria);
        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        $arrData = array();
        $arrData['devices'] = array();
        $arrData['devices_by_province'] = array();

        $youtube_rating_table = 'youtube_rating_log_'.date('Y').'_'.date('n');


        

        $query = $this->ci->db->select('count(youtube_rating_daily_devices.devices_id) as total_devices,device_addresses.region_name')
        ->from('youtube_rating_daily_devices')
        ->join('device_addresses','youtube_rating_daily_devices.devices_id = device_addresses.devices_id')
        ->where('device_addresses.country_name','Thailand')
        ->where('youtube_rating_daily_devices.updated >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('device_addresses.region_name !=','')
        ->group_by('device_addresses.region_name')
        ->order_by('total_devices','desc')
        ->get();


        if(@$_GET['test']=='test'){
            //print_r($this->ci->db->last_query());exit;
        }


        // $query = $this->ci->db->select('count('.$youtube_rating_table.'.id) as total_devices,device_addresses.region_name')
        // ->from($youtube_rating_table)
        // ->join('device_addresses',''.$youtube_rating_table.'.devices_id = device_addresses.devices_id')
        // ->where('device_addresses.country_name','Thailand')




        //print_r($query->result());exit;
        $arrData = [];
       
        $arrData['Bangkok'] = [];
        $arrData['Central'] = [];
        $arrData['Northern'] = [];
        $arrData['NorthEastern'] = [];
        $arrData['Western'] = [];
        $arrData['Eastern'] = [];
        $arrData['Southern'] = [];
        
        

        foreach ($query->result() as $key => $value) {
            # code...

            $stdValue  = $value;
            $query_province_region = $this->ci->db->select('*')
            ->from('provinces')->where('ipstack_province_name',$value->region_name)->get();

            if($query_province_region->num_rows() > 0){

                //array_push($arrData[$query_province_region->row()->province_region], $value);

                $row_province_region = $query_province_region->row();

                $stdValue->{'province_id'} = $row_province_region->id;
                $stdValue->{'count_devices'} = $this->getTotalDevicesByProvinceId([
                    'province_id'=>$row_province_region->id
                ]);
                $stdValue->{'population'} = $row_province_region->population;
                array_push($arrData[$row_province_region->province_region], $stdValue);
            }
        }

        $arrReturn = [];

        //print_r($arrData);exit;

        foreach ($arrData as $key => $value) {
            //print_r($value);exit;
            # code...
            $arrReturn[$key]['status'] = true;
            $arrReturn[$key]['ReturnData']['devices_by_province'] = [];

            foreach ($value as $k => $v) {
                array_push($arrReturn[$key]['ReturnData']['devices_by_province'], [
                    'province_id'=>$v->province_id,
                    'total_devices'=>$v->total_devices,
                    'count_all_devices'=>$v->count_devices,
                    'population'=>$v->population,
                    'province_region'=>$this->getRegionNameWithoutOtherText($v->region_name)
                ]);
            }
            
            $arrReturn[$key]['result_code'] = '000';
            $arrReturn[$key]['result_desc'] = 'Success'; 

        }


        return $arrReturn;
    }

    public function setYoutubeAllDeviceOnlineEachProvinceByRegionNameRV1($requestCriteria){
        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        $arrData = array();
        $arrData['devices'] = array();
        $arrData['devices_by_province'] = array();

        $youtube_rating_table = 'youtube_rating_log_'.date('Y').'_'.date('n');


        

        $query = $this->ci->db->select('count(youtube_rating_daily_devices.devices_id) as total_devices,devices_addresses_tmp.region_name')
        ->from('youtube_rating_daily_devices')
        ->join('devices_addresses_tmp','youtube_rating_daily_devices.devices_id = devices_addresses_tmp.devices_tmp_id')
        ->where('devices_addresses_tmp.country_name','Thailand')
        ->where('youtube_rating_daily_devices.updated >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('devices_addresses_tmp.region_name !=','')
        ->group_by('devices_addresses_tmp.region_name')
        ->order_by('total_devices','desc')
        ->get();


        $arrData = [];
       
        $arrData['Bangkok'] = [];
        $arrData['Central'] = [];
        $arrData['Northern'] = [];
        $arrData['NorthEastern'] = [];
        $arrData['Western'] = [];
        $arrData['Eastern'] = [];
        $arrData['Southern'] = [];
        
        

        foreach ($query->result() as $key => $value) {
            # code...

            $stdValue  = $value;
            $query_province_region = $this->ci->db->select('*')
            ->from('provinces_rv1')->where('ipstack_province_name',$value->region_name)->get();

            if($query_province_region->num_rows() > 0){

                //array_push($arrData[$query_province_region->row()->province_region], $value);

                $row_province_region = $query_province_region->row();

                $stdValue->{'province_id'} = $row_province_region->id;
                $stdValue->{'count_devices'} = $this->getTotalDevicesByProvinceIdRV1([
                    'province_id'=>$row_province_region->id
                ]);
                $stdValue->{'population'} = $row_province_region->population;
                array_push($arrData[$row_province_region->province_region], $stdValue);
            }
        }

        $arrReturn = [];

        //print_r($arrData);exit;

        foreach ($arrData as $key => $value) {
            //print_r($value);exit;
            # code...
            $arrReturn[$key]['status'] = true;
            $arrReturn[$key]['ReturnData']['devices_by_province'] = [];

            foreach ($value as $k => $v) {
                array_push($arrReturn[$key]['ReturnData']['devices_by_province'], [
                    'province_id'=>$v->province_id,
                    'total_devices'=>$v->total_devices,
                    'count_all_devices'=>$v->count_devices,
                    'population'=>$v->population,
                    'province_region'=>$this->getRegionNameWithoutOtherText($v->region_name)
                ]);
            }
            
            $arrReturn[$key]['result_code'] = '000';
            $arrReturn[$key]['result_desc'] = 'Success'; 

        }


        return $arrReturn;



    }

    public function getAllProvinceRegion($requestCriteria){
        //print_r($requestCriteria);
        // $province_region = array(
        //     'Northern'=>'ภาคเหนือ',
        //     'Southern'=>'ภาคใต้',
        //     'Central'=>'ภาคกลาง',
        //     'Western'=>'ภาคตะวันตก',
        //     'Eastern'=>'ภาคตะวันออก',
        //     'NorthEastern'=>'ภาคตะวันออกเฉียงเหนือ'
        // );

        $province_region = array(
            'Bangkok'=>'Bangkok',
            'Central'=>'Central',
            'Northern'=>'Northern',
            'NorthEastern'=>'NorthEastern',           
            'Western'=>'Western',
            'Eastern'=>'Eastern',
            'Southern'=>'Southern'
            
        );

        return array(
            'status'=>true,
            'ProvinceRegion'=>$province_region,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    public function getChannelAdvertisementMobilePopup($requestCriteria){
        //print_r($requestCriteria);

        if(!property_exists($requestCriteria, 'channel_ascii_code') || !property_exists($requestCriteria, 'band_type')){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found channel ascii code'
            );
        }

        /* get channel data by ascii and band type */
        $channel_id = 0;

        switch ($requestCriteria->band_type) {
            case 'C':
                # code...
                $queryChannel = $this->ci->db->select('id,cband_ordinal')
                ->from('channels')
                ->where('cband_ordinal',$requestCriteria->channel_ascii_code)
                ->get();

                if($queryChannel->num_rows() > 0){
                    $channel_id = $queryChannel->row()->id;
                }
            break;
            case 'KU':

                $queryChannel = $this->ci->db->select('id,kuband_ordinal')
                ->from('channels')
                ->where('kuband_ordinal',$requestCriteria->channel_ascii_code)
                ->get();

                if($queryChannel->num_rows() > 0){
                    $channel_id = $queryChannel->row()->id;
                }

            break;            
            default:
                $queryChannel = $this->ci->db->select('id,cband_ordinal')
                ->from('channels')
                ->where('cband_ordinal',$requestCriteria->channel_ascii_code)
                ->get();

                if($queryChannel->num_rows() > 0){
                    $channel_id = $queryChannel->row()->id;
                }
            break;
        }


        //echo $channel_id;exit;
        if(!$channel_id){
            /* maybe write someting logs */
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found this channel.'
            );
        }


        /* get advertisement campaign */
        $queryAdvertisement = $this->ci->db->select('*')
        ->from('advertisement_campaigns')
        ->where('channels_id',$channel_id)
        ->where('start_datetime <= ',date('Y-m-d H:i:s'))
        ->where('end_datetime >= ',date('Y-m-d H:i:s'))
        ->where('active',1)
        ->order_by('id','desc')
        ->limit(1)
        ->get();

        //echo $this->ci->db->last_query();exit;
        /* eof get advertisement campaign */

        $ads_data = array();
        if($queryAdvertisement->num_rows() > 0){
            $row = $queryAdvertisement->row();
            $ads_data = array(
                'id'=>$row->id,
                'name'=>$row->name,
                'description'=>$row->description,
                'start_datetime'=>date('d/m/Y H:i',strtotime($row->start_datetime)),
                'end_datetime'=>date('d/m/Y H:i',strtotime($row->end_datetime)),
                'popup_image'=>base_url('uploaded/advertisement/popup_images/'.$row->id.'/'.$row->popup_image),
                'popup_image_redirect'=>$row->popup_image_url,
                'created'=>$row->created
            );


        }

        if(count($ads_data) <= 0){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found advertisement.'
            );
        }else{

            return array(
                'status'=>true,
                'AdsData'=>$ads_data,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
        }






    }

    public function setNewPassword($requestCriteria){
        //print_r($requestCriteria);

        /* check user exist */
        $user = new M_user($requestCriteria->user_id);
        if(!$user->id){
            return array(
                'status'=> false,
                'result_code'=>'-002',
                'result_desc'=>'Not found user.'
            );
        }
        /* eof check user exist */

        if($user->id == 178){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Do not permission.'
            );
        }

        $user->password = sha1($requestCriteria->new_password);
        if($user->save()){
            return array(
                'status'=>true,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
        }else{
            return array(
                'status'=>false,
                'result_code'=>'-004',
                'result_desc'=>'Something wrong when save data to system.'
            );
        }


    }

    public function getCompareManualChannelsMultiOptions($requestCriteria){
        //print_r($requestCriteria);

        $arrData = array();

        switch ($requestCriteria->range_type_value) {
            case 'today':
                # code...
                $chartformat = 'time';
                $arrData = $this->getGraphRatingToday($requestCriteria);
            break;

            case 'yesterday':
                $chartformat = 'time';

                $arrData = $this->getGraphRatingYesterdayNewVersion($requestCriteria);

                // if($this->checkMaintenanceDaily()){
                //     echo json_encode([
                //         'status'=>false,
                //         'result_code'=>'-002',
                //         'result_desc'=>'The system is in the process of summarizing data.Please open again at 01:00 AM'
                //     ]);
                // }else{
                //     $arrData = $this->getGraphRatingYesterdayNewVersion($requestCriteria);
                // }




            break;

            case 'last_seven_days':
                $chartformat = 'date';
                $arrData = $this->getGraphRatingLastSevenDays($requestCriteria);
            break;

            case 'last_thirty_days':
                $chartformat = 'date';
                $arrData = $this->getGraphRatingLastThirtyDays($requestCriteria);
            break;

            case 'this_month':
                $chartformat = 'date';
                $arrData = $this->getGraphRatingThisMonth($requestCriteria);
            break;

            case 'last_month':
                $chartformat = 'date';
                $arrData = $this->getGraphRatingLastMonth($requestCriteria);
            break;

            case 'custom':
                /* check date property and check another */

                if(!property_exists($requestCriteria, 'date') || $requestCriteria->date == ''){

                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'Not found date.'
                    );
                }
                $chartformat = 'time';
                // $arrData = $this->getGraphRatingCustom($requestCriteria);
                $date_criteria = new DateTime($requestCriteria->date);

                if($date_criteria->format('Y-m-d') == date('Y-m-d')){
                    $arrData = $this->getGraphRatingToday($requestCriteria);
                }else{
                    $arrData = $this->getGraphRatingCustomNewVersion($requestCriteria);
                }

            break; 

            case 'range':
                if(!property_exists($requestCriteria, 'start_date') || $requestCriteria->start_date == ''){
                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'Not found start date.'
                    );
                } 

                if(!property_exists($requestCriteria, 'end_date') || $requestCriteria->end_date == ''){
                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'Not found end date.'
                    );
                } 


                $chartformat = 'date';

                $arrData = $this->getGraphRatingByRange($requestCriteria);

            break;
            case 'range_24h':

                if(!property_exists($requestCriteria, 'start_date') || $requestCriteria->start_date == ''){
                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'Not found start date.'
                    );
                } 

                if(!property_exists($requestCriteria, 'end_date') || $requestCriteria->end_date == ''){
                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'Not found end date.'
                    );
                } 


                $chartformat = 'date';
                $arrData = $this->getGraphRatingByRange24H($requestCriteria);

                // echo json_encode([
                //     'data'=>$arrData
                // ]);exit;
                // print_r($arrData);exit;
                $xlsx_file_name = '';
                if(isset($_GET['export']) && $_GET['export'] == 'xlsx'){
                    $xlsx_file_name = $this->exportDataToXLSX24H([
                        'RatingData'=>$arrData
                    ]); 
                    return array(
                        'status'=>true,
                        'RatingData'=>$arrData,
                        'ChartFormat'=>$chartformat,
                        'xlsx_file'=>base_url('uploaded/xlsx/'.$xlsx_file_name),
                        'result_code'=>'000',
                        'result_desc'=>'Success'
                    );


                }



            break;
            
            default:
                # code...
            break;
        }
        $xlsx_file_name = '';
        if(isset($_GET['export']) && $_GET['export'] == 'xlsx'){
            $xlsx_file_name = $this->exportDataToXLSX([
                'RatingData'=>$arrData
            ]); 
            return array(
                'status'=>true,
                'RatingData'=>$arrData,
                'ChartFormat'=>$chartformat,
                'xlsx_file'=>base_url('uploaded/xlsx/'.$xlsx_file_name),
                'result_code'=>'000',
                'result_desc'=>'Success'
            );


        }else{
            return array(
                'status'=>true,
                'RatingData'=>$arrData,
                'ChartFormat'=>$chartformat,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );

        }


        



    }

    private function getChannelDataByChannelId($channel_id){
        //$channel = new M_channels($channel_id);

        $queryChannel = $this->ci->db->select('id,channel_name,logo,frq,sym,pol,vdo_pid,ado_pid,merge_s3remote_id')
        ->from('channels')
        ->where('id',$channel_id)
        ->get();


        if($queryChannel->num_rows() > 0){
            $channel = $queryChannel->row();
            return array(
                'channel_id'=>$channel->id,
                'channel_name'=>$channel->channel_name,
                'channel_logo'=>$this->getRatingChannelLogo([
                    'merge_s3remote_id'=>$channel->merge_s3remote_id
                ]).'?'.time(),
                'channel_frq'=>$channel->frq,
                'channel_sym'=>$channel->sym,
                'channel_pol'=>$channel->pol,
                'channel_vdo_pid'=>$channel->vdo_pid,
                'channel_ado_pid'=>$channel->ado_pid

            );

        }else{
            return array();
        }
    }

    public function getCompareManualChannelsSelectOptions($requestCriteria){
        //print_r($requestCriteria);
        $this->ci->load->config('api');

        $manual_range_list = $this->ci->config->item('manual_range_list');

        $arrRangeList = array();
        foreach ($manual_range_list as $key => $value) {
            # code...
            array_push($arrRangeList, array(
                'range_type_value'=>$key,
                'range_type_name'=>$value
            ));
        }
        return array(
            'status'=>true,
            'RangeList'=>$arrRangeList,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    public function getCompareChannelTopTwentyRatingNewVersion($requestCriteria){
        
        //print_r($requestCriteria);exit;
        /* check month and year criteria */
        if(!property_exists($requestCriteria, 'month') || $requestCriteria->month == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found month.'
            );
        }


        if(!property_exists($requestCriteria, 'year') || $requestCriteria->year == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found year.'
            );
        }
        /* eof check month and year criteria */

        $arrData = array();

        $fom = date('01-'.$requestCriteria->month.'-'.$requestCriteria->year);
        $lom = date('t-'.$requestCriteria->month.'-'.$requestCriteria->year);


        $start_datetime = new DateTime($fom);
        $end_datetime = new DateTime($lom);

        //if(@$_GET['test'] == 'test'){
            return $this->calculateTop20V2([
                'start_datetime'=>$start_datetime,
                'end_datetime'=>$end_datetime,
                'user_id'=>@$requestCriteria->user_id
            ]);
        //}



        $query_string = "select TOP 2400 channel_daily_rating_id,AVG(channel_daily_rating_logs.rating) as average_rating";
        $query_string .= " from channel_daily_rating_logs";
        $query_string .= " where created >= '".$start_datetime->format('Y-m-d H:i:s')."'";
        $query_string .= " and created <= '".$end_datetime->format('Y-m-d H:i:s')."'";
        $query_string .= " group by channel_daily_rating_id";
        $query_string .= " order by average_rating desc";

        //echo $query_string;exit;

        $query = $this->ci->db->query($query_string);

        $all_channels = array();
        $average_channel = array();
        $all_average_channel = array();

        $check_is_show_that_hide_by_user = $this->checkShowHideChannelsByUser([
            'user_id'=>@$requestCriteria->user_id
        ]);


        


        if($query->num_rows() > 0){

            /* get all channel not show on ranking  */
            if(!$check_is_show_that_hide_by_user){
                $not_show_on_ranking = $this->getAllChannelNotShowOnRanking();

            }
            /* eof get all channel not show on ranking */



            foreach ($query->result() as $key => $value) {
                # code...
                if(!$check_is_show_that_hide_by_user){
                    $queryChannelAndRatingDaily = $this->ci->db->select('*')
                    ->from('channel_daily_rating')
                    ->join('channels','channel_daily_rating.channels_id = channels.id')
                    ->where('channel_daily_rating.id',$value->channel_daily_rating_id)
                    ->where_not_in('channels.id',$not_show_on_ranking)
                    ->get();

                    if(@$_GET['test'] == 'where_not'){
                        echo $this->ci->db->last_query();exit;
                    }
                }else{
                    $queryChannelAndRatingDaily = $this->ci->db->select('*')
                    ->from('channel_daily_rating')
                    ->join('channels','channel_daily_rating.channels_id = channels.id')
                    ->where('channel_daily_rating.id',$value->channel_daily_rating_id)
                    ->get();
                }

                // echo $this->ci->db->last_query();exit;

                //print_r($queryChannelAndRatingDaily->row());
                if($queryChannelAndRatingDaily->num_rows() > 0){
                    $rowData = $queryChannelAndRatingDaily->row();

                    //print_r($rowData);exit;

                    array_push($all_channels, $rowData->channels_id);
                    array_push($all_average_channel, array(
                        'channels_id'=>$rowData->channels_id,
                        'rating'=>$value->average_rating
                    ));
                }


            }

            // print_r($all_average_channel);exit;
            // print_r($all_channels);exit;

            $all_channels = array_unique($all_channels);
            //print_r($all_channels);exit;

            $arr_summary = array();
            foreach ($all_channels as $key => $value) {
                # code...
                $arr_summary[$key]['channels_id'] = $value;
                $arr_summary[$key]['rating'] = array();
                foreach ($all_average_channel as $k => $v) {
                    # code...
                    if($value == $v['channels_id']){
                        array_push($arr_summary[$key]['rating'], $v['rating']);
                    }
                }

            }
            // print_r($arr_summary);exit;
            $ordinal_number = 1;
            //$count = 1;
            foreach ($arr_summary as $key => $value) {
                # code...
                if($ordinal_number <= 100){
                    $queryChannel = $this->ci->db->select('id,channel_name')
                    ->from('channels')
                    ->where('id',$value['channels_id'])
                    ->where('channel_extended_status',0)
                    ->get();

                    if($queryChannel->num_rows() > 0){
                        $rowChannel = $queryChannel->row();

                        array_push($arrData, array(
                            'ordinal'=>$ordinal_number,
                            'channel_id'=>$rowChannel->id,
                            'channel_name'=>$rowChannel->channel_name,
                            //'rating'=>number_format(max($value['rating']),2),
                            'rating'=>number_format(array_sum($value['rating']) / count(array_filter($value['rating'])),3),
                            'watch_time'=>$this->getWatchTimeByChannelAndDate(array(
                                'channels_id'=>$rowChannel->id,
                                'date_start'=>$start_datetime->format('Y-m-d'),
                                'date_end'=>$end_datetime->format('Y-m-d')
                            ))
                        ));

                        $ordinal_number++;
                    }


                }
            }
            //print_r($arrData);exit;



        }

        // usort($arrData, function($a, $b)
        // {
        //     return strcmp($a['rating'], $b['rating']);
        // });
        $rating = array();
        foreach ($arrData as $key => $row) {
            $rating[$key]  = $row['rating'];
        }

        array_multisort($rating, SORT_DESC, $arrData);

        $count = 1;
        foreach ($arrData as $key => $value) {
            # code...
            $arrData[$key]['ordinal'] = $count++;
        }

        // usort($myArray, function($a, $b) {
        //     return (int)$a['rating'] <=> (int)$b['rating'];
        // });
        // usort($arrData, array($this, "cmp"));
        //usort($your_data, "cmp");
        //print_r($arrData);exit;
        //rsort($arrData);

        //print_r($arrData);exit;


        return array(
            'status'=>true,
            'ChannelList'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );

        //exit;





    }

    public function getCompareChannelTopTwentyRatingNewVersionUnAuth($requestCriteria){
        //print_r($requestCriteria);exit;
        /* check month and year criteria */
        if(!property_exists($requestCriteria, 'month') || $requestCriteria->month == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found month.'
            );
        }


        if(!property_exists($requestCriteria, 'year') || $requestCriteria->year == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found year.'
            );
        }
        /* eof check month and year criteria */

        $arrData = array();

        $fom = date('01-'.$requestCriteria->month.'-'.$requestCriteria->year);
        $lom = date('t-'.$requestCriteria->month.'-'.$requestCriteria->year);


        $start_datetime = new DateTime($fom);
        $end_datetime = new DateTime($lom);

        $query_string = "select TOP 500 channel_daily_rating_id,AVG(channel_daily_rating_logs.rating) as average_rating";
        $query_string .= " from channel_daily_rating_logs";
        $query_string .= " where created >= '".$start_datetime->format('Y-m-d H:i:s')."'";
        $query_string .= " and created <= '".$end_datetime->format('Y-m-d H:i:s')."'";
        $query_string .= " group by channel_daily_rating_id";
        $query_string .= " order by average_rating desc";

        //echo $query_string;exit;

        $query = $this->ci->db->query($query_string);

        $all_channels = array();
        $average_channel = array();
        $all_average_channel = array();
        if($query->num_rows() > 0){
            $not_show_on_ranking = $this->getAllChannelNotShowOnRanking();
            foreach ($query->result() as $key => $value) {
                # code...

                


                if($not_show_on_ranking){
                    $queryChannelAndRatingDaily = $this->ci->db->select('*')
                    ->from('channel_daily_rating')
                    ->join('channels','channel_daily_rating.channels_id = channels.id')
                    ->where('channel_daily_rating.id',$value->channel_daily_rating_id)
                    ->where_not_in('channels.id',$not_show_on_ranking)
                    ->get();
                }else{
                    $queryChannelAndRatingDaily = $this->ci->db->select('*')
                    ->from('channel_daily_rating')
                    ->join('channels','channel_daily_rating.channels_id = channels.id')
                    ->where('channel_daily_rating.id',$value->channel_daily_rating_id)
                    ->get();
                }
                echo $this->ci->db->last_query();exit;

                //print_r($queryChannelAndRatingDaily->row());
                if($queryChannelAndRatingDaily->num_rows() > 0){
                    $rowData = $queryChannelAndRatingDaily->row();

                    //print_r($rowData);exit;

                    array_push($all_channels, $rowData->channels_id);
                    array_push($all_average_channel, array(
                        'channels_id'=>$rowData->channels_id,
                        'rating'=>$value->average_rating
                    ));
                }


            }

            // print_r($all_average_channel);exit;
            // print_r($all_channels);exit;

            $all_channels = array_unique($all_channels);
            //print_r($all_channels);exit;

            $arr_summary = array();
            foreach ($all_channels as $key => $value) {
                # code...
                $arr_summary[$key]['channels_id'] = $value;
                $arr_summary[$key]['rating'] = array();
                foreach ($all_average_channel as $k => $v) {
                    # code...
                    if($value == $v['channels_id']){
                        array_push($arr_summary[$key]['rating'], $v['rating']);
                    }
                }

            }

            // print_r(count($arr_summary));
            // print_r($arr_summary);exit;
            $ordinal_number = 1;
            //$count = 1;
            foreach ($arr_summary as $key => $value) {
                # code...
                if($ordinal_number <= 20){
                    $queryChannel = $this->ci->db->select('*')
                    ->from('channels')
                    ->where('id',$value['channels_id'])
                    ->get();

                    $rowChannel = $queryChannel->row();

                    array_push($arrData, array(
                        'ordinal'=>$ordinal_number,
                        'channel_id'=>$rowChannel->id,
                        'channel_name'=>$rowChannel->channel_name,
                        //'rating'=>number_format(max($value['rating']),2),
                        'rating'=>number_format(array_sum($value['rating']) / count(array_filter($value['rating'])),3),
                        'watch_time'=>$this->getWatchTimeByChannelAndDate(array(
                            'channels_id'=>$rowChannel->id,
                            'date_start'=>$start_datetime->format('Y-m-d'),
                            'date_end'=>$end_datetime->format('Y-m-d')
                        ))
                    ));

                    $ordinal_number++;


                }
            }
            //print_r($arrData);exit;



        }

        // usort($arrData, function($a, $b)
        // {
        //     return strcmp($a['rating'], $b['rating']);
        // });
        $rating = array();
        foreach ($arrData as $key => $row) {
            $rating[$key]  = $row['rating'];
        }

        array_multisort($rating, SORT_DESC, $arrData);

        $count = 1;
        foreach ($arrData as $key => $value) {
            # code...
            $arrData[$key]['ordinal'] = $count++;
        }

        // usort($myArray, function($a, $b) {
        //     return (int)$a['rating'] <=> (int)$b['rating'];
        // });
        // usort($arrData, array($this, "cmp"));
        //usort($your_data, "cmp");
        //print_r($arrData);exit;
        //rsort($arrData);

        //print_r($arrData);exit;

        return array(
            'status'=>true,
            'ChannelList'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );

        //exit;
    }

    public function getChannelGraphRatingByMonthYear($requestCriteria){
        //print_r($requestCriteria);exit;
        /* check channel object */
        if(!property_exists($requestCriteria, 'channel_id') || $requestCriteria->channel_id == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found channel id.'
            );
        }
        /* check month object */
        if(!property_exists($requestCriteria, 'month') || $requestCriteria->month == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found month.'
            );
        }
        /* eof check month object*/

        /* check year object */
        if(!property_exists($requestCriteria, 'year') || $requestCriteria->year == ''){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found year.'
            );
        }
        /* eof check year object*/


        /* check channel exist */
        $channel_data = $this->getChannelDataByChannelId($requestCriteria->channel_id);
        if(count($channel_data) <= 0){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found channel.'
            );
        }
        /* eof check channel exist */


        $arrReturn = array();

        $fom = date('01-'.$requestCriteria->month.'-'.$requestCriteria->year);
        $lastday = date('t',strtotime($fom));

        //echo $lastday;exit;
        $lom = date($lastday.'-'.$requestCriteria->month.'-'.$requestCriteria->year);

        //echo $lom;exit;


        $start_datetime = new DateTime($fom);
        $end_datetime = new DateTime($lom);
        $end_datetime->modify('+1 day');


        // $end_datetime = $end_datetime->modify( '+1 day' ); 

        $interval = DateInterval::createFromDateString('1 day');
        // $interval = new DateInterval('P1D');
        $period = new DatePeriod($start_datetime, $interval, $end_datetime);
        

        


       
        //print_r($channel_data);exit;
        $data_push = $this->getChannelDailyRatingByPeriod(array(
            'channels_id'=>$channel_data['channel_id'],
            'period'=>$period
        ));

        $data_genderage = $this->getChannelPieChartGenderAndAgeByMonthYear([
            'channels_id'=>$channel_data['channel_id'],
            'period'=>$period,
            'requestCriteria'=>$requestCriteria
        ]);



        // array_push($arrReturn,array(
        //     'channel_id'=>$channel_data['channel_id'],
        //     'channel_name'=>$channel_data['channel_name'],
        //     'channel_logo'=>$channel_data['channel_logo'],
        //     'rating_by_period'=>$data_push
        // ));

        $arrReturn['channel_id'] = $channel_data['channel_id'];
        $arrReturn['channel_name'] = $channel_data['channel_name'];
        $arrReturn['channel_logo'] = $channel_data['channel_logo'];
        $arrReturn['rating_by_period'] = $data_push;
        // $arrReturn['pie_chart_data'] = $data_genderage;
            

        //print_r($arrReturn);
        return array(
            'status'=>true,
            'RatingData'=>$arrReturn,
            'PieChartData'=>$data_genderage,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );


    }

    public function getS3RemoteChannelHits($requestCriteria){

        

        //print_r($requestCriteria);exit;
        //print_r($requestCriteria);
        $arrData = array();
        $current_time = new DateTime();
        $last_twenty = $current_time->sub(new DateInterval('PT5M'));
        $this_time = new DateTime();

        $start_datetime = $last_twenty;
        $end_datetime = $this_time;

        $query_string = "select TOP 80 channel_daily_rating_id,AVG(channel_daily_rating_logs.rating) as average_rating";
        $query_string .= " from channel_daily_rating_logs";
        $query_string .= " where created >= '".$start_datetime->format('Y-m-d H:i:s')."'";
        $query_string .= " and created <= '".$end_datetime->format('Y-m-d H:i:s')."'";
        $query_string .= " group by channel_daily_rating_id";
        $query_string .= " order by average_rating desc";

        //echo $query_string;exit;

        $query = $this->ci->db->query($query_string);

        $all_channels = array();
        $average_channel = array();
        $all_average_channel = array();
        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $queryChannelAndRatingDaily = $this->ci->db->select('*')
                ->from('channel_daily_rating')
                ->join('channels','channel_daily_rating.channels_id = channels.id')
                ->where('channel_daily_rating.id',$value->channel_daily_rating_id)
                ->where('channels.merge_s3remote_id !=',0)
                ->get();

                //print_r($queryChannelAndRatingDaily->row());
                if($queryChannelAndRatingDaily->num_rows() > 0){
                    $rowData = $queryChannelAndRatingDaily->row();

                    //print_r($rowData);exit;

                    array_push($all_channels, $rowData->channels_id);
                    array_push($all_average_channel, array(
                        'channels_id'=>$rowData->channels_id,
                        'rating'=>$value->average_rating
                    ));
                }


            }

            // print_r($all_average_channel);exit;
            // print_r($all_channels);exit;

            $all_channels = array_unique($all_channels);
            //print_r($all_channels);exit;

            $arr_summary = array();
            foreach ($all_channels as $key => $value) {
                # code...
                $arr_summary[$key]['channels_id'] = $value;
                $arr_summary[$key]['rating'] = array();
                foreach ($all_average_channel as $k => $v) {
                    # code...
                    if($value == $v['channels_id']){
                        array_push($arr_summary[$key]['rating'], $v['rating']);
                    }
                }

            }

            if(@$_GET['test'] == 'test'){
                print_r($arr_summary);exit;
            }
            $ordinal_number = 1;
            //$count = 1;
            foreach ($arr_summary as $key => $value) {
                # code...
                if($ordinal_number <= 20){
                    $queryChannel = $this->ci->db->select('*')
                    ->from('channels')
                    ->where('id',$value['channels_id'])
                    ->get();

                    $rowChannel = $queryChannel->row();
                    $s3channel_data = $this->getS3ChannelDataForChannelHits(array(
                        'tvchannels_id'=>$rowChannel->merge_s3remote_id
                    ));


                    $ascii_data = $this->getAsciiCodeForChannelHits(array(
                        'band_type'=>$requestCriteria->band_type,
                        'channel_data'=>$s3channel_data
                    ));

                    //print_r($s3channel_data);exit;

                    if($ascii_data['status']){
                        array_push($arrData, array(
                            'channel_id'=>$s3channel_data->id,
                            'channel_logo'=>$this->getChannelLogo($s3channel_data->logo),
                            'channel_name'=>$rowChannel->channel_name,
                            'channel_description'=>$rowChannel->channel_description,
                            'channel_ascii_code'=>(string)$ascii_data['ascii_code'],
                            'band_type'=>$requestCriteria->band_type,
                            'rating'=>@$value['rating'][0]
                        ));

                        $ordinal_number++;
                    }


                }
            }
            //print_r($arrData);exit;



        }

        return array(
            'status'=>true,
            'ChannelList'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );





        
    }

    public function getChannelPieChartGenderAndAgeByMonthYear($data = []){
        //print_r($requestCriteria);exit;

        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        // error_reporting(E_ALL);

        $arrPieChart = [];

        $fom = new DateTime('01-'.$data['requestCriteria']->month.'-'.$data['requestCriteria']->year);
        $channel_daily_devices_summary_table = 'channel_daily_devices_summary_'.$fom->format('Y').'_'.$fom->format('n');

        //echo $channel_daily_devices_summary_table;exit;

        $data['channel_daily_devices_summary_table'] = $channel_daily_devices_summary_table;
        $countDataSummaryByGender = $this->getCountDataSummaryByGender($data);

        $countDataSummaryByAge = $this->getCountDataSummaryByAge($data);
        //print_r($countDataSummaryByGender);exit;
        //print_r($countDataSummaryByAge);exit;



        $arrGender = [
            'Totals'=>$countDataSummaryByGender['count_all'],
            'Male'=>$countDataSummaryByGender['count_male'],
            'Female'=>$countDataSummaryByGender['count_female'],
            'NotSpecify'=>$countDataSummaryByGender['count_not_specify']
        ];

        //print_r($arrPieChart);exit;

        return [
            'GenderData'=>$arrGender,
            'AgeData'=>$countDataSummaryByAge
        ];        


    }

    public function getDeviceLocationByProvince($requestCriteria){
        // print_r($requestCriteria);exit;

        /* check province criteria */
        if(!property_exists($requestCriteria, 'province_id') || $requestCriteria->province_id == ''){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found province,Please assign province_id'
            ];
        }
        /* eof check province criteria */

        /* check type  */
        if(!property_exists($requestCriteria, 'type') || $requestCriteria->type == ''){
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found type,Please assign type (all or active)'
            ];
        }


        $this->checkReturnImmediatelyDevices([
            'requestCriteria'=>$requestCriteria
        ]);

        if(strtolower($requestCriteria->type) == 'all'){
            $query = $this->ci->db->select('device_addresses.devices_id,
                device_addresses.latitude,
                device_addresses.longitude,
                device_addresses.tmp_latitude,
                device_addresses.tmp_longitude,
                device_addresses.tmp_latlon_status')
            ->from('device_addresses')
            ->join('provinces','device_addresses.region_name = provinces.ipstack_province_name')
            ->where('provinces.id',$requestCriteria->province_id)
            ->where('device_addresses.latitude <>','0.000000')
            ->where('device_addresses.longitude <>','0.000000')
            ->where('device_addresses.latitude <>','0')
            ->where('device_addresses.longitude <>','0')
            ->get();

            //echo $this->ci->db->last_query();exit;
        }else if(strtolower($requestCriteria->type) == 'active'){

            $current_datetime = new DateTime();

            $current_datetime->sub(new DateInterval('PT5H'));


            $query = $this->ci->db->select(',
                device_addresses.devices_id,
                device_addresses.latitude,
                device_addresses.longitude,
                device_addresses.tmp_latitude,
                device_addresses.tmp_longitude,
                device_addresses.tmp_latlon_status')
            ->from('device_addresses')
            ->join('provinces','device_addresses.region_name = provinces.ipstack_province_name')
            ->join($this->current_rating_data_daily_devices,$this->current_rating_data_daily_devices.'.devices_id = device_addresses.devices_id')
            ->where('provinces.id',$requestCriteria->province_id)
            ->where($this->current_rating_data_daily_devices.'.updated >=',$current_datetime->format('Y-m-d H:i:s'))
            ->where('device_addresses.latitude <>','0.000000')
            ->where('device_addresses.longitude <>','0.000000')
            ->where('device_addresses.latitude <>','0')
            ->where('device_addresses.longitude <>','0')
            ->get();

            //echo $this->ci->db->last_query();exit;

        }

        $arrData = [];

        foreach ($query->result() as $key => $value) {
            # code...
            array_push($arrData, [
                'devices_id'=>$value->devices_id,
                'latitude'=>($value->tmp_latlon_status)?$value->tmp_latitude:$value->latitude,
                'longitude'=>($value->tmp_latlon_status)?$value->tmp_longitude:$value->longitude
            ]);
        }

        return [
            'status'=>true,
            'TotalDevices'=>$query->num_rows(),
            'Devices'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ];



    }

    public function getTVProgramRating($requestCriteria){
        //print_r($requestCriteria);exit;


        /* check channel program exist  */
        $queryCheck = $this->ci->db->select('count(channel_programs.id) as count_programs')
        ->from('channel_programs')
        ->where('channel_programs.channels_id',$requestCriteria->channels_id)
        ->group_by('channel_programs.channels_id')
        ->get();

        if($queryCheck->num_rows() > 0){
            $rowCheck = $queryCheck->row();

            if($rowCheck->count_programs <= 0){
                return [
                    'status'=>false,
                    'result_code'=>'-002',
                    'result_desc'=>'Not found channel program from this channel.'
                ];
            }
        }else{

            $queryCheckBaseon = $this->ci->db->select('count(id) as count_programs')
            ->from('channel_programs_baseon_date')
            ->where('channels_id',$requestCriteria->channels_id)
            ->group_by('channels_id')
            ->get();
            $rowCheckBase = $queryCheckBaseon->row();

            if($rowCheckBase->count_programs <= 0){
                return [
                    'status'=>false,
                    'result_code'=>'-002',
                    'result_desc'=>'Not found channel program from this channel.'
                ];
            }


            
        }
        /* eof check channel program exist  */




        /* check date criteria n format */
        $date_three_letter_format = $this->getDateDateThreeLetterFormat([
            'date'=>$requestCriteria->date
        ]);
        /* eof check criteria n format */


        /* get all tv program */
        $tv_programs = $this->getTVPrograms([
            'channels_id'=>$requestCriteria->channels_id,
            'date'=>$requestCriteria->date,
            'date_of_week'=>$date_three_letter_format
        ]);
        /* eof get all tv program */


        $arrReturn = [];

        foreach ($tv_programs as $key => $value) {
            # code...
            $start_time = $requestCriteria->date.' '.$value->start_time;
            $end_time = $requestCriteria->date.' '.$value->end_time;

            $start_datetime = new DateTime($start_time);
            $end_datetime = new DateTime($end_time);


            array_push($arrReturn, [
                'program_name'=>$value->name,
                'start_datetime'=>$start_datetime->format('Y-m-d H:i:s'),
                'end_datetime'=>$end_datetime->format('Y-m-d H:i:s'),
                'avg_rating'=>$this->getAVGRatingByProgramRank([
                    'channels_id'=>$requestCriteria->channels_id,
                    'date'=>$requestCriteria->date,
                    'start_datetime'=>$start_datetime,
                    'end_datetime'=>$end_datetime
                ]),
                'start_time'=>$start_datetime->format('H:i'),
                'end_time'=>$end_datetime->format('H:i')
            ]);
        }

        if(@$_GET['test'] == 'test'){
            //print_r($arrReturn);exit;
        }


        /* filter array and return average 0.00 with value */
            $newArr = $arrReturn;
            foreach ($arrReturn as $key => $value) {
                # code...

                $current_dt = new DateTime();
                $start_dt = new DateTime($value['start_datetime']);

                $arr_avg = [];
                if($value['avg_rating'] == '0.00' && $start_dt <= $current_dt){

                    if(isset($arrReturn[$key-1]['avg_rating']) && $arrReturn[$key-1]['avg_rating'] != '0.00'){
                        array_push($arr_avg, $arrReturn[$key-1]['avg_rating']);
                    }else{
                        (isset($arrReturn[$key-2]['avg_rating']) && $arrReturn[$key-2]['avg_rating'] != '0.00')?array_push($arr_avg, $arrReturn[$key-2]['avg_rating']):'';
                    }

                    if(isset($arrReturn[$key+1]['avg_rating']) && $arrReturn[$key+1]['avg_rating'] != '0.00'){
                        array_push($arr_avg, $arrReturn[$key+1]['avg_rating']);
                    }else{
                        (isset($arrReturn[$key+2]['avg_rating']) && $arrReturn[$key+2]['avg_rating'] != '0.00')?array_push($arr_avg, $arrReturn[$key+2]['avg_rating']):'';
                    }

                    //$a = array_filter($arr_avg);
                    $average = (count($arr_avg) > 0)?array_sum($arr_avg)/count($arr_avg):0;
                    $newArr[$key]['avg_rating'] = number_format($average,3);
                    

                }
            }

            /* filter time diff less or equal 15 minute */

            if(@$this->ci->input->get('test') == 'test'){
                //print_r($arrReturn);exit;
            }

                foreach ($arrReturn as $key => $value) {
                    # code...
                    $ar_avg = [];
                    $start_datetime = new DateTime($value['start_datetime']);
                    $end_datetime = new DateTime($value['end_datetime']);

                    $interval = $start_datetime->diff($end_datetime);

                    // print_r($interval);

                    if($interval->h <= 0 && $interval->i <= 15 && $value['avg_rating'] != '0.00'){
                        //print_r($value);
                        $keyMinus = ($key-1 <= 0)?0:$key-1;
                        
                        array_push($ar_avg, $newArr[$keyMinus]['avg_rating']);

                        if(isset($newArr[$key+1])){
                            array_push($ar_avg, $newArr[$key+1]['avg_rating']);
                        }

                        //print_r($ar_avg);

                        $average = (count($ar_avg) > 0)?array_sum($ar_avg)/count($ar_avg):0;
                        $newArr[$key]['avg_rating'] = number_format($average,3);


                    }
                }


            /*  eof filter time diff*/

        return [
            'status'=>true,
            'TVPrograms'=>$newArr,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ];




    }

    public function getCompareChannelByRegionRV1($requestCriteria){
        //print_r($requestCriteria);

        $arrRegion = array(
            'Northern',
            'NorthEastern',
            'Central',
            'Eastern',
            'Southern',
            'Western'
        );
        $arrRegionData = array(
            'Bangkok'=>array(
                'name'=>'Bangkok',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Bangkok'
            ),
            'Central'=>array(
                'name'=>'Central',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Central'
            ),
            'Northern'=>array(
                'name'=>'Northern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'North'
            ),
            'NorthEastern'=>array(
                'name'=>'NorthEastern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'Northeast'
            ),
            'Western' => array(
                'name'=>'Western',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'West'
            ),
            
            'Eastern' => array(
                'name'=>'Eastern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'East'
            ),
            'Southern' => array(
                'name'=>'Southern',
                'watch_time_array'=>array(),
                'watch_time_summary'=>0,
                'devices'=>array(),
                'total_devices'=>0,
                'short_name'=>'South'
            )
            
        );

        $user = new M_user();
        $user->where('id',$requestCriteria->user_id)
        ->get();


        $datetime_criteria = new DateTime('01-'.$requestCriteria->month.'-'.$requestCriteria->year);

        //print_r($datetime_criteria);exit;


        $channel_id = (property_exists($requestCriteria, 'channel_id') && $requestCriteria->channel_id != '')?$requestCriteria->channel_id:$user->owner_channel_id;


        $query = $this->ci->db->select('province_region,AVG(rating) as average_rating,SUM(sum_duration) as sum_duration')
        ->from('channel_audience_region_summary_rv1')
        ->where('MONTH(date)',$datetime_criteria->format('m'))
        ->where('YEAR(date)',$datetime_criteria->format('Y'))
        ->where('channels_id',$channel_id)
        ->group_by('province_region')
        ->get();

        
        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                foreach ($arrRegionData as $k => $v) {
                    # code...
                    if($value->province_region == $v['name']){
                        $arrRegionData[$k]['watch_time_summary'] = $value->sum_duration;
                        $arrRegionData[$k]['rating'] = number_format($value->average_rating,3);
                        $arrRegionData[$k]['watch_time'] = $this->getRegionWatchTime($value->sum_duration);
                    }
                }
            }

            //print_r($arrRegionData);
        }

        if(@$_GET['test']=='test'){
            print_r($arrRegionData);exit;
        }

        return array(
            'status'=>true,
            'RegionData'=>$arrRegionData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );



    }

    public function getAllProvinceRegionRV1($requestCriteria){

        $province_region = array(
            'Bangkok'=>'Bangkok',
            'Central'=>'Central',
            'Northern'=>'Northern',
            'NorthEastern'=>'NorthEastern',           
            'Western'=>'Western',
            'Eastern'=>'Eastern',
            'Southern'=>'Southern'
            
        );

        return array(
            'status'=>true,
            'ProvinceRegion'=>$province_region,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }

    public function getAllDeviceOnlineEachRegionRV1($requestCriteria){
        //print_r($requestCriteria);
            $summary_view_all_type = $this->getSummaryViewersByTypeRV1();
            $summary_view_all_type_totals = ($summary_view_all_type['Satellite']+$summary_view_all_type['Youtube']+$summary_view_all_type['InternetTV']);
            //print_r($summary_view_all_type);exit;

            if(property_exists($requestCriteria, 'select_type')){

                switch ($requestCriteria->select_type) {
                    case 'Satellite':
                        # code...
                        $getSummaryData = $this->ci->db->select('*')
                        ->from('online_each_region_rv1')
                        ->where('created !=','')
                        ->where('type','satellite')
                        ->limit(1)->get();

                        if($getSummaryData->num_rows() > 0){
                            $row = $getSummaryData->row();
                            $decode_response = json_decode($row->response);
                            $decode_response->{'SummaryViewerTotals'} = $summary_view_all_type_totals;
                            $decode_response->{'SummaryViewers'} = $summary_view_all_type;
                            echo json_encode($decode_response);exit;
                        }
                    break;
                    case 'Youtube':
                        $getSummaryData = $this->ci->db->select('*')
                        ->from('online_each_region_rv1')
                        ->where('created !=','')
                        ->where('type','youtube')
                        ->limit(1)->get();

                        if($getSummaryData->num_rows() > 0){
                            $row = $getSummaryData->row();
                            $decode_response = json_decode($row->response);                            
                            $decode_response->{'SummaryViewerTotals'} = $summary_view_all_type_totals;
                            $decode_response->{'SummaryViewers'} = $summary_view_all_type;
                            echo json_encode($decode_response);exit;
                        }
                    break;
                    
                    default:
                        $getSummaryData = $this->ci->db->select('*')
                        ->from('online_each_region_rv1')
                        ->where('created !=','')
                        ->where('type','satellite')
                        ->limit(1)->get();

                        if($getSummaryData->num_rows() > 0){
                            $row = $getSummaryData->row();
                            $decode_response = json_decode($row->response);                            
                            $decode_response->{'SummaryViewerTotals'} = $summary_view_all_type_totals;
                            $decode_response->{'SummaryViewers'} = $summary_view_all_type;
                            echo json_encode($decode_response);exit;
                        }
                    break;
                }


            }else{
                $GetByTable = $this->ci->db->select('*')
                ->from('online_each_region_rv1')
                ->where('created !=','')
                ->where('type','satellite')
                ->limit(1)
                ->get();

                if($GetByTable->num_rows() > 0){
                    $row = $GetByTable->row();
                    $decode_response = json_decode($row->response);
                    $decode_response->{'SummaryViewerTotals'} = $summary_view_all_type_totals;
                    $decode_response->{'SummaryViewers'} = $summary_view_all_type;
                    echo json_encode($decode_response);exit;
                }
            }
    }

    public function getAllDeviceOnlineEachProvinceByRegionNameRV1($requestCriteria){
        /* check request criteria */
        if(!property_exists($requestCriteria, 'province_region')){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Not found province region'
            );
        }
        /* eof check request criteria */

        if(property_exists($requestCriteria, 'select_type')){
                switch ($requestCriteria->select_type) {
                    case 'Satellite':
                        # code...
                        $querySummary = $this->ci->db->select('*')
                        ->from('online_each_region_by_name_rv1')
                        ->where('province_region',$requestCriteria->province_region)
                        ->where('type','satellite')
                        ->limit(1)->get();

                        if($querySummary->num_rows() > 0){
                            echo $querySummary->row()->response;exit;
                        }

                    break;
                    case 'Youtube':
                        $querySummary = $this->ci->db->select('*')
                        ->from('online_each_region_by_name_rv1')
                        ->where('province_region',$requestCriteria->province_region)
                        ->where('type','youtube')
                        ->limit(1)->get();

                        if($querySummary->num_rows() > 0){
                            echo $querySummary->row()->response;exit;
                        }
                    break;
                    
                    default:
                        $querySummary = $this->ci->db->select('*')
                        ->from('online_each_region_by_name_rv1')
                        ->where('province_region',$requestCriteria->province_region)
                        ->where('type','satellite')
                        ->limit(1)->get();

                        if($querySummary->num_rows() > 0){
                            echo $querySummary->row()->response;exit;
                        }
                    break;
                }

            }else{
                $queryFromTable = $this->ci->db->select('*')
                ->from('online_each_region_by_name_rv1')
                ->where('province_region',$requestCriteria->province_region)
                ->where('type','satellite')
                ->limit(1)
                ->get();

                if($queryFromTable->num_rows() > 0){
                    echo $queryFromTable->row()->response;exit;
                }
            }


    }

    public function setUserChannelCompare($requestCriteria){

        // check user 
        if(property_exists($requestCriteria, 'user_id') && $requestCriteria->user_id != ""){

            // 
            $queryUser = $this->ci->db->select('id,active')
            ->from('user')
            ->where('id',$requestCriteria->user_id)
            ->where('active',1)
            ->get();

            if($queryUser->num_rows() <= 0){
                return [
                    'status'=>false,
                    'result_code'=>'-002',
                    'result_desc'=>'Not found user'
                ];
            }
        }else{
            return [
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'Request not found.'
            ];
        }


        // check already in user_compare_channel
        $queryCheckUser = $this->ci->db->select('*')
        ->from('user_channels_compare')
        ->where('user_id',$requestCriteria->user_id)
        ->get();

        if($queryCheckUser->num_rows() <= 0){ // if not found insert new record
            $this->ci->db->insert('user_channels_compare',[
                'user_id'=>$requestCriteria->user_id,
                'save_channels_compare'=>json_encode($requestCriteria->arr_channels),
                'created'=>date('Y-m-d H:i:s')
            ]);


        }else{ // if found just update
            $this->ci->db->update('user_channels_compare',[
                'save_channels_compare'=>json_encode($requestCriteria->arr_channels),
                'updated'=>date('Y-m-d H:i:s')
            ],['user_id'=>$requestCriteria->user_id]);


        }

        return [
            'status'=>true,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ];



    }


    private function getChannelLogo($image_logo){
        switch (ENVIRONMENT) {
            case 'development':
                // return 'http://psi.development/uploaded/tv/logo/'.$image_logo.'?'.time();
            return 'http://psi.development/uploaded/tv/logo/'.$image_logo;
            break;
            case 'testing':

            break;
            case 'production':
                // return 'http://apiservice.psisat.com/uploaded/tv/logo/'.$image_logo.'?'.time();
                // return 'http://apiservice.psisat.com/uploaded/tv/logo/'.$image_logo;
                return 'http://s3remoteservice.psi.co.th/tv/uploaded/tv/logo/'.$image_logo;
            break;
            
            default:
                # code...
            break;
        }
    }


    private function getLastUpdateByChannel($channels_id){
        // $query = $this->ci->db->select('id,channels_id,created')
        // ->from($this->current_rating_data_table)
        // ->where('channels_id',$channels_id)
        // ->order_by('created','desc')
        // ->limit(1)
        // ->get();
        $query = $this->ci->db->select('channels_id,updated,date')
        ->from('rating_data_daily')
        ->where('channels_id',$channels_id)
        ->where('date',date('Y-m-d H:i:s'))
        ->get();



        if($query->num_rows() > 0){
            $row = $query->row();
            $updated_datetime = new DateTime($row->updated);

            return $updated_datetime->format('d').' '.__($updated_datetime->format('M'),'default').' '.$updated_datetime->format('Y').' '.$updated_datetime->format('H:i:s');
        }else{
            return false;
        }
    }

    private function getChannelCountView($channels_id){


        $query = $this->ci->db->select('*')
        ->from('rating_data_daily')
        ->where('date',date('Y-m-d'))
        ->where('channels_id',$channels_id)
        ->get();

        if($query->num_rows() > 0){
            return $query->row()->reach_devices;
        }else{
            return 0;
        }
    }

    private function getChannelCountViewByMinusTime($channels_id){
        $this->ci->load->config('api');
        $channel_countview_minus_time = $this->ci->config->item('channel_countview_minus_time');


        /* last check point */
        $checkpoint = new DateTime();
        $checkpoint->sub(new DateInterval($channel_countview_minus_time));
        /* eof get last check point */

        //print_r($checkpoint);exit;

        $query = $this->ci->db->select('*')
        ->from('rating_data_daily')
        ->where('date',date('Y-m-d'))
        ->where('channels_id',$channels_id)
        ->get();

        $row_data_daily = $query->row();


        $queryDailyDevice =  $this->ci->db->select('*')
        ->from($this->current_rating_data_daily_devices)
        ->where('rating_data_daily_id',@$row_data_daily->id)
        ->where('updated >= ',$checkpoint->format('Y-m-d H:i:s'))
        ->get();

        // echo $this->ci->db->last_query();exit;



        if($query->num_rows() > 0){
            return $queryDailyDevice->num_rows();
        }else{
            return 0;
        }

    }

    private function getChannelRating($data = array()){

        $channels_id = $data['channel_id'];
        $reach_devices = $data['reach_devices'];
        /* all device */
        $queryAllDevice = $this->ci->db->select('count(id) as all_devices')
        ->from('devices')
        ->get();

        $count_devices = $queryAllDevice->row()->all_devices;

        return number_format(($reach_devices/$count_devices)*100,3);


        //echo $count_devices;exit;
    }

    private function getMainPageData($channels_id){
        $this->ci->load->helper([
            'our'
        ]);
        $channel_count_views = $this->getChannelCountViewByMinusTime($channels_id);

        $channel_daily_rating_data = $this->getChannelDailyRatingData(array(
            'channel_id'=>$channels_id
        )); 
        return array(
            'count_views'=>$channel_count_views,
            'count_views_show'=>thousandsCurrencyFormat($channel_count_views),
            'rating'=>$this->getChannelRating(array(
                    'channel_id'=>$channels_id,
                    'reach_devices'=>$this->getChannelReachDevicesNewVersion(['channels_id'=>$channels_id])
            )),
            'lastest_updated'=>($this->getLastUpdateByChannel($channels_id))?$this->getLastUpdateByChannel($channels_id):'-',
            'all_channel'=>$channel_daily_rating_data['total_channel'],
            'ordinal'=>$channel_daily_rating_data['channel_ordinal']
        );
    }

    private function getWatchTimeByChannelAndDate($data = array()){

        $str_query = "select channels_id,SUM(sum_seconds) as total_seconds";
        $str_query .= " from rating_data_daily";
        $str_query .= " where date >= '".$data['date_start']."'";
        $str_query .= " and date <= '".$data['date_end']."'";
        $str_query .= " and channels_id = '".$data['channels_id']."'";
        $str_query .= " group by channels_id";

        $query = $this->ci->db->query($str_query);

        // if(@$_GET['test']){
        //     print_r($this->ci->db->last_query());exit;
        // }

        if($query->num_rows() > 0){
            $total_seconds = $query->row()->total_seconds;
            $hours = floor($total_seconds / 3600);
            $minutes = floor(($total_seconds / 60) % 60);
            $seconds = $total_seconds % 60;

            return $hours.' h '.$minutes.' m '.$seconds.' s';
        }else{
            return "-";
        }

        

        

    }

    private function getAnnualRatingByChannel($data = array()){

        $arrAnnualData = array();

        $this_year = date('Y');
        $this_month = date('m');

        for ($i=1; $i <= $this_month ; $i++) { 
            # code...
            $month_cal = ($i<10)?'0'.$i:$i;

            array_push($arrAnnualData, array(
                'month'=>$month_cal,
                'month_name'=> $this->getMonthThaiByKey($month_cal),
                'rating'=>number_format($this->getMonthlyRatingByChannel(array(
                    'channel_id'=>$data['channel_id'],
                    'month'=>$month_cal,
                    'year'=>$this_year
                )),3)
            )); 

        }
        return $arrAnnualData;

    }

    private function getMonthlyRatingByChannel($data = array()){

            $arrRating = array();
            $query = $this->ci->db->select('channel_daily_rating_logs.channel_daily_rating_id,AVG(channel_daily_rating_logs.rating) as average_rating')
            ->from('channel_daily_rating_logs')
            ->join('channel_daily_rating','channel_daily_rating.id = channel_daily_rating_logs.channel_daily_rating_id')
            ->where('MONTH(channel_daily_rating_logs.created)',$data['month'])
            ->where('YEAR(channel_daily_rating_logs.created)',$data['year'])
            ->where('channel_daily_rating.channels_id',$data['channel_id'])
            ->group_by('channel_daily_rating_logs.channel_daily_rating_id')
            ->get();

            if($query->num_rows() <= 0){
                return 0;
            }else if($query->num_rows() == 1){
                return $query->row()->average_rating;
            }else{
                foreach ($query->result() as $k => $v) {
                    # code...
                    array_push($arrRating, $v->average_rating);
                }
                $a = array_filter($arrRating);
                $average = array_sum($a)/count($a);
                return $average;
            }


    }

    private function getMonthThaiByKey($month_txt){
        $arrMonth = array(
                "01" => "มกราคม",
                "02" => "กุมภาพันธ์",
                "03" => "มีนาคม",
                "04" => "เมษายน",
                "05" => "พฤษภาคม",
                "06" => "มิถุนายน",
                "07" => "กรกฎาคม",
                "08" => "สิงหาคม",
                "09" => "กันยายน",
                "10" => "ตุลาคม",
                "11" => "พฤศจิกายน",
                "12" => "ธันวาคม"
        );

        return $arrMonth[$month_txt];

    }

    private function getChannelDailyRatingData($data = array()){
        $arrReturn = array(
            'channel_ordinal'=>0,
            'total_channel'=>0
        );  
        $query = $this->ci->db->select('*')
        ->from('channel_daily_rating')
        ->where('date',date('Y-m-d'))
        ->where('rating !=',0)
        ->order_by('rating','desc')
        ->get();

        //echo $this->ci->db->last_query();exit;

        if($query->num_rows() > 0){
            $arrReturn['total_channel'] = $query->num_rows();

            $arrChannel = array();
            foreach ($query->result() as $key => $value) {
                # code...   
                array_push($arrChannel, $value->channels_id);

            }

            $key = array_search($data['channel_id'], $arrChannel);


            if(@$_GET['test'] == 'test'){
                print_r($key);exit;
            }
            if(is_numeric($key)){
                $arrReturn['channel_ordinal'] = $key+1;
            }
            
            //print_r($arrChannel);
        }
        return $arrReturn;   
    }

    private function getRegionRating($data = array()){

        $queryAllDevice = $this->ci->db->select('count(id) as all_devices')
        ->from('devices')
        ->get();

        $count_devices = $queryAllDevice->row()->all_devices;

        return number_format(($data['total_devices']/$count_devices)*100,3);

    }

    private function getRegionWatchTime($total_seconds){

            $hours = abs(floor($total_seconds / 3600));
            $minutes = abs(floor(($total_seconds / 60) % 60));
            $seconds = abs($total_seconds % 60);

            return $hours.' h '.$minutes.' m '.$seconds.' s';

    }

    private function setCurrentDataTable(){
        $this->current_rating_data_table = 'rating_data_'.date('Y').'_'.date('n');
        $this->current_rating_data_daily_devices = 'rating_data_daily_devices_'.date('Y').'_'.date('n');
    }

    private function getDeviceAddressAndDeviceData($data = array()){
        $query = $this->ci->db->select('device_addresses.id,
            device_addresses.devices_id,
            devices.ship_code,
            devices.id,
            device_addresses.active,
            device_addresses.province_region,
            device_addresses.active')
        ->from('device_addresses')
        ->join('devices','device_addresses.devices_id = devices.id')
        ->where('device_addresses.devices_id',$data['devices_id'])
        ->where('device_addresses.province_region != ','')
        ->where('device_addresses.active',1)
        ->get();

        // echo $this->ci->db->last_query();exit;

        if($query->num_rows() > 0){
            $row = $query->row();
            return array(
                'id'=>$row->devices_id,
                'chip_code'=>$row->ship_code,
                'province_region'=>$row->province_region
            );
        }else{
            return false;
        }

    }

    private function getAllDeviceAddressAndDeviceData($data = array()){
        $query = $this->ci->db->select('device_addresses.id,
            device_addresses.devices_id,
            devices.ship_code,
            devices.id,
            device_addresses.active,
            device_addresses.province_region,
            device_addresses.active,
            device_addresses.region_name,
            device_addresses.city,
            device_addresses.zip,
            device_addresses.latitude,
            device_addresses.longitude')
        ->from('device_addresses')
        ->join('devices','device_addresses.devices_id = devices.id')
        ->where('device_addresses.devices_id',$data['devices_id'])
        ->where('device_addresses.province_region != ','')
        ->where('device_addresses.active',1)
        ->get();

        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }

    }

    private function getAllDeviceOnlineLastTwoHours(){
        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        //print_r($minusTwoHour);exit;
        $query = $this->ci->db->select($this->current_rating_data_table.'.devices_id')
        ->from($this->current_rating_data_table)
        ->where($this->current_rating_data_table.'.created >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->group_by($this->current_rating_data_table.'.devices_id')
        ->get();

        //echo $this->ci->db->last_query();exit;

        return $query;

    }

    private function getAllDeviceOnlineLastTwoHoursNewVersion(){
        $current_datetime = new DateTime();

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        $query = $this->ci->db->select('count('.$this->current_rating_data_daily_devices.'.devices_id) as sum_all_devices,device_addresses.province_region')
        ->from($this->current_rating_data_daily_devices)
        ->join('device_addresses',$this->current_rating_data_daily_devices.'.devices_id = device_addresses.devices_id')
        ->where($this->current_rating_data_daily_devices.'.updated >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('device_addresses.province_region !=','')
        ->group_by('device_addresses.province_region')
        ->get();

        //print_r($this->ci->db->last_query());exit;

        return $query;
    }

    private function getAllDeviceOnlineLastTwoHoursNewVersionRV1(){
        $current_datetime = new DateTime();

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        $query = $this->ci->db->select('count('.$this->current_rating_data_daily_devices.'.devices_id) as sum_all_devices,devices_addresses_tmp.province_region')
        ->from($this->current_rating_data_daily_devices)
        ->join('devices_addresses_tmp',$this->current_rating_data_daily_devices.'.devices_id = devices_addresses_tmp.devices_tmp_id')
        ->where($this->current_rating_data_daily_devices.'.updated >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('devices_addresses_tmp.province_region !=','')
        ->group_by('devices_addresses_tmp.province_region')
        ->get();
        return $query;
    }



    private function getAllDeviceOnlineLastTwoHoursByProvinceRegion($province_region){

        $current_datetime = new DateTime();
        // $minusTwoHour = $current_datetime->sub(new DateInterval('PT2H'));

        $minusTwoHour = $current_datetime->sub(new DateInterval('PT5H'));

        //print_r($minusTwoHour);exit;
        $query = $this->ci->db->select($this->current_rating_data_table.'.devices_id')
        ->from($this->current_rating_data_table)
        ->join('device_addresses',$this->current_rating_data_table.'.devices_id = device_addresses.devices_id')
        ->where($this->current_rating_data_table.'.created >= ',$minusTwoHour->format('Y-m-d H:i:s'))
        ->where('device_addresses.province_region',$province_region)
        ->group_by($this->current_rating_data_table.'.devices_id')
        ->get();

       // echo $this->ci->db->last_query();exit;

        return $query;
    }

    private function getGraphRatingToday($requestCriteria){

        //print_r($requestCriteria);exit;
        /* get channel daily rating by channel id desc */
        $arrReturn = array();
        foreach ($requestCriteria->channels as $key => $value) {
            # code...
            /* get channel daily rating id */
            $query = $this->ci->db->select('id,channels_id,date')
            ->from('channel_daily_rating')
            ->where('channels_id',$value)
            ->where('date',date('Y-m-d'))
            ->get();
            /* eof get channel daily rating */
            if($query->num_rows() > 0){
                $row = $query->row();
                $channel_data = $this->getChannelDataByChannelId($row->channels_id);
                $data_push = $this->getChannelDailyRatingByTwentyFourHours(array(
                    'channel_daily_rating_id'=>$row->id,
                    'date'=>date('Y-m-d')
                ));
                array_push($arrReturn,array(
                    'channel_id'=>$row->channels_id,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'period_txt'=>$this->getGraphPeriodTxtShow([
                        'type'=>'period',
                        'start_date'=>date('Y-m-d')
                    ]),
                    'rating_by_period'=>$data_push,
                    
                ));
            }else{
                $channel_data = $this->getChannelDataByChannelId($value);
                array_push($arrReturn, array(
                    'channel_id'=>$value,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'period_txt'=>$this->getGraphPeriodTxtShow([
                        'type'=>'period',
                        'start_date'=>date('Y-m-d')
                    ]),
                    'rating_by_period'=>$this->getChannelDailyRatingByTwentyFourHoursEmpty(array(
                        'date'=>date('Y-m-d')
                    ))
                ));


            }

        }

        //print_r($arrReturn);exit;
        return $arrReturn;
    }

    private function getGraphRatingYesterday($requestCriteria){

        //print_r($requestCriteria);exit;
        $yesterday = new DateTime();
        $yesterday->sub(new DateInterval('P1D'));
        /* get channel daily rating by channel id desc */
        $arrReturn = array();
        foreach ($requestCriteria->channels as $key => $value) {
            # code...
            /* get channel daily rating id */
            $query = $this->ci->db->select('id,channels_id,date')
            ->from('channel_daily_rating')
            ->where('channels_id',$value)
            ->where('date',$yesterday->format('Y-m-d'))
            ->get();
            /* eof get channel daily rating */
            if($query->num_rows() > 0){
                $row = $query->row();
                $channel_data = $this->getChannelDataByChannelId($row->channels_id);
                $data_push = $this->getChannelDailyRatingByTwentyFourHours(array(
                    'channel_daily_rating_id'=>$row->id,
                    'date'=>$yesterday->format('Y-m-d')
                ));
                array_push($arrReturn,array(
                    'channel_id'=>$row->channels_id,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'period_txt'=>$this->getGraphPeriodTxtShow([
                        'type'=>'period',
                        'start_date'=>$yesterday->format('Y-m-d')
                    ]),
                    'rating_by_period'=>$data_push
                ));
            }else{
                $channel_data = $this->getChannelDataByChannelId($value);
                array_push($arrReturn, array(
                    'channel_id'=>$value,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'period_txt'=>$this->getGraphPeriodTxtShow([
                        'type'=>'period',
                        'start_date'=>$yesterday->format('Y-m-d')
                    ]),
                    'rating_by_period'=>$this->getChannelDailyRatingByTwentyFourHoursEmpty(array(
                        'date'=>$yesterday->format('Y-m-d')
                    ))
                ));
            }

        }

        //print_r($arrReturn);exit;
        return $arrReturn;



    }

    private function getGraphRatingYesterdayNewVersion($requestCriteria){
        //print_r($requestCriteria);exit;
        $yesterday = new DateTime();
        $yesterday->sub(new DateInterval('P1D'));
        /* get channel daily rating by channel id desc */
        $arrReturn = array(); 

        if(date('Y-m-d') == '2019-08-31'){
            $yesterday = new DateTime('2019-08-29');
        }

        foreach ($requestCriteria->channels as $key => $value) {

            $queryData = $this->ci->db->select('*')
            ->from('channel_daily_period_summary')
            ->where('channels_id',$value)
            ->where('date',$yesterday->format('Y-m-d'))
            ->get();

            $channel_data = $this->getChannelDataByChannelId($value);

            if($queryData->num_rows() > 0){
                //$row = $queryData->row();
                

                array_push($arrReturn,array(
                    'channel_id'=>$value,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'period_txt'=>$this->getGraphPeriodTxtShow([
                        'type'=>'period',
                        'start_date'=>$yesterday->format('Y-m-d')
                    ]),
                    'rating_by_period'=>$this->getChannelPeriodSummary([
                        'result_data'=>$queryData->result()
                    ])
                ));
            }else{
                array_push($arrReturn, array(
                    'channel_id'=>$value,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'period_txt'=>$this->getGraphPeriodTxtShow([
                        'type'=>'period',
                        'start_date'=>$yesterday->format('Y-m-d')
                    ]),
                    'rating_by_period'=>$this->getChannelDailyRatingByTwentyFourHoursEmpty(array(
                        'date'=>$yesterday->format('Y-m-d')
                    ))
                    
                ));


            }


        }
        return $arrReturn;



    }

    private function getGraphRatingLastSevenDays($requestCriteria){

        $last_seven_days = new DateTime();
        $last_seven_days->sub(new DateInterval('P7D'));

        $begin = new DateTime($last_seven_days->format('Y-m-d H:i:s'));
        $end = new DateTime();

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        //print_r($interval);exit;
        //print_r($interval);exit;
        //print_r($period);

        $arrReturn = array();
        foreach ($requestCriteria->channels as $key => $value) {


           
                $channel_data = $this->getChannelDataByChannelId($value);

                //print_r($channel_data);exit;

                /* older version */
                // $data_push = $this->getChannelDailyRatingByPeriod(array(
                //         'channels_id'=>$value,
                //         'period'=>$period
                // ));

                $data_push = $this->getChannelDailyDateSummary([
                    'channels_id'=>$value,
                    'period'=>$period
                ]);


                $count_data_push = count($data_push);
                $last_day = $data_push[$count_data_push-1]['date'];
                $last_date = new DateTime($last_day);
                //print_r($count_data_push);exit;

                array_push($arrReturn,array(
                        'channel_id'=>$channel_data['channel_id'],
                        'channel_name'=>$channel_data['channel_name'],
                        'channel_logo'=>$channel_data['channel_logo'],
                        'period_txt'=>$this->getGraphPeriodTxtShow([
                            'type'=>'rank',
                            'start_date'=>$begin->format('Y-m-d'),
                            'end_date'=>$last_date->format('Y-m-d')
                        ]),
                        'rating_by_period'=>$data_push
                        
                ));
            

        }

        return $arrReturn;

        //exit;

        //print_r($last_seven_days->format('Y-m-d'));exit;

    }

    private function getGraphRatingLastThirtyDays($requestCriteria){

                $last_seven_days = new DateTime();
        $last_seven_days->sub(new DateInterval('P30D'));

        $begin = new DateTime($last_seven_days->format('Y-m-d H:i:s'));
        $end = new DateTime();

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $arrReturn = array();
        foreach ($requestCriteria->channels as $key => $value) {


           
                $channel_data = $this->getChannelDataByChannelId($value);

                //print_r($channel_data);exit;
                // $data_push = $this->getChannelDailyRatingByPeriod(array(
                //         'channels_id'=>$value,
                //         'period'=>$period
                // ));

                $data_push = $this->getChannelDailyDateSummary([
                    'channels_id'=>$value,
                    'period'=>$period
                ]);

                $count_data_push = count($data_push);
                $last_day = $data_push[$count_data_push-1]['date'];
                $last_date = new DateTime($last_day);

                array_push($arrReturn,array(
                        'channel_id'=>$channel_data['channel_id'],
                        'channel_name'=>$channel_data['channel_name'],
                        'channel_logo'=>$channel_data['channel_logo'],
                        'period_txt'=>$this->getGraphPeriodTxtShow([
                            'type'=>'rank',
                            'start_date'=>$begin->format('Y-m-d'),
                            'end_date'=>$last_date->format('Y-m-d')
                        ]),
                        'rating_by_period'=>$data_push
                        
                ));
            

        }

        return $arrReturn;

        //exit;

        //print_r($last_seven_days->format('Y-m-d'));exit;


    }

    private function getGraphRatingThisMonth($requestCriteria){


        $fom = date('01-m-Y',strtotime('this month'));

        //echo $fom;exit;
        $begin = new DateTime($fom);
        $end = new DateTime();

        // print_r($begin);exit;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);



        $arrReturn = array();
        foreach ($requestCriteria->channels as $key => $value) {


           
                $channel_data = $this->getChannelDataByChannelId($value);

                //print_r($channel_data);exit;
                // $data_push = $this->getChannelDailyRatingByPeriod(array(
                //         'channels_id'=>$value,
                //         'period'=>$period
                // ));
                $data_push = $this->getChannelDailyDateSummary([
                    'channels_id'=>$value,
                    'period'=>$period
                ]);

                $count_data_push = count($data_push);
                $last_day = $data_push[$count_data_push-1]['date'];
                $last_date = new DateTime($last_day);

                array_push($arrReturn,array(
                        'channel_id'=>$channel_data['channel_id'],
                        'channel_name'=>$channel_data['channel_name'],
                        'channel_logo'=>$channel_data['channel_logo'],
                        'period_txt'=>$this->getGraphPeriodTxtShow([
                            'type'=>'rank',
                            'start_date'=>$begin->format('Y-m-d'),
                            'end_date'=>$last_date->format('Y-m-d')
                        ]),
                        'rating_by_period'=>$data_push
                        
                ));
            

        }

        return $arrReturn;

        //exit;

        //print_r($last_seven_days->format('Y-m-d'));exit;



    } 

    private function getGraphRatingByRange($requestCriteria){
        $begin = new DateTime($requestCriteria->start_date);
        $end = new DateTime($requestCriteria->end_date); 

        $end = $end->modify( '+1 day' ); 

        //$interval = DateInterval::createFromDateString('1 day');
        $interval = new DateInterval('P1D');
        $period = new DatePeriod($begin, $interval, $end); 

        $arrReturn = array(); 

        foreach ($requestCriteria->channels as $key => $value) {  

                $channel_data = $this->getChannelDataByChannelId($value); 

                $data_push = $this->getChannelDailyDateSummary([
                    'channels_id'=>$value,
                    'period'=>$period
                ]);

                $count_data_push = count($data_push);
                $last_day = $data_push[$count_data_push-1]['date'];
                $last_date = new DateTime($last_day);

                array_push($arrReturn,array(
                        'channel_id'=>$channel_data['channel_id'],
                        'channel_name'=>$channel_data['channel_name'],
                        'channel_logo'=>$channel_data['channel_logo'],
                        'period_txt'=>$this->getGraphPeriodTxtShow([
                            'type'=>'rank',
                            'start_date'=>$begin->format('Y-m-d'),
                            'end_date'=>$last_date->format('Y-m-d')
                        ]),
                        'rating_by_period'=>$data_push
                        
                ));

        } 

        return $arrReturn;


    }

    private function getGraphRatingByRange24H($requestCriteria){

        $begin = new DateTime($requestCriteria->start_date);
        $end = new DateTime($requestCriteria->end_date); 

        $end = $end->modify( '+1 day' ); 

        //$interval = DateInterval::createFromDateString('1 day');
        $interval = new DateInterval('P1D');
        $period = new DatePeriod($begin, $interval, $end); 

        $arrReturn = array(); 

        foreach ($requestCriteria->channels as $key => $value) {  

            $channel_data = $this->getChannelDataByChannelId($value);
            array_push($arrReturn,array(
                        'channel_id'=>$value,
                        'channel_name'=>$channel_data['channel_name'],
                        'channel_logo'=>$channel_data['channel_logo'],
                        'period_txt'=>$this->getGraphPeriodTxtShow([
                            'type'=>'period',
                            'start_date'=>$begin->format('Y-m-d')
                        ]),
                        'rating_by_period'=>[],
                        
            ));

            foreach ($period as $dt) {
                # code...

                /* get channel daily rating id */
                $query = $this->ci->db->select('id,channels_id,date')
                ->from('channel_daily_rating')
                ->where('channels_id',$value)
                ->where('date',$dt->format('Y-m-d'))
                ->get();
                /* eof get channel daily rating */
                if($query->num_rows() > 0){
                    $row = $query->row();                    
                    $data_push = $this->getChannelDailyRatingByTwentyFourHours(array(
                        'channel_daily_rating_id'=>$row->id,
                        'date'=>$dt->format('Y-m-d')
                    ));
                    array_push($arrReturn[$key]['rating_by_period'], $data_push);
                    
                }


            }


                // $channel_data = $this->getChannelDataByChannelId($value); 

                // $data_push = $this->getChannelDailyDateSummary([
                //     'channels_id'=>$value,
                //     'period'=>$period
                // ]);

                // $count_data_push = count($data_push);
                // $last_day = $data_push[$count_data_push-1]['date'];
                // $last_date = new DateTime($last_day);

                // array_push($arrReturn,array(
                //         'channel_id'=>$channel_data['channel_id'],
                //         'channel_name'=>$channel_data['channel_name'],
                //         'channel_logo'=>$channel_data['channel_logo'],
                //         'period_txt'=>$this->getGraphPeriodTxtShow([
                //             'type'=>'rank',
                //             'start_date'=>$begin->format('Y-m-d'),
                //             'end_date'=>$last_date->format('Y-m-d')
                //         ]),
                //         'rating_by_period'=>$data_push
                        
                // ));

        } 

        return $arrReturn;
    }

    private function getGraphRatingLastMonth($requestCriteria){
        $fom = date('01-m-Y',strtotime('last month'));
        $lom = date('t-m-Y',strtotime($fom));
        


        //print_r($fom);
        //print_r($lom);exit;
        $begin = new DateTime($fom);
        $end = new DateTime($lom);

        $end = $end->modify( '+1 day' ); 

        //$interval = DateInterval::createFromDateString('1 day');
        $interval = new DateInterval('P1D');
        $period = new DatePeriod($begin, $interval, $end);

        $arrReturn = array();
        foreach ($requestCriteria->channels as $key => $value) {


           
                $channel_data = $this->getChannelDataByChannelId($value);

                //print_r($channel_data);exit;
                // $data_push = $this->getChannelDailyRatingByPeriod(array(
                //         'channels_id'=>$value,
                //         'period'=>$period
                // ));
                $data_push = $this->getChannelDailyDateSummary([
                    'channels_id'=>$value,
                    'period'=>$period
                ]);

                $count_data_push = count($data_push);
                $last_day = $data_push[$count_data_push-1]['date'];
                $last_date = new DateTime($last_day);

                array_push($arrReturn,array(
                        'channel_id'=>$channel_data['channel_id'],
                        'channel_name'=>$channel_data['channel_name'],
                        'channel_logo'=>$channel_data['channel_logo'],
                        'period_txt'=>$this->getGraphPeriodTxtShow([
                            'type'=>'rank',
                            'start_date'=>$begin->format('Y-m-d'),
                            'end_date'=>$last_date->format('Y-m-d')
                        ]),
                        'rating_by_period'=>$data_push
                        
                ));
            

        }

        return $arrReturn;

    }

    private function getGraphRatingCustom($requestCriteria){

        //print_r($requestCriteria);exit;
        $date_criteria = new DateTime($requestCriteria->date);
        /* get channel daily rating by channel id desc */
        $arrReturn = array();
        foreach ($requestCriteria->channels as $key => $value) {
            # code...
            /* get channel daily rating id */
            $query = $this->ci->db->select('id,channels_id,date')
            ->from('channel_daily_rating')
            ->where('channels_id',$value)
            ->where('date',$date_criteria->format('Y-m-d'))
            ->get();
            /* eof get channel daily rating */
            if($query->num_rows() > 0){
                $row = $query->row();
                $channel_data = $this->getChannelDataByChannelId($row->channels_id);
                $data_push = $this->getChannelDailyRatingByTwentyFourHours(array(
                    'channel_daily_rating_id'=>$row->id,
                    'date'=>$date_criteria->format('Y-m-d')
                ));
                array_push($arrReturn,array(
                    'channel_id'=>$row->channels_id,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'period_txt'=>$this->getGraphPeriodTxtShow([
                        'type'=>'period',
                        'start_date'=>$yesterday->format('Y-m-d')
                    ]),
                    'rating_by_period'=>$data_push
                    
                ));
            }

        }

        //print_r($arrReturn);exit;
        return $arrReturn;


    }



    private function getChannelDailyRatingByTwentyFourHours($data = array()){
        $this->ci->load->config('api');

        //echo 'eeee';exit;
        //print_r($this->ci->config->item('arr_twenty_four_hours'));exit;
        //$query = $this->ci->db->select('*')
        $arrData = array();
        $now_datetime = new DateTime();
        foreach ($this->ci->config->item('arr_twenty_four_hours') as $key => $value) {
            # code...
            $twenty_four_datetime = new DateTime($data['date'].' '.$value['start_time']);


            if($twenty_four_datetime <= $now_datetime){
                $arrData[$key]['date'] = $data['date'];
                $arrData[$key]['time'] = $value['start_time'];
                $arrData[$key]['arr_rating'] = array();

                $query = $this->ci->db->select('id,channel_daily_rating_id,rating,created')
                ->from('channel_daily_rating_logs')
                ->where('channel_daily_rating_id',$data['channel_daily_rating_id'])
                ->where('created >= ',$data['date'].' '.$value['start_time'])
                ->where('created <= ',$data['date'].' '.$value['end_time'])
                ->get();

                if($query->num_rows() > 0){
                    foreach ($query->result() as $k => $v) {
                        # code...
                        array_push($arrData[$key]['arr_rating'], $v->rating);

                    }
                }
            }

        }

        //print_r($arrData);
        $arrReturn = array();

        foreach ($arrData as $key => $value) {

            $a = array();
            $average = 0;
            /* average array */

            if(count($value['arr_rating']) > 0){
                $a = array_filter($value['arr_rating']);
                $average = array_sum($a)/count($a);
            }
            # code...
            $arrReturn[$key]['date'] = $value['date'];
            $arrReturn[$key]['time'] = $value['time'];
            $arrReturn[$key]['rating'] = number_format($average,3);
            
        }

       return $arrReturn;

    }

    private function getChannelDailyRatingByTwentyFourHoursEmpty($data = array()){
        $this->ci->load->config('api');
        //echo 'eeee';exit;
        //print_r($this->ci->config->item('arr_twenty_four_hours'));exit;
        //$query = $this->ci->db->select('*')
        $arrData = array();
        $now_datetime = new DateTime();
        foreach ($this->ci->config->item('arr_twenty_four_hours') as $key => $value) {
            # code...
            $twenty_four_datetime = new DateTime($data['date'].' '.$value['start_time']);


            if($twenty_four_datetime <= $now_datetime){
                $arrData[$key]['date'] = $data['date'];
                $arrData[$key]['time'] = $value['start_time'];
                $arrData[$key]['rating'] = number_format(0,3);

            }

        }
        return $arrData;

    }

    private function getChannelDailyRatingByPeriod($data = array()){

        $today = new DateTime();
        $arrData = array();
        foreach ($data['period'] as $key => $value) {
            # code...
            //print_r($value->format('Y-m-d'));
            if($value->format('Y-m-d H:i:s') <= $today->format('Y-m-d H:i:s')){
                $arrData[$key]['date'] = $value->format('d-m-Y');
                $arrData[$key]['time'] = "";
                /* get rating channel daily rating first */
                $query = $this->ci->db->select('id,channels_id,date')
                ->from('channel_daily_rating')
                ->where('channels_id',$data['channels_id'])
                ->where('date',$value->format('Y-m-d'))
                ->get();

                if($query->num_rows() > 0){
                    $queryLogs = $this->ci->db->select('AVG(rating) as average_rating')
                    ->from('channel_daily_rating_logs')
                    ->where('channel_daily_rating_id',$query->row()->id)
                    ->group_by('channel_daily_rating_id')
                    ->get();

                    $row = $queryLogs->row();
                    $arrData[$key]['rating'] = (@$row->average_rating)?(string)@$row->average_rating:(string)0;


                }else{
                    $arrData[$key]['rating'] = (string)0;
                }
            }
            
        }

        return $arrData;


    }

    private function getChannelDailyDateSummary($data = []){

        $today = new DateTime();
        $arrData = array();
        foreach ($data['period'] as $key => $value) {
            # code...
            //print_r($value->format('Y-m-d'));
            if($value->format('Y-m-d H:i:s') <= $today->format('Y-m-d H:i:s')){
                $arrData[$key]['date'] = $value->format('d-m-Y');
                $arrData[$key]['time'] = "";
                /* get rating channel daily rating first */

                $querySummary = $this->ci->db->select('*')
                ->from('channel_daily_date_summary')
                ->where('channels_id',$data['channels_id'])
                ->where('date',$value->format('Y-m-d'))
                ->get();

                if($querySummary->num_rows() > 0){
                    $row = $querySummary->row();
                    $arrData[$key]['rating'] = ($row->rating)?(string)number_format($row->rating,3):(string)0;


                }else{
                    $arrData[$key]['rating'] = (string)0;

                }

            }
        }

        return $arrData;


    }

    private function getChannelRelatedCategory($channels_id){
        $arrReturn = array();
        $query = $this->ci->db->select('id,channel_categories_id')
        ->from('channels')
        ->where('id',$channels_id)
        ->get();

        if($query->num_rows() > 0){
                $row = $query->row();
                array_push($arrReturn, $row->id);


                // 340 = PSI Saradee
                // 463 = CH3
                // 416 = CH7
                // 286 = Shop Channel

                if($row->channel_categories_id == 12){
                    $arrShoppingChannel = [
                        278,280,284,287,282,286
                    ];

                    if(in_array($row->id, $arrShoppingChannel)){
                        $keyIndex = array_search($row->id, $arrShoppingChannel);
                        unset($arrShoppingChannel[$keyIndex]);
                    }

                    $count = 0;
                    foreach ($arrShoppingChannel as $key => $value) {
                        # code...
                        if($count <= 5){
                            array_push($arrReturn, $value);
                        }
                        $count += 1;
                    }


                }else if($row->id != '340' && $row->id != '463' && $row->id != '416'){
                    /* get other channel has same category */
                    $queryChannelCategories = $this->ci->db->select('id,channel_categories_id')
                    ->from('channels')
                    ->where('channel_categories_id',$row->channel_categories_id)
                    ->where('id !=',$row->id)
                    ->order_by('id')
                    ->limit(5)
                    ->get();

                    if($queryChannelCategories->num_rows() > 0){
                        foreach ($queryChannelCategories->result() as $key => $value) {
                            # code...
                            array_push($arrReturn, $value->id);
                        }
                    }
                }else if($row->id == '463'){
                    array_push($arrReturn, 416);
                    array_push($arrReturn, 270);
                    array_push($arrReturn, 264);
                    array_push($arrReturn, 273);
                    array_push($arrReturn, 272);
                }else if($row->id == '416'){
                    array_push($arrReturn, 463);
                    array_push($arrReturn, 270);
                    array_push($arrReturn, 264);
                    array_push($arrReturn, 273);
                    array_push($arrReturn, 272);
                }else{

                    array_push($arrReturn, 416);
                    array_push($arrReturn, 463);
                    array_push($arrReturn, 270);
                    array_push($arrReturn, 264);
                    array_push($arrReturn, 273);

                }

        }
        return $arrReturn;
    }

    private function getS3ChannelDataForChannelHits($data = array()){

        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->getS3ChannelUrl().'welcome/getChannelDataById?tvchannels_id='.$data['tvchannels_id'],
            CURLOPT_TIMEOUT=>10000
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        //echo $resp;
        // Close request to clear up some resources
        curl_close($curl);

        

        $decode_data = json_decode($resp);

        if($decode_data->status){
            return $decode_data->ChannelData;
        }else{
            return false;
        }





    }

    private function getS3ChannelUrl(){
        switch (ENVIRONMENT) {
            case 'development':
                return 'http://s3remoteservice.development/';
            break;
            case 'testing':

            break;
            case 'production':
                return 'http://s3remoteservice.psi.co.th/';
            break;
            
            default:
                # code...
                break;
        }
    }

    private function getAsciiCodeForChannelHits($data = array()){
        $ascii_code = 0;
        $channel_data = $data['channel_data'];
        
        if($channel_data->band_type == 'ALL' || $channel_data->band_type == $data['band_type']){

            switch ($data['band_type']) {
                case 'C':
                    $ascii_code = $channel_data->cband_ordinal;
                break;

                case 'KU':
                    $ascii_code = $channel_data->kuband_ordinal;
                break;
                
                default:
                    $ascii_code = 0;
                break;
            }

            return array(
                'status'=>true,
                'ascii_code'=>$ascii_code
            );

        }else{
            return array(
                'status'=>false
            );


        }


    }
    private function cmp($a, $b){
        return strcmp($a->rating, $b->rating);
    }

    private function getAllDeviceRegistered(){
        $query = $this->ci->db->select('count(id) as all_devices')
        ->from('devices')
        ->where('active',1)
        ->get();

        return $query->row()->all_devices;
    }

    private function getChannelPeriodSummary($data = []){

        $arrReturn = array();

        $result_data = $data['result_data'];

        foreach ($result_data as $key => $value) {
            # code...
            if($key<=23){
            array_push($arrReturn, [
                'date'=>(string)$value->date,
                'time'=>(string)$value->time,
                'rating'=>(string)number_format($value->rating,3)
            ]);
            }

        }

        return $arrReturn;


    }

    private function getGraphRatingCustomNewVersion($requestCriteria){

        $date_criteria = new DateTime($requestCriteria->date);


        $arrReturn = array();

        foreach ($requestCriteria->channels as $key => $value) {

            $queryData = $this->ci->db->select('*')
            ->from('channel_daily_period_summary')
            ->where('channels_id',$value)
            ->where('date',$date_criteria->format('Y-m-d'))
            ->get();

            $channel_data = $this->getChannelDataByChannelId($value);

            if($queryData->num_rows() > 0){
                //$row = $queryData->row();
                

                array_push($arrReturn,array(
                    'channel_id'=>$value,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'rating_by_period'=>$this->getChannelPeriodSummary([
                        'result_data'=>$queryData->result()
                    ])
                ));
            }else{
                array_push($arrReturn, array(
                    'channel_id'=>$value,
                    'channel_name'=>$channel_data['channel_name'],
                    'channel_logo'=>$channel_data['channel_logo'],
                    'rating_by_period'=>$this->getChannelDailyRatingByTwentyFourHoursEmpty(array(
                        'date'=>$date_criteria->format('Y-m-d')
                    ))
                ));


            }


        }
        return $arrReturn;

    }

    private function getInternetTVViewers(){

        // bak
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
         
            CURLOPT_URL => $this->getTotalInternetTVOnlineUrl(),
            CURLOPT_TIMEOUT=>10000
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // print curl_error($curl);exit();
        //echo $resp;
        // Close request to clear up some resources
        curl_close($curl);
       
            
        //echo $this->getTotalInternetTVOnlineUrl();exit();
        $decode_data = json_decode($resp);
        //echo '<PRE>';
        //print_r($decode_data);exit();
        if(isset($decode_data->totals)){
            return $decode_data->totals;
        }else{
            return 0;
        }

    }

    private function getSummaryViewersByType(){

        $arrReturn = [];

        $query = $this->ci->db->select('*')
        ->from('online_each_region')
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                if($value->type == 'satellite'){
                    $arrReturn['Satellite'] = (is_numeric($value->sum_devices))?$value->sum_devices:0;
                }else if($value->type == 'youtube'){
                    $arrReturn['Youtube'] = (is_numeric($value->sum_devices))?$value->sum_devices:0;
                }

            }
        }

        $arrReturn['InternetTV'] = $this->getInternetTVViewers();

        return $arrReturn;



    }

    private function getSummaryViewersByTypeRV1(){
        $arrReturn = [];

        $query = $this->ci->db->select('*')
        ->from('online_each_region_rv1')
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                if($value->type == 'satellite'){
                    $arrReturn['Satellite'] = (is_numeric($value->sum_devices))?$value->sum_devices:0;
                }else if($value->type == 'youtube'){
                    $arrReturn['Youtube'] = (is_numeric($value->sum_devices))?$value->sum_devices:0;
                }

            }
        }

        $arrReturn['InternetTV'] = $this->getInternetTVViewers();

        return $arrReturn;

    }

    private function getTotalInternetTVOnlineUrl(){
        switch (ENVIRONMENT) {
            case 'development':
                return 'http://s3remoteservice.development/welcome/getTotalInternetTVOnline';
            break;
            case 'testing':
                return 'http://s3remoteservice.psi.co.th/welcome/getTotalInternetTVOnline';
            break;

            case 'production':
                return 'http://s3remoteservice.psi.co.th/welcome/getTotalInternetTVOnline';
            break;
            
            default:
                # code...
            break;
        }
    }

    private function getCountDataSummaryByGender($data = []){

        $channel_daily_devices_summary_table = $data['channel_daily_devices_summary_table'];

        $this->checkCreateDailyDevicesSummaryTable([
            'channel_daily_devices_summary_table'=>$channel_daily_devices_summary_table
        ]);

        /* count male */
        $queryCountMale = $this->ci->db->select('count(id) as count_male')
        ->from($channel_daily_devices_summary_table)
        ->where('channels_id',$data['requestCriteria']->channel_id)
        ->where('gender','Male')
        ->get();

        if($queryCountMale->num_rows() > 0){
            $count_male = $queryCountMale->row()->count_male;
        }
        


        /* count female */
        $queryCountFemale = $this->ci->db->select('count(id) as count_female')
        ->from($channel_daily_devices_summary_table)
        ->where('channels_id',$data['requestCriteria']->channel_id)
        ->where('gender','Female')
        ->get();

        if($queryCountFemale->num_rows() > 0){
            $count_female = $queryCountFemale->row()->count_female;
        }
        

        /* count not specify */
        $queryCountNotSpecify = $this->ci->db->select('count(id) as count_not_specify')
        ->from($channel_daily_devices_summary_table)
        ->where('channels_id',$data['requestCriteria']->channel_id)
        ->where_not_in('gender',['Male','Female'])
        ->get();

        if($queryCountNotSpecify->num_rows() > 0){
            $count_not_specify = $queryCountNotSpecify->row()->count_not_specify;
        }
        


        return [
            'count_all'=>($count_male+$count_female+$count_not_specify),
            'count_male'=>$count_male,
            'count_female'=>$count_female,
            'count_not_specify'=>$count_not_specify
        ];  
    }

    private function getCountDataSummaryByAge($data = []){
        $arrReturn = [];
        $this->ci->load->config('api');
        $pie_chart_age_range = $this->ci->config->item('pie_chart_age_range');

        $channel_daily_devices_summary_table = $data['channel_daily_devices_summary_table'];

        foreach ($pie_chart_age_range as $key => $value) {
            # code...
            $arrReturn[$key] = [
                'txt_range'=>$value['start_age'].'-'.$value['end_age'],
                'start_age'=>$value['start_age'],
                'end_age'=>$value['end_age'],
                'totals'=>0
            ];
            $query = $this->ci->db->select('count(id) as count_by_range')
            ->from($channel_daily_devices_summary_table)
            ->where('channels_id',$data['requestCriteria']->channel_id)
            ->where('age >=',$value['start_age'])
            ->where('age <=',$value['end_age'])
            ->get();

            if($query->num_rows() > 0){
                $arrReturn[$key]['totals'] = $query->row()->count_by_range;
            }
        }

        return $arrReturn;

    }

    private function getGraphPeriodTxtShow($data = []){
        switch ($data['type']) {
            case 'period':
                # code...
                $period_date = new DateTime($data['start_date']);

                return __('Duration','default').' - '.$period_date->format('d').' '.__($period_date->format('M'),'default').' '.$period_date->format('Y').' / 00:00 '.__('to','default').' 23:00';
            break;

            case 'rank':
                $start_date = new DateTime($data['start_date']);
                $end_date = new DateTime($data['end_date']);

                return __('Duration','default').' - '.$start_date->format('d').' '.__($start_date->format('M'),'default').' '.$start_date->format('Y').' '.__('to','default').' '.$end_date->format('d').' '.__($end_date->format('M'),'default').' '.$end_date->format('Y');
            break;
            
            default:
                return __('Duration','default').' - '.$period_date->format('d').' '.__($period_date->format('M'),'default').' '.$period_date->format('Y').' / 00:00 '.__('to','default').' 23:00';
            break;
        }
    }

    private function getAllChannelNotShowOnRanking(){

        $arr_return = [];
        $channels = new M_channels();
        $channels->where('show_on_ranking',0)
        ->where('active',1)->get();

        if($channels->result_count() > 0){
            foreach($channels as $key => $row){
                    array_push($arr_return, $row->id);
            }
        }

        return (count($arr_return) > 0)?$arr_return:false;



    }

    private function checkShowHideChannelsByUser($data = []){
        $user_id = $data['user_id'];

        $user = new M_user();
        $user->where('id',$user_id)->get();

        if($user->id){
            if($user->show_hide_channels_ranking){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    private function getRegionNameWithoutOtherText($region_name){
        $return_region = $region_name;

        if(strpos($return_region, 'Changwat') !== false){
                $return_region = str_replace('Changwat ', '', $return_region);
        }

        if(strpos($return_region, 'Province') !== false){
                $return_region = str_replace(' Province', '', $return_region);
        }

        return $return_region;

    }

    private function checkMaintenanceDaily(){
        $this->ci->load->config('api');
        $maintenance_daily = $this->ci->config->item('maintenance_time');

        $current_datetime = new DateTime();
        $maintenance_start = new DateTime($maintenance_daily['start_time']);
        $maintenance_end = new DateTime($maintenance_daily['end_time']);


        if(($current_datetime >= $maintenance_start) && ($current_datetime <= $maintenance_end)){
            return true;
        }



    }

    private function getTotalDevicesByProvinceId($data = []){

        if($data['province_id'] == 1){

            $query = $this->ci->db->select('count(device_addresses.id) as count_devices')
            ->from('device_addresses')
            ->join('provinces','device_addresses.region_name = provinces.ipstack_province_name')
            ->where('device_addresses.province_region','Bangkok')
            ->get();

            return $query->row()->count_devices;


        }else{
            $query = $this->ci->db->select('count(device_addresses.id) as count_devices')
            ->from('device_addresses')
            ->join('provinces','device_addresses.region_name = provinces.ipstack_province_name')
            ->where('provinces.id',$data['province_id'])
            ->get();

            return $query->row()->count_devices;

        }
 
        

    }

    private function getTotalDevicesByProvinceIdRV1($data = []){

    }

    private function getChannelRatingNewVersion($data = []){
        $channels_id = $data['channel_id'];
        $reach_devices = $data['reach_devices'];
        /* all device */
        $queryAllDevice = $this->ci->db->select('count(id) as all_devices')
        ->from('devices')
        ->get();

        $count_devices = $queryAllDevice->row()->all_devices;

        return number_format(($reach_devices/$count_devices)*100,3);

    }

    private function getChannelReachDevicesNewVersion($data = []){

        $rating_data_daily_devices_table = 'rating_data_daily_devices_'.date('Y').'_'.date('n');

        $current_datetime = new DateTime();

        $minusOneHour = $current_datetime->sub(new DateInterval('PT1H'));

        $query = $this->ci->db->select('count('.$rating_data_daily_devices_table.'.devices_id) as count_devcies')
        ->from($rating_data_daily_devices_table)
        ->join('rating_data_daily','rating_data_daily.id = '.$rating_data_daily_devices_table.'.rating_data_daily_id')
        ->where('rating_data_daily.channels_id',$data['channels_id'])
        ->where($rating_data_daily_devices_table.'.updated >=',$current_datetime->format('Y-m-d H:i:s'))
        ->group_by($rating_data_daily_devices_table.'.devices_id')
        ->get();

        return $query->num_rows();


    }

    private function getDateDateThreeLetterFormat($data = []){
        $dateTimeCriteria = new DateTime($data['date']);
        return $dateTimeCriteria->format('D');
    }

    private function getTVPrograms($data = []){

        $program_datetime = new DateTime($data['date']);

        /* check is already tv program */
        $queryBaseOn = $this->ci->db->select('*')
        ->from('channel_programs_baseon_date')
        ->where('channels_id',$data['channels_id'])
        ->where('date',$program_datetime->format('Y-m-d'))
        ->where('active',1)
        ->order_by('start_time','asc')
        ->get();

        //echo $this->ci->db->last_query();exit;

        if($queryBaseOn->num_rows() > 0){
            return $queryBaseOn->result();
        }else{
            /* eof check is already tv program */

            $query = $this->ci->db->select('*')
            ->from('channel_programs')
            ->where('channels_id',$data['channels_id'])
            ->where('date_of_week',$data['date_of_week'])
            ->where('active',1)
            ->order_by('start_time','asc')
            ->get();

            return $query->result();
        }


        


    }

    private function getAVGRatingByProgramRank($data = []){

        $queryAVG = $this->ci->db->select('AVG(channel_daily_rating_logs.rating) as avg_rating')
        ->from('channel_daily_rating_logs')
        ->join('channel_daily_rating','channel_daily_rating_logs.channel_daily_rating_id = channel_daily_rating.id')
        ->where('channel_daily_rating.channels_id',$data['channels_id'])
        ->where('channel_daily_rating.date',$data['date'])
        ->where('channel_daily_rating_logs.created >=',$data['start_datetime']->format('Y-m-d H:i:s'))
        ->where('channel_daily_rating_logs.created <=',$data['end_datetime']->format('Y-m-d H:i:s'))
        ->get();

        return number_format($queryAVG->row()->avg_rating,3);

    }

    private function checkReturnImmediatelyDevices($data = []){
        $requestCriteria = $data['requestCriteria'];

        $file_path = "";

        $current_datetime = new DateTime();

        $minus_datetime = $current_datetime;
        $minus_datetime->sub(new DateInterval('PT5M'));

        $file_path = './assets/json/province_'.$requestCriteria->province_id.'_'.$requestCriteria->type.'.json';

        if(file_exists($file_path)){

            //echo 'aaaa';exit;
            $strJsonFileContent = file_get_contents($file_path);
            echo $strJsonFileContent;exit;
        }else{
            return true;
        }
    }

    private function calculateTop20V2($data = []){

        $start_datetime = $data['start_datetime'];
        $end_datetime = $data['end_datetime'];
        $user_id = $data['user_id'];


        $check_is_show_that_hide_by_user = $this->checkShowHideChannelsByUser([
            'user_id'=>@$user_id
        ]);

        /* get all channel not show on ranking  */
            if(!$check_is_show_that_hide_by_user){
                $not_show_on_ranking = $this->getAllChannelNotShowOnRanking();

                $string_not_in = implode(', ', $not_show_on_ranking);
                // print_r($string);exit;
            }
            /* eof get all channel not show on ranking */

        
        
        $query_string = "select TOP 100 AVG(channel_daily_rating_logs.rating) as average_rating,channel_daily_rating.channels_id";
        $query_string .= " from channel_daily_rating_logs";
        $query_string .= " join channel_daily_rating";
        $query_string .= " on channel_daily_rating_logs.channel_daily_rating_id = channel_daily_rating.id";
        $query_string .= " where channel_daily_rating_logs.created >= '".$start_datetime->format('Y-m-d H:i:s')."' and channel_daily_rating_logs.created <= '".$end_datetime->format('Y-m-d H:i:s')."'";

        if(!$check_is_show_that_hide_by_user){$query_string .= " and channel_daily_rating.channels_id NOT IN (".$string_not_in.")";}
        $query_string .= " group by channel_daily_rating.channels_id";
        $query_string .= " order by average_rating desc";


        $query = $this->ci->db->query($query_string);

        $arrData = [];


        $count_ordinal = 1; 

        if(@$this->ci->input->get('test') == 'test'){
            // print_r($query->result());exit;
        }
        foreach ($query->result() as $key => $value) {
            # code...
                    $queryChannel = $this->ci->db->select('id,channel_name')
                    ->from('channels')
                    ->where('id',$value->channels_id)
                    ->where('channel_extended_status',0)
                    ->get();

                    if($queryChannel->num_rows() > 0){
                        $rowChannel = $queryChannel->row();

                        array_push($arrData, array(
                            'ordinal'=>$count_ordinal++,
                            'channel_id'=>$rowChannel->id,
                            'channel_name'=>$rowChannel->channel_name,
                            //'rating'=>number_format(max($value['rating']),2),
                            'rating'=>number_format($value->average_rating,3),
                            'watch_time'=>$this->getWatchTimeByChannelAndDate(array(
                                'channels_id'=>$rowChannel->id,
                                'date_start'=>$start_datetime->format('Y-m-d'),
                                'date_end'=>$end_datetime->format('Y-m-d')
                            ))
                        ));

                    }else{
                        if(@$this->ci->input->get('test') == 'test'){
                            print_r($value);exit;
                        }
                    }
        }

        return array(
            'status'=>true,
            'ChannelList'=>$arrData,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );
    }



    private function generateStorageToken($data = []){
        $arrForToken = [
            'user_id'=>$data['user_id'],
            'time'=>date('Y-m-d H:i:s')
        ];

        $txtForToken = http_build_query($arrForToken);

        return sha1($txtForToken);
    }

    private function returnCheckUserAuthenticationToken($data = []){
        $this->ci->load->library('user_agent');
        $arrReturn = [];


        /* check login with flag equal 0 */

        $checkFlag = new M_user_authentication_token();
        $checkFlag->where('storage_token',$data['storage_token'])
        ->where('user_id',$data['user']->id)
        ->where('flag',0)
        ->get();

        if($checkFlag->id){
            return [
                'status'=>false,
                'event'=>'login-by-other'
            ];

        }

        $authen = new M_user_authentication_token();
        $authen->where('storage_token',$data['storage_token'])
        ->where('user_id',$data['user']->id)
        ->where('flag',1)
        ->get();

        if($authen->id){
            $arrReturn = [
                'status'=>true
            ];  
        }else{
            /* update older record and set flag equal 0 
                - all record this user flag equal to 0
            */
            $this->ci->db->update('user_authentication_token',[
                'flag'=>0,
                'updated'=>date('Y-m-d H:i:s')
            ],['user_id'=>$data['user']->id,'flag'=>1]);


            /* insert new record for  */

            $this->ci->load->library('user_agent');
            $insert_rec = new M_user_authentication_token();
            $insert_rec->user_id = $data['user']->id;
            $insert_rec->storage_token = $data['storage_token'];
            $insert_rec->flag = 1;
            $insert_rec->user_agent = $this->ci->agent->platform();
            $insert_rec->save();


            $arrReturn = [
                'status'=>true
            ];
        }

        return $arrReturn;


    }

    private function getStorageTokenByUser($data = []){
        $user_authentication_token = new M_user_authentication_token();
        $user_authentication_token->where('user_id',$data['user_id'])
        ->where('flag',1)
        ->get();

        if($user_authentication_token->id){
            return $user_authentication_token->storage_token;
        }else{
            return '';
        }
    }

    private function checkReceiveToken($data = []){

        $this->ci->load->library('user_agent');

        $checkExistToken = new M_user_authentication_token();
        $checkExistToken->where('user_id',$data['user']->id)
        ->where('flag',1)
        ->get();

        if($checkExistToken->id){
            $updateExist = new M_user_authentication_token($checkExistToken->id);
            $updateExist->flag = 0;
            $updateExist->save();
        }


        // insert new record for this token
        $insertToken = new M_user_authentication_token();
        $insertToken->user_id = $data['user']->id;
        $insertToken->storage_token = $this->generateStorageToken(['user_id'=>$data['user']->id]);
        $insertToken->user_agent = $this->ci->agent->platform();
        $insertToken->flag = 1;
        $insertToken->save();


    }

    private function getRatingChannelLogo($data = []){

        $merge_s3remote_id = $data['merge_s3remote_id'];


        if($merge_s3remote_id){
            $s3channel_data = $this->getS3RemoteChannelDataByRatingMergeId(array(
                    'tvchannels_id'=>$merge_s3remote_id
            ));

            if($s3channel_data){
                return $this->getS3ChannelUrl().'tv/uploaded/tv/logo/'.$s3channel_data->logo;
            }else{
                return $this->getS3ChannelUrl().'tv/uploaded/tv/logo/no-product-image.jpg';
            }
        }else{
            // return default logo
            return $this->getS3ChannelUrl().'tv/uploaded/tv/logo/no-product-image.jpg';
        }


        //print_r($s3channel_data);




    }

    private function getS3RemoteChannelDataByRatingMergeId($data = []){



        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->getS3ChannelUrl().'welcome/getChannelDataByMergeId?tvchannels_id='.$data['tvchannels_id'],
            CURLOPT_TIMEOUT=>10000
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        //echo $resp;
        // Close request to clear up some resources
        curl_close($curl);

        

        $decode_data = json_decode($resp);
        // if($decode_data->status){
        //     return $decode_data->ChannelData;
        // }else{
        //     return false;
        // }

        if(isset($decode_data->status)){
            if($decode_data->status){
                return $decode_data->ChannelData;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    private function getRatingPackagesData($data = []){
        $query = $this->ci->db->select('*')
        ->from('rating_packages')
        ->where('id',$data['rating_packages_id'])
        ->get();

        if($query->num_rows() > 0){
            return $query->row();

        }else{
            return false;
        }
    }

    private function getUserChannelCompare($user_id){
        $arr_return = [];

        $queryCheck = $this->ci->db->select('*')
        ->from('user_channels_compare')
        ->where('user_id',$user_id)
        ->get();

        if($queryCheck->num_rows() > 0){
            $arr_return =  json_decode($queryCheck->row()->save_channels_compare);
        }

        return $arr_return;
    }

    private function checkCreateDailyDevicesSummaryTable($data = []){
        $table_name = $data['channel_daily_devices_summary_table'];

        //echo $table_name;exit;
        $query = $this->ci->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$table_name."'");

        if($query->num_rows() <= 0){
                /* create table for rating data */
                $strQuery = "CREATE TABLE ".$table_name." (
                    id int IDENTITY(1,1) PRIMARY KEY,
                    devices_id int,
                    channels_id int,
                    chip_code varchar(255),
                    gender varchar(10),
                    age int,
                    os varchar(50),
                    os_version varchar(50),                    
                    device_type varchar(50),
                    created datetime
                )";

                $this->ci->db->query($strQuery);

        }


    }

    private function getUserIP(){
                       $client  = @$_SERVER['HTTP_CLIENT_IP'];
                            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                            $remote  = $_SERVER['REMOTE_ADDR'];

                            if(filter_var($client, FILTER_VALIDATE_IP))
                            {
                                $ip = $client;
                            }
                            elseif(filter_var($forward, FILTER_VALIDATE_IP))
                            {
                                $ip = $forward;
                            }
                            else
                            {
                                $ip = $remote;
                            }

                            return $ip;
    }

    private function createLogFilePath($filename = '') {
        $log_path = './application/logs/rating_login';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function exportDataToXLSX($data = []){ 

        $RatingData = $data['RatingData']; 

        // print_r($RatingData); 

        


        $filename = 'RatingExport'.date('YmdHis').'.xlsx';

        require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel.php';



        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Rating Report")
                    ->setLastModifiedBy("ADMIN")
                    ->setTitle($filename)
                    ->setSubject("Excel file for rating report")
                    ->setDescription("Excel file for rating report")
                    ->setKeywords("Rating Report")
                    ->setCategory("Rating REport");



        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1')->setCellValue('A1',$filename);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        
        $arrChannelName = [];
        foreach ($RatingData as $key => $value) {
            # code...
            array_push($arrChannelName, $value['channel_name']);
            //$objPHPExcel->setCellValue('')

        }



        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A2','Date')
                            ->setCellValue('B2',@$arrChannelName[0])
                            ->setCellValue('C2',@$arrChannelName[1])
                            ->setCellValue('D2',@$arrChannelName[2])
                            ->setCellValue('E2',@$arrChannelName[3])
                            ->setCellValue('F2',@$arrChannelName[4])
                            ->setCellValue('G2',@$arrChannelName[5]);

            $count = 1;
            $table_row = 3;
                foreach ($RatingData[0]['rating_by_period'] as $key => $value) {
                    # code...

                    $dt = new DateTime(@$value['date'].' '.@$value['time']);
                    
                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$table_row.'',($value['time'])?$dt->format('d/m/Y H:i'):$dt->format('d/m/Y'))
                                ->setCellValue('B'.$table_row.'',@$RatingData[0]['rating_by_period'][$key]['rating'])
                                ->setCellValue('C'.$table_row.'',@$RatingData[1]['rating_by_period'][$key]['rating'])
                                ->setCellValue('D'.$table_row.'',@$RatingData[2]['rating_by_period'][$key]['rating'])
                                ->setCellValue('E'.$table_row.'',@$RatingData[3]['rating_by_period'][$key]['rating'])
                                ->setCellValue('F'.$table_row.'',@$RatingData[4]['rating_by_period'][$key]['rating'])
                                ->setCellValue('G'.$table_row.'',@$RatingData[5]['rating_by_period'][$key]['rating']);
                     $count++;
                    $table_row++;

                }

                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('A')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('B')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('C')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('D')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('E')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('F')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('G')
                ->setAutoSize(true);

            // Rename worksheet
                $objPHPExcel->getActiveSheet()->setTitle('RatingReport');


                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);


                // Redirect output to a client’s web browser (Excel2007)
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$filename.'"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');

                // If you're serving to IE over SSL, then the following may be needed
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                // $objWriter->save('php://output');
                $objWriter->save('./uploaded/xlsx/'.$filename.''); 

                return $filename;

                exit;

    }

    private function exportDataToXLSX24H($data = []){
        $RatingData = $data['RatingData']; 

        // print_r($RatingData); 

        


        $filename = 'RatingExport24H'.date('YmdHis').'.xlsx';

        require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel.php';



        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Rating Report")
                    ->setLastModifiedBy("ADMIN")
                    ->setTitle($filename)
                    ->setSubject("Excel file for rating report")
                    ->setDescription("Excel file for rating report")
                    ->setKeywords("Rating Report")
                    ->setCategory("Rating REport");



        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1')->setCellValue('A1',$filename);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        
        $arrChannelName = [];
        foreach ($RatingData as $key => $value) {
            # code...
            array_push($arrChannelName, $value['channel_name']);
            //$objPHPExcel->setCellValue('')

        }



        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A2','Date')
                            ->setCellValue('B2',@$arrChannelName[0])
                            ->setCellValue('C2',@$arrChannelName[1])
                            ->setCellValue('D2',@$arrChannelName[2])
                            ->setCellValue('E2',@$arrChannelName[3])
                            ->setCellValue('F2',@$arrChannelName[4])
                            ->setCellValue('G2',@$arrChannelName[5]);

            $count = 1;
            $table_row = 3;
                foreach ($RatingData[0]['rating_by_period'] as $key => $value) {
                    # code...

                    foreach ($value as $k => $v) {
                        $dt = new DateTime(@$v['date'].' '.@$v['time']);
                    
                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$table_row.'',($v['time'])?$dt->format('d/m/Y H:i'):$dt->format('d/m/Y'))
                                ->setCellValue('B'.$table_row.'',@$RatingData[0]['rating_by_period'][$key][$k]['rating'])
                                ->setCellValue('C'.$table_row.'',@$RatingData[1]['rating_by_period'][$key][$k]['rating'])
                                ->setCellValue('D'.$table_row.'',@$RatingData[2]['rating_by_period'][$key][$k]['rating'])
                                ->setCellValue('E'.$table_row.'',@$RatingData[3]['rating_by_period'][$key][$k]['rating'])
                                ->setCellValue('F'.$table_row.'',@$RatingData[4]['rating_by_period'][$key][$k]['rating'])
                                ->setCellValue('G'.$table_row.'',@$RatingData[5]['rating_by_period'][$key][$k]['rating']);
                        $count++;
                        $table_row++;
                    }
                    

                }

                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('A')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('B')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('C')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('D')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('E')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('F')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('G')
                ->setAutoSize(true);

            // Rename worksheet
                $objPHPExcel->getActiveSheet()->setTitle('RatingReport');


                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);


                // Redirect output to a client’s web browser (Excel2007)
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$filename.'"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');

                // If you're serving to IE over SSL, then the following may be needed
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                // $objWriter->save('php://output');
                $objWriter->save('./uploaded/xlsx/'.$filename.''); 

                return $filename;

                exit;
    }



   
}