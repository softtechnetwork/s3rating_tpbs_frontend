<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP Encrypt/Decrypt: Simple class to encrypt url data
 *
 * Provides two-way keyed encoding using Mcrypt
 *
 * @package	CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author	Zeeshan Rasool
 * @link	http://www.99points.info/2010/06/php-encrypt-decrypt-functions-to-encrypt-url-data/
 */

class EncryptUrlData {
    var $skey     = ''; // you can change it
    
    public  function safe_b64encode($string) {
    
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
 
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
    
    public  function encode_url_data($value, $key=''){ 
          
        
        if(!$value){return false;} 
        $text = $value;
     
        $this->skey = $key; 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);

        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);     
        
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->skey, $text, MCRYPT_MODE_ECB, $iv);

        return trim($this->safe_b64encode($crypttext)); 
    }
    
    public function decode_url_data($value, $key=''){
        
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value); 
        //echo $key;
        //echo $crypttext;
        $this->skey = $key;         
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);   
        //echo $decrypttext;
        return trim($decrypttext);
    }
        
}

