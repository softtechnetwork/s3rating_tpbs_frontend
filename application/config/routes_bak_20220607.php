<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['api/SetChannelRating'] = "S3Module/index";
$route['api/UpdateDeviceAddress'] = "S3Module/UpdateDeviceAddress";
$route['api/createMonthYearRatingDataTable'] = "S3Module/createMonthYearRatingDataTable";



/* for rating system */
$route['api/SetUserLogin'] = "S3RatingModule/SetUserLogin";
$route['api/GetRefreshMainPageData'] = "S3RatingModule/GetRefreshMainPageData";
$route['api/GetCompareChannelTopTwentyRating'] = "S3RatingModule/GetCompareChannelTopTwentyRating";
$route['api/GetCompareManualChannels'] = "S3RatingModule/GetCompareManualChannels";
$route['api/GetCompareManualChannelsMultiOptions'] = "S3RatingModule/GetCompareManualChannelsMultiOptions";
$route['api/GetAllChannelRating'] = "S3RatingModule/GetAllChannelRating";
$route['api/GetAllChannelRatingWithChannelProgram'] = "S3RatingModule/GetAllChannelRatingWithChannelProgram";
$route['api/GetCompareChannelByRegion'] = "S3RatingModule/GetCompareChannelByRegion";
$route['api/GetCompareManualChannelsSelectOptions'] = "S3RatingModule/GetCompareManualChannelsSelectOptions";

$route['api/GetAllDeviceOnlineEachRegion'] = "S3RatingModule/GetAllDeviceOnlineEachRegion";
$route['api/GetAllDeviceOnlineEachProvinceByRegionName'] = "S3RatingModule/GetAllDeviceOnlineEachProvinceByRegionName";
$route['api/GetAllProvinceRegion'] = "S3RatingModule/GetAllProvinceRegion";
$route['api/SetNewPassword'] = "S3RatingModule/SetNewPassword";


$route['api/GetCompareChannelTopTwentyRatingNewVersion'] = "S3RatingModule/GetCompareChannelTopTwentyRatingNewVersion";

$route['api/GetCompareChannelTopTwentyRatingNewVersionUnAuth'] = "S3RatingModule/GetCompareChannelTopTwentyRatingNewVersionUnAuth";

$route['api/GetChannelGraphRatingByMonthYear'] = "S3RatingModule/GetChannelGraphRatingByMonthYear";

$route['api/GetChannelPieChartGenderAndAgeByMonthYear'] = "S3RatingModule/GetChannelPieChartGenderAndAgeByMonthYear";

$route['api/GetDeviceLocationByProvince'] = "S3RatingModule/GetDeviceLocationByProvince";

$route['api/GetTVProgramRating'] = "S3RatingModule/GetTVProgramRating";


/* mobile popup advertisement */
$route['api/GetChannelAdvertisementMobilePopup'] = "S3RatingModule/GetChannelAdvertisementMobilePopup";
/* eof mobile popup advertisement*/



/* get top 18 hits channel from s3remote application */
$route['api/GetChannelHits'] = "S3RatingModule/GetChannelHits";
/* eof get top 18 hits channel from remote application */


/* set youtube view logs for s3remote application */
$route['api/SetYoutubeViewLogs'] = "S3Module/SetYoutubeViewLogs";
/* eof set youtube view logs for s3remote application */

/* set  history link  logs for s3remote application */
//$route['api/SetHistoryLinkLogs'] = "S3Module/SetHistoryLinkLogs";
/* eof set history link  logs for s3remote application */




/* Additional Rating 30.07.19 */
$route['api/GetCompareChannelByRegionRV1'] = "S3RatingModule/GetCompareChannelByRegionRV1";
$route['api/GetAllProvinceRegionRV1'] = "S3RatingModule/GetAllProvinceRegionRV1";

$route['api/GetAllDeviceOnlineEachRegionRV1'] = "S3RatingModule/GetAllDeviceOnlineEachRegionRV1";
$route['api/GetAllDeviceOnlineEachProvinceByRegionNameRV1'] = "S3RatingModule/GetAllDeviceOnlineEachProvinceByRegionNameRV1";


// Additional Rating 20.09.19
$route['api/SetUserChannelCompare'] = "S3RatingModule/SetUserChannelCompare";


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['backend'] = 'backend/login';
$route['backend/(.*)'] = "backend/$1";

$route['report'] = 'report/login';
$route['report/(.*)'] = "report/$1";


$route['admin/channels/setChannelProgramBODate/(:any)/(:any)'] = "admin/Channels/setChannelProgramBODate/$1/$2";

$route['admin/channels/setChannelProgram/(:any)/(:any)'] = "admin/Channels/setChannelProgram/$1/$2";

$route['admin'] = 'admin/login';
$route['admin/(.*)'] = "admin/$1";

