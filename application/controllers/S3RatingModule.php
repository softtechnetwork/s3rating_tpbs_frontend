<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require_once(APPPATH. "libraries/api/S3RatingLibrary.php");



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class S3RatingModule extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }


    public function SetUserLogin_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetUserLogin');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setUserLogin($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetRefreshMainPageData_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetRefreshMainPageData');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getRefreshMainPageData($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetCompareChannelTopTwentyRating_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetCompareChannelTopTwentyRating');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getCompareChannelTopTwentyRating($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetCompareManualChannels_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetCompareManualChannels');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getCompareManualChannels($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetAllChannelRating_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllChannelRating');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllChannelRating($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetAllChannelRatingWithChannelProgram_post(){
                if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllChannelRatingWithChannelProgram');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllChannelRatingWithChannelProgram($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetCompareChannelByRegion_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetCompareChannelByRegion');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getCompareChannelByRegion($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetAllDeviceOnlineEachRegion_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllDeviceOnlineEachRegion');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllDeviceOnlineEachRegion($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetAllDeviceOnlineEachProvinceByRegionName_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllDeviceOnlineEachProvinceByRegionName');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllDeviceOnlineEachProvinceByRegionName($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetAllProvinceRegion_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllProvinceRegion');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllProvinceRegion($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }


    }

    public function GetChannelAdvertisementMobilePopup_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;
                    $output = $this->__getChannelAdvertisementMobilePopup($requestCriteria);
                    echo json_encode($output);exit;

                    
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function SetNewPassword_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetNewPassword');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setNewPassword($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetCompareManualChannelsSelectOptions_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetCompareManualChannelsSelectOptions');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getCompareManualChannelsSelectOptions($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetCompareManualChannelsMultiOptions_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetCompareManualChannelsMultiOptions');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getCompareManualChannelsMultiOptions($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetCompareChannelTopTwentyRatingNewVersion_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetCompareChannelTopTwentyRatingNewVersion');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getCompareChannelTopTwentyRatingNewVersion($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetCompareChannelTopTwentyRatingNewVersionUnAuth_get(){

        $month = $this->get('month');
        $year = $this->get('year');

        $requestCriteria = new StdClass();
        $requestCriteria->month = $month;
        $requestCriteria->year = $year;
        $requestCriteria->channel_id = 340;


        $output = $this->__getCompareChannelTopTwentyRatingNewVersionUnAuth($requestCriteria);
       // print_r($requestCriteria);

        print_r($output);

    }

    public function GetChannelGraphRatingByMonthYear_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetChannelGraphRatingByMonthYear');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getChannelGraphRatingByMonthYear($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetChannelHits_get(){

        $ci =& get_instance();
        $band_type = $this->get('band_type');
        //print_r($band_type);exit;

        $requestCriteria = new StdClass();
        $requestCriteria->month = date('m');
        $requestCriteria->year = date('Y');
        $requestCriteria->band_type = $band_type;

        /* check last update in last five minute */
        $ci =& get_instance();
        $queryCheck = $ci->db->select('*')
        ->from('channel_hits')
        ->where('band_type',$band_type)
        ->limit(1)
        ->get();

        $d_date = new DateTime();
        $d_date->sub(new DateInterval('PT5M'));

        if($queryCheck->row()->updated >= $d_date->format('Y-m-d H:i:s')){
            echo $queryCheck->row()->response;
        }else{


                $output = $this->__getS3RemoteChannelHits($requestCriteria);

                if(count($output['ChannelList']) > 0){


                    $queryCheckRow = $ci->db->select('*')
                    ->from('channel_hits')
                    ->where('band_type',$band_type)
                    ->order_by('id','asc')
                    ->limit(1)
                    ->get();

                    /* insert new record into  channel_hits */
                    $ci->db->update('channel_hits',array(
                        'response'=>json_encode($output),
                        'band_type'=>$band_type,
                        'updated'=>date('Y-m-d H:i:s')
                    ),array('id'=>$queryCheckRow->row()->id));
                    /* eof insert new record into channel_hits */

                    echo json_encode($output);
                }else{

                    /* find last record response */
                    $query = $ci->db->select('*')
                    ->from('channel_hits')
                    ->where('band_type',$band_type)
                    ->order_by('created')
                    ->limit(1)
                    ->get();

                    if($query->num_rows() > 0){
                        echo $query->row()->response;
                    }else{
                        echo json_encode($output);
                    }


                }
        }
        //echo json_encode($output);
        //print_r($output);
    }

    public function GetChannelPieChartGenderAndAgeByMonthYear_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetChannelPieChartGenderAndAgeByMonthYear');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getChannelPieChartGenderAndAgeByMonthYear($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetDeviceLocationByProvince_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetDeviceLocationByProvince');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getDeviceLocationByProvince($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetTVProgramRating_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetTVProgramRating');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getTVProgramRating($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetAllProvinceRegionRV1_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllProvinceRegionRV1');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllProvinceRegionRV1($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetCompareChannelByRegionRV1_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetCompareChannelByRegionRV1');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getCompareChannelByRegionRV1($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetAllDeviceOnlineEachRegionRV1_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllDeviceOnlineEachRegionRV1');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllDeviceOnlineEachRegionRV1($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetAllDeviceOnlineEachProvinceByRegionNameRV1_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllDeviceOnlineEachProvinceByRegionNameRV1');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllDeviceOnlineEachProvinceByRegionNameRV1($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function SetUserChannelCompare_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetUserChannelCompare');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setUserChannelCompare($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    private function __setUserLogin($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->setUserLogin($requestCriteria);
        return $output;
    }

    private function __getRefreshMainPageData($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getRefreshMainPageData($requestCriteria);
        return $output;
    }

    private function __getCompareChannelTopTwentyRating($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getCompareChannelTopTwentyRating($requestCriteria);
        return $output;
    }

    private function __getCompareManualChannels($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getCompareManualChannels($requestCriteria);
        return $output;
    
    }
    private function __getAllChannelRating($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getAllChannelRating($requestCriteria);
        return $output;
    }

    private function __getAllChannelRatingWithChannelProgram($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getAllChannelRatingWithChannelProgram($requestCriteria);
        return $output;
    }

    private function __getCompareChannelByRegion($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getCompareChannelByRegion($requestCriteria);
        return $output;
    }

    private function __getAllDeviceOnlineEachRegion($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getAllDeviceOnlineEachRegion($requestCriteria);
        return $output;
    }

    private function __getAllDeviceOnlineEachProvinceByRegionName($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getAllDeviceOnlineEachProvinceByRegionName($requestCriteria);
        return $output;
    }

    private function __getAllProvinceRegion($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getAllProvinceRegion($requestCriteria);
        return $output;
    }

    private function __getChannelAdvertisementMobilePopup($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getChannelAdvertisementMobilePopup($requestCriteria);
        return $output;
    }

    private function __setNewPassword($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->setNewPassword($requestCriteria);
        return $output;
    }

    private function __getCompareManualChannelsSelectOptions($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getCompareManualChannelsSelectOptions($requestCriteria);
        return $output;
    }

    private function __getCompareManualChannelsMultiOptions($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getCompareManualChannelsMultiOptions($requestCriteria);
        return $output;
    }

    private function __getCompareChannelTopTwentyRatingNewVersion($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getCompareChannelTopTwentyRatingNewVersion($requestCriteria);
        return $output;
    }


    private function __getCompareChannelTopTwentyRatingNewVersionUnAuth($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getCompareChannelTopTwentyRatingNewVersionUnAuth($requestCriteria);
        return $output;
    }


    private function __getChannelGraphRatingByMonthYear($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getChannelGraphRatingByMonthYear($requestCriteria);
        return $output;
    }

    private function __getS3RemoteChannelHits($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getS3RemoteChannelHits($requestCriteria);
        return $output;
    }

    private function __getChannelPieChartGenderAndAgeByMonthYear($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getChannelPieChartGenderAndAgeByMonthYear($requestCriteria);
        return $output;
    }

    private function __getDeviceLocationByProvince($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getDeviceLocationByProvince($requestCriteria);
        return $output;
    }

    private function __getTVProgramRating($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getTVProgramRating($requestCriteria);
        return $output;
    }

    private function __getCompareChannelByRegionRV1($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getCompareChannelByRegionRV1($requestCriteria);
        return $output;
    }

    private function __getAllProvinceRegionRV1($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getAllProvinceRegionRV1($requestCriteria);
        return $output;
    }

    private function __getAllDeviceOnlineEachRegionRV1($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getAllDeviceOnlineEachRegionRV1($requestCriteria);
        return $output;
    }

    private function __getAllDeviceOnlineEachProvinceByRegionNameRV1($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->getAllDeviceOnlineEachProvinceByRegionNameRV1($requestCriteria);
        return $output;
    }

    private function __setUserChannelCompare($requestCriteria){
        $output = array();
        $S3RatingLibrary = new S3RatingLibrary();
        $output = $S3RatingLibrary->setUserChannelCompare($requestCriteria);
        return $output;
    } 

}