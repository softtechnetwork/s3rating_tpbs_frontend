<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once APPPATH . 'libraries/ReportLibrary.php';
class Overview extends ReportLibrary
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//print_r($this->session->userdata('user_data'));
		date_default_timezone_set('Asia/Bangkok');

		# load dashboard javascript file 
		$this->bootstrapDatepicker();

		$this->loadDatePickerStyle();
		$this->loadDatePickerScript();

		$this->loadDateRangePickerStyle(); // load jquery datepicker rang
		$this->loadDateRangePickerScript(); // load jquery picker

		$this->loadDataTableStyle(); // load css data table
		$this->loadDataTableScript(); // load jquery data table

		$this->template->javascript->add(base_url('assets/report/js/overview.js?v14'));

		$this->template->javascript->add("https://www.gstatic.com/charts/loader.js");
		$this->template->javascript->add("https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js");
		
		$session_data = $this->report_data;
		# get tvd channel
		$tvd_array = $this->get_channels_array(null , $session_data);
		// echo '<PRE>';print_r($tvd_array);exit();
		$tvd_channelsId = array_column($tvd_array, 'channel_id');
		$tvd_channels = $this->fetch_channels($tvd_channelsId);

		$data = array(
			"tvd_channels" =>  $tvd_channels,
			"type"         =>  ENVIRONMENT,
		);

		$this->template->content->view('report/overview', $data);
		$this->template->publish();
	}

	public function get_overview() {
        $data = new tpbs_report_store();
        $data->where('report_type', 'overview')->where('default_language', 1)->get();
        $this->default_language = $shop_language->language->get()->getForeign_language_name();
       // echo "default lang =".$this->default_language;
    }

	public function get_overviews() {
		$search_post = $this->input->post();

		$where = " WHERE report_type ='overview' ";
		if(!empty($search_post['duration']) ){
			$where .= " AND date ='".$search_post['duration']."'";
		}
		$where_overviewdaily = " WHERE report_type ='overview_daily' ";
		if(!empty($search_post['duration']) ){
			$where_overviewdaily .= " AND date ='".$search_post['duration']."'";
		}
		$where_avgview = " WHERE report_type ='avgview_perminute' ";
		if(!empty($search_post['duration']) ){
			$where_avgview .= " AND date ='".$search_post['duration']."'";
		}

		/*if(!empty($Data['duration']) ){
			$where = " AND d.date = $Data['duration'] ";
		}*/
		/** =========== bak : old query 2022 - 04 -25 =====================  */
		// $Query = "SELECT overview.* ";
		// $Query .= ", d.date, d.foldername "; 
		// $Query .= ", convert(char(5), overview.start_datetime, 108) start_time "; 
		// $Query .= ", convert(char(5), overview.end_datetime, 108) end_time ";  
		// $Query .= " FROM tpbs_report_store_overview overview";
		// $Query .= " left join tpbs_report_store d on overview.tpbs_report_store_id = d.id";
        // $Query .= $where;
		// $Query .= " order by overview.type ,overview.start_datetime";
		/** =========== eof bak : old query 2022 - 04 -25 =====================  */
		$Query = "SELECT   overview_daily.filename  as overview_daily_filename , overview_daily.foldername  as overview_daily_foldername ,avgview_perminute.filename  as avgview_perminute_filename , avgview_perminute.foldername  as avgview_perminute_foldername  ,overview.* ";
		$Query .= ", d.date, d.foldername "; 
		$Query .= ", convert(char(5), overview.start_datetime, 108) start_time "; 
		$Query .= ", convert(char(5), overview.end_datetime, 108) end_time ";  
		$Query .= " FROM  ( select top 1 * from tpbs_report_store  {$where}
		order by id desc ) as d";
		$Query .= "   left join  tpbs_report_store_overview overview 
		on overview.tpbs_report_store_id = d.id ";
		$Query .= "  left join (select top 1 * from tpbs_report_store {$where_overviewdaily} order by id desc) as overview_daily  on overview_daily.date  = d.date ";
		$Query .= "  left join (select top 1 * from tpbs_report_store {$where_avgview} order by id desc) as avgview_perminute  on avgview_perminute.date  = d.date ";
		$Query .= " order by overview.type ,overview.start_datetime";
		$Res = $this->db->query($Query);
		// echo $this->db->last_query();exit();
		$data = $Res->result();
	    
		echo json_encode($data);
    }
	public function get_store_overviews() {
		$search_post = $this->input->post();

		$where = " ";
		if(!empty($search_post['duration']) ){
			$where .= " WHERE d.type = 'all' and overview.date ='".$search_post['duration']."' ";
		}
		/*if(!empty($Data['duration']) ){
			$where = " AND d.date = $Data['duration'] ";
		}*/

		$Query = "SELECT TOP(1) overview.*, d.filename as fname ";
		$Query .= " FROM tpbs_report_store overview ";
		$Query .= " left join tpbs_report_store_overview d on overview.id = d.tpbs_report_store_id ";
		$Query .= $where;
		$Query .= " order by overview.id desc ";
		
		$Res = $this->db->query($Query);
		$data = $Res->result();
	    
		echo json_encode($data);
    }



















	public function fetch_channels($channels_array = null)
	{

		$query =  $this->db->select("*")->from("channels");
		if (!empty($channels_array)) {
			$query = $query->where_in("id", $channels_array);
		}
		$result = $query->get();
		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}
	}
	public function get_channels_array($channels_id = null , $session_data = null)
	{
		# set a default channels
		$channels = array(
			"0" => array("channel_id" => 280,  "channel_name" =>  "TVD1"), "1" => array("channel_id" => 281,  "channel_name" =>  "TVD2"), "2" => array("channel_id" => 296,  "channel_name" =>  "TVD3"), "3" => array("channel_id" => 513,  "channel_name" =>  "TVD4"), "4" => array("channel_id" => 278,  "channel_name" =>  "TVD7"), "5" => array("channel_id" => 289,  "channel_name" =>  "TVD8"), "6" => array("channel_id" => 477,  "channel_name" =>  "TVD9"), "7" => array("channel_id" => 318,  "channel_name" =>  "TVD10หรือTOPNEWS")
		);
		if(!empty($session_data) || !empty($channels_id)){
			$user_id = $session_data['id'];
			// $query = $this->db->select('channels.id as channel_id,channels.channel_name')->from("user_owner_channels")
			// ->join('channels', 'user_owner_channels.channels_id = channels.id');
			$query = $this->db->select('channels.id as channel_id,channels.channel_name')->from("channels");
			if(!empty($session_data)){
				// $query = $query->where('user_id' , $user_id);
			}
			// $query = $query->group_start() //this will start grouping
			// 	->where('channels_id', 252) # tbps channel
			// 	->or_where('channels_id', 525)
			// 	->group_end(); //this will end grouping
			$query = $query
				->where('id', 252) # tbps channel
				->or_where('id', 525); # altv channel


			
			$query = $query->get();
			// echo $this->db->last_query();exit();
			if($query->num_rows() > 0){
				$channels = $query->result_array();
			}
		}

	
	
		if (!empty($channels_id)) {

			$find_channelkey = array_search($channels_id, array_column($channels, 'channel_id'));
			
			if (is_numeric($find_channelkey)) {
				$new_array = array();
				$new_array[] = $channels[$find_channelkey];
				$channels    = $new_array;
			}
			
		}
		// echo '<PRE>';print_r($channels);
		return $channels;
	}
	public function exportchanneldailyrating_ajaxcall(){
		$channels_id = $_GET['id'];
		$duration_month_start = $_GET['duration_month_start'];
		$duration_month_end   = $_GET['duration_month_end'];
		$channels =  $this->get_channels_array($channels_id );
		$export_dailyajax = $this->exportchanneldailydaterating($channels , $duration_month_start ,  $duration_month_end);

		echo json_encode(array("status"=>true));
	}


	public function exportchanneldailyratinghr_ajaxcall(){
		$channels_id = $_GET['id'];
		$duration_month_start = $_GET['duration_month_start'];
		$duration_month_end   = $_GET['duration_month_end'];
		$channels =  $this->get_channels_array($channels_id);
		$export_dailyajax = $this->exportchanneldailyrating($channels , $duration_month_start ,  $duration_month_end);

		echo json_encode(array("status"=>true));
	}


	public function ajaxSetSidenavToggle()
	{
		$post_data = $this->input->post();

		if ($post_data['toggled_status'] == 'toggled') {
			$this->session->set_userdata('toggle_status', 'sidenav-toggled');
		} else {
			$this->session->set_userdata('toggle_status', '');
		}

		echo json_encode(array(
			'status' => true,
			'post_data' => $post_data
		));
	}




	public function exportchanneldailydaterating($channels, $start_date = null, $end_date = null)
	{


		require_once APPPATH . 'third_party/PHPExcel/Classes/PHPExcel.php';

		$channel_id   = $channels[0]['channel_id'];
		$channel_name   = $channels[0]['channel_name'];
		$filename = 'RatingExportรายวัน' . $channel_name . '_' . date('YmdHis') . '.xlsx';


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();


		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Rating Report")
			->setLastModifiedBy("ADMIN")
			->setTitle($filename)
			->setSubject("Excel file for rating report")
			->setDescription("Excel file for rating report")
			->setKeywords("Rating Report")
			->setCategory("Rating REport");

		# duration start
		$arr_duration_start = explode("/", $start_date);
		$month_start =  $arr_duration_start[0];
		$year_start =  $arr_duration_start[1];
		# duration end
		$arr_duration_end = explode("/", $end_date);
		$old_month_end =  $arr_duration_end[0];
		$year_end =  $arr_duration_end[1];
	
		$sheet = 0;
		$get_lastdatearr = $this->getdatetimereportV2($year_end, $old_month_end, null);
		$lastdate_filter = end($get_lastdatearr);; # case : วันสุดท้าย
		for ($y = $year_start; $y <= $year_end; $y++) {
			# case : ถ้าเป็นเดือนแรกของปี
			$month_start = $y == $year_start ? $month_start : 1;
			$month_end   = $y == $year_end ? $old_month_end : 12;

			for ($i = $month_start; $i <= $month_end; $i++) {
				$date_filter = $this->getdatetimereportV2($y, $i, $lastdate_filter);
				$data = array();
				echo "month/year = ".$i .'--'. $y .' m/end' . $month_end ." ------- ";
				# case : declare variable date start and date end
				$date_start  = $date_filter[key($date_filter)];
				$date_end    = end($date_filter);
			
				# case : query period summary
				$query = $this->db->select("channel_daily_date_summary.*")
					->from("channel_daily_date_summary")
					->where('channels_id', $channel_id)
					->where("date >= ", $date_start)
					->where("date <= ", $date_end)
					->order_by("date", "asc")
					->get();
				
				foreach ($query->result() as $key => $value) {
					# set default array value
					if (!isset($data[$value->date])) {
						$data[$value->date] = array();
						for ($j = 0; $j <= 23; $j++) {
							$prefix = $j <= 9 ? '0' : "";
							$data[$value->date][$prefix . $j . ':00'] = 0;
						}
					}
					# set rating data
					$data[$value->date] = $value->rating; # set rating

				}

				// Add new sheet
				$objPHPExcel->createSheet();
				$activesheet = $objPHPExcel->setActiveSheetIndex($sheet);
				$activesheet->setTitle("{$i}_{$y}");

				$row    = 1;
				$column = 1;
				$key = 0;
				if (!empty($date_filter)) {



					# case : set first column
					$activesheet->setCellValueByColumnAndRow(0, 1, 'date');
					$colIndex = PHPExcel_Cell::stringFromColumnIndex(0); # case : get column string name
					$activesheet->getColumnDimension("{$colIndex}")->setWidth(10); # set column width
					$activesheet->getStyle($colIndex . '1')->applyFromArray(
						array(
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'FFFF00')
							)
						)
					);
					# case : set date cell

					$activesheet->setCellValueByColumnAndRow(1, 1, 'rating');
					$colIndex = PHPExcel_Cell::stringFromColumnIndex(1); # case : get column string name
					$activesheet->getColumnDimension("{$colIndex}")->setWidth(10); # set column width
					$activesheet->getStyle($colIndex . '1')->applyFromArray(
						array(
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'FFFF00')
							)
						)
					);


					$rowstart = 2;
					foreach ($data as $date_key => $value) {
						$activesheet->setCellValueByColumnAndRow(0, $rowstart,  $date_key);
						# case : set channel daily rating
						$activesheet->setCellValueByColumnAndRow(1, $rowstart,  $value);
						$colIndexvalue = PHPExcel_Cell::stringFromColumnIndex(1); # case : get column string name
						$activesheet->getStyle($colIndexvalue . $rowstart)->getNumberFormat()->setFormatCode('0.000');
						++$rowstart;
					}
				}


				++$sheet;
			} #end for loop month
		} # end for loop year
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		$objWriter->save('php://output');


		 exit;
	}


	public function exportchanneldailyrating($channels, $start_date = null, $end_date = null)
	{


		require_once APPPATH . 'third_party/PHPExcel/Classes/PHPExcel.php';
		$channel_id   = $channels[0]['channel_id'];
		$channel_name   = $channels[0]['channel_name'];
		$filename = 'RatingExportHourly' . $channel_name . '_' . date('YmdHis') . '.xlsx';





		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();


		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Rating Report")
			->setLastModifiedBy("ADMIN")
			->setTitle($filename)
			->setSubject("Excel file for rating report")
			->setDescription("Excel file for rating report")
			->setKeywords("Rating Report")
			->setCategory("Rating REport");

		# duration start
		$arr_duration_start = explode("/", $start_date);
		$month_start =  $arr_duration_start[0];
		$year_start =  $arr_duration_start[1];
		# duration end
		$arr_duration_end = explode("/", $end_date);
		$old_month_end =  $arr_duration_end[0];
		$year_end =  $arr_duration_end[1];

		$sheet = 0;
		$get_lastdatearr = $this->getdatetimereportV2($year_end, $old_month_end, null);
		$lastdate_filter = end($get_lastdatearr);; # case : วันสุดท้าย

		for ($y = $year_start; $y <= $year_end; $y++) {
			# case : ถ้าเป็นเดือนแรกของปี
			$month_start = $y == $year_start ? $month_start : 1;
			$month_end   = $y == $year_end ? $old_month_end : 12;

			for ($i = $month_start; $i <= $month_end; $i++) {
				$date_filter = $this->getdatetimereportV2($y, $i, $lastdate_filter);
				$data = array();

				# case : declare variable date start and date end
				$date_start  = $date_filter[key($date_filter)];
				$date_end    = end($date_filter);

				# case : query period summary
				$query = $this->db->select("channel_daily_period_summary.*")
					->from("channel_daily_period_summary")
					->where('channels_id', $channel_id)
					->where("date >= ", $date_start)
					->where("date <= ", $date_end)->get();
				foreach ($query->result() as $key => $value) {
					# set default array value
					if (!isset($data[$value->date])) {
						$data[$value->date] = array();
						for ($j = 0; $j <= 23; $j++) {
							$prefix = $j <= 9 ? '0' : "";
							$data[$value->date][$prefix . $j . ':00'] = 0;
						}
					}
					# set rating data
					$data[$value->date][$value->time] = $value->rating; # set rating

				}

				// Add new sheet
				$objPHPExcel->createSheet();
				$activesheet = $objPHPExcel->setActiveSheetIndex($sheet);
				$activesheet->setTitle("{$i}_{$y}");

				$row    = 1;
				$column = 1;
				$key = 0;
				if (!empty($date_filter)) {



					# case : set first column
					$activesheet->setCellValueByColumnAndRow(0, 1, 'time');
					$colIndex = PHPExcel_Cell::stringFromColumnIndex(0); # case : get column string name
					$activesheet->getColumnDimension("{$colIndex}")->setWidth(10); # set column width
					$activesheet->getStyle($colIndex . '1')->applyFromArray(
						array(
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'FFFF00')
							)
						)
					);
					# case : set time cell
					$timecellstart = 2;
					for ($timecell = 0; $timecell <= 23; $timecell++) {
						$timecellprefix = $timecell <= 9 ? '0' : "";
						$activesheet->setCellValueByColumnAndRow(0, $timecellstart, $timecellprefix . $timecell . ':00');
						++$timecellstart;
					}

					while ($key < count($date_filter)) {


						$activesheet->setCellValueByColumnAndRow($column, 1, $date_filter[$key]);
						$colIndex = PHPExcel_Cell::stringFromColumnIndex($column); # case : get column string name
						$activesheet->getColumnDimension("{$colIndex}")->setWidth(10); # set column width
						$activesheet->getStyle($colIndex . '1')->applyFromArray(
							array(
								'fill' => array(
									'type' => PHPExcel_Style_Fill::FILL_SOLID,
									'color' => array('rgb' => 'FFFF00')
								)
							)
						);

						$rowstart = 2;

						for ($x = 0; $x <= 23; $x++) {
							# case : set cell value
							$prefix = $x <= 9 ? '0' : "";
							$cellvalue = $data[$date_filter[$key]][$prefix . $x . ":00"];
							$activesheet->setCellValueByColumnAndRow($column, $rowstart, $cellvalue);
							$colIndexvalue = PHPExcel_Cell::stringFromColumnIndex($column); # case : get column string name
							$activesheet->getStyle($colIndexvalue . $rowstart)->getNumberFormat()->setFormatCode('0.000');
							++$rowstart;
						}
						++$key;
						++$column;
					}
				}


				++$sheet;
			}
		} # end for loop year
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		// We'll be outputting an excel file
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		$objWriter->save('php://output');
		exit;
	


	}













}
