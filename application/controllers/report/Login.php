<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		//$this->loadValidator();
		$this->load->library(array('Lang_controller','Msg'));
		$this->load->helper(array('our','lang','cookie'));

		//$this->template->set_template('login');


	}
	public function index(){
		//$this->load->view('welcome_message');

		$this->loadValidator();

		if($this->session->userdata('report_data')){
			//redirect(base_url('report/dashboard'));
			redirect(base_url('report/overview'));
		}


		if($this->input->post(NULL,FALSE)){
			//print_r($this->input->post());exit;
			/* check user login*/
			$queryCheck = $this->db->select('*')
			->from('user')
			->where('email',$this->input->post('email'))
			->where('password',sha1($this->input->post('password')))
			->get();

			if($queryCheck->num_rows() > 0){
				$row = $queryCheck->row();
				$checkusermenupermission = array();
				$checkusermenupermission = $this->checkUserMenuPermission($queryCheck->row());

				// print_r($checkaccountpermission);exit;

				if(!$checkusermenupermission['status']){
					// $this->msg->add(__('Permission not set yet,Please contact administrator','signin'),'error');
					// redirect($this->uri->uri_string());
				}else{
					$this->session->set_userdata('controller_available',$checkusermenupermission['controller_available']);
					$this->session->set_userdata('menu_available',$checkusermenupermission['menu_available']);

				}


				if($this->input->post('stay_signin')){
						$cookie_value = array(
							'email'=>base64_encode(base64_encode($row->email)),
							'password'=>base64_encode(base64_encode($this->input->post('password')))
						);
						set_cookie(array(
							'name'=>'remember_me_email',
							'value'=>$cookie_value['email'],
							'expire'=>1296000,
							'prefix'=>'_s3rating_'
						));
						set_cookie(array(
							'name'=>'remember_me_password',
							'value'=>$cookie_value['password'],
							'expire'=>1296000,
							'prefix'=>'_s3rating_'
						));

				}else{
						delete_cookie('_s3rating_remember_me_email');
						delete_cookie('_s3rating_remember_me_password');
				}

				if(!$row->active){
					$this->msg->add(__('Access has been disable,Please try again.','report/login'),'error');
					redirect($this->uri->uri_string());
				}


				$this->session->set_userdata('report_data',$queryCheck->row_array());

				if($checkaccountpermission['status'] && $row->user_accesstype_id != 1){
						$fsController = $checkaccountpermission['menu_available'][0]['controller'];
						$fsMethod = $checkaccountpermission['menu_available'][0]['method'];
						redirect(base_url($fsController.'/'.$fsMethod));
				}else{
						//redirect(base_url('report/dashboard'));
						redirect(base_url('report/overview'));
				}


			}else{
				$this->msg->add(__('Username or password invalid','report/login'),'error');
				redirect($this->uri->uri_string());
			}


		}
		$data = array(
			'remember_me'=>$this->getRememberMe()
		);

		$this->load->view('report/login',$data);

	}
	private function loadValidator(){
        $this->template->stylesheet->add(base_url('assets/css/bootstrapvalidator/bootstrapValidator.min.css'));
        $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/bootstrapValidator.min.js'));
    }

    private function getRememberMe(){
    	$arReturn = array(
    		'email'=>'',
    		'password'=>''
    	);

    	if(get_cookie('_s3rating_remember_me_email') && get_cookie('_s3rating_remember_me_password')){

    		$username = base64_decode(base64_decode(get_cookie('_s3rating_remember_me_email')));
    		$admin_password = base64_decode(base64_decode(get_cookie('_s3rating_remember_me_password')));
    		
    		$arReturn['email'] = $username;
    		$arReturn['password'] = $admin_password;
    	}
    	return $arReturn;
    }
    private function checkUserMenuPermission($objUser){
    	$arReturn = array();
    	if($objUser->user_accesstype_id != 1){
    		/* check already set to accountant menu permission*/
    		$usermenupermission = $this->db->select('*')
    		->from('user_menu_permission')
    		->join('user_menu','user_menu_permission.user_menu_id = user_menu.id')
    		->where('user_menu_permission.user_id',$objUser->id)->get();

    		if($usermenupermission->num_rows() <= 0){
    			$arReturn['status'] = false;
    		}else{
    			$arReturn['controller_available'] = array();
    			$arReturn['menu_available'] = array();
    			foreach ($usermenupermission->result() as $key => $value) {
    				# code...
    				array_push($arReturn['controller_available'], strtolower($value->controller_name));

    				array_push($arReturn['menu_available'], array(
    					'controller'=>strtolower($value->controller_name),
    					'method'=>strtolower($value->method_name),
    					'eventability'=>json_decode($value->event_ability,true)
    				));
    				
    			}
    			$arReturn['status'] = true;
    		}
    	}else{
    		$arReturn['status'] = true;
    	}

    	return $arReturn;

    }

    public function logout(){
    	//$this->session->sess_destroy();
    	$this->session->unset_userdata('controller_available');
    	$this->session->unset_userdata('menu_available');
    	$this->session->unset_userdata('report_data');
		redirect(base_url('report'));
    }


}
